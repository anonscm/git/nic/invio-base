package de.tarent.invio.android.admin;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

//this is a dummy test because we have no tests currently, so this is just there to generate coverage files
//can savely be removed when we have actual tests
public class DummyTest {

    @Test
    public void dummy(){
        assertTrue(true);
    }
}
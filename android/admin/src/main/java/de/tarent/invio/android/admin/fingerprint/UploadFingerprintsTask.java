package de.tarent.invio.android.admin.fingerprint;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import de.tarent.invio.android.admin.R;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;

import java.io.IOException;

import static de.tarent.invio.android.base.utils.DialogManager.keepDialogOpen;

/**
 * This is the AsyncTask which uploads the fingerprints to the server, in the background.
 */
class UploadFingerprintsTask extends AsyncTask {

    private static final String TAG = "UploadFingerprintsTask";

    /**
     * The minimum length of time the dialog should stay open for.
     */
    private static final int KEEP_DIALOG_OPEN_FOR = 2000;

    private FingerprintManager fingerprintManager;

    private ProgressDialog progressDialog;

    private Activity activity;

    private final MapServerClient mapServerClient;
    private String mapName;

    /**
     * Construct a new UploadFingerprintsTask, which belongs to a specific FingerprintManager
     *
     * @param fingerprintManager the owner of this task
     * @param activity           the activity, for resources and UI
     * @param mapServerClient    the {@link MapServerClient}
     * @param mapName            the name of the map to which we shall upload the fingerprints
     */
    public UploadFingerprintsTask(final FingerprintManager fingerprintManager,
                                  final Activity activity,
                                  final MapServerClient mapServerClient,
                                  final String mapName) {

        this.fingerprintManager = fingerprintManager;
        this.activity = activity;
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
    }

    // TODO: give notification in case of failure. Don't just log the errors. Do something :-)
    @Override
    protected Object doInBackground(final Object... params) {
        showProgressDialog();
        final long timeDialogOpened = System.currentTimeMillis();
        final String wifiJson = fingerprintManager.getWifiFingerprintsJson();
        final String bluetoothJson = fingerprintManager.getBluetoothLEFingerprintsJson();
        doUpload(wifiJson, bluetoothJson);
        keepDialogOpen(timeDialogOpened, KEEP_DIALOG_OPEN_FOR);
        progressDialog.dismiss();
        return null;
    }

    private void doUpload(final String wifiJson, final String bluetoothJson) {
        try {

            if (wifiJson != null) {
                mapServerClient.uploadWifiFingerprintsData(mapName, wifiJson);
            }

            if (bluetoothJson != null) {
                mapServerClient.uploadBluetoothLEFingerprintsData(mapName, bluetoothJson);
            }

        } catch (final InvioException | IOException e) {
            Log.e(TAG, "Failed to upload fingerprints: " + e);
        }
    }

    private void showProgressDialog() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ProgressDialog progress = new ProgressDialog(activity);
                progress.setCancelable(true);
                progress.setMessage(App.getContext().getString(R.string.saving_fingerprints));
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setCanceledOnTouchOutside(false);
                progress.show();
                progressDialog = progress;
            }
        });
    }
}

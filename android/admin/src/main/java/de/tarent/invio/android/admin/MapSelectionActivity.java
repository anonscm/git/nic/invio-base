package de.tarent.invio.android.admin;

import android.content.Intent;
import de.tarent.invio.android.base.AbstractMapSelectionActivity;
import de.tarent.invio.android.base.map.MapParams;

/**
 * The Admin-App implementation of the {@link AbstractMapSelectionActivity}.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class MapSelectionActivity extends AbstractMapSelectionActivity {
    
    public MapSelectionActivity() { //NOSONAR - This is only temporary code.
        MapParams.setIsAdmin(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Intent getIntentWithMapName(final String mapName) {
        final Intent intent = new Intent(this, MapActivity.class);
        //Use map prefix to filter which maps should be shown.
        final String mapsPrefix = this.getString(R.string.maps_prefix);
        String newMapName = mapName;
        //For testing purposes we check for the special prefix "tarentdebug" which will show us all presented maps.
        if (!mapsPrefix.equals("tarentdebug")) {
            newMapName = mapsPrefix + newMapName;
        }
        intent.putExtra("MapName", newMapName);

        return intent;
    }

    @Override
    public String getMapsPrefix() {
        final String mapsPrefix = this.getString(R.string.maps_prefix);
        return mapsPrefix;
    }
}

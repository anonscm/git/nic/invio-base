package de.tarent.invio.android.admin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import de.tarent.invio.android.admin.fingerprint.AdminFingerprintManager;
import de.tarent.invio.android.base.AbstractMapActivity;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.dialog.VersionDialog;
import de.tarent.invio.android.base.map.IndoorMap;
import de.tarent.invio.android.base.task.CachedDownloadMapDataTask;
import de.tarent.invio.android.base.task.CachedDownloadOccupancyGridMapTask;
import de.tarent.invio.android.base.task.DownloadListener;
import de.tarent.invio.android.base.task.DownloadTask;
import de.tarent.invio.android.base.task.InvioOsmXmlParser;
import de.tarent.invio.android.base.task.OsmParserKeys;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.MapServerClientImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.overlay.Overlay;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static de.tarent.invio.android.base.task.CachedDownloadOccupancyGridMapTask.isOsmDownloadFinished;
import static de.tarent.invio.android.base.task.CachedDownloadOccupancyGridMapTask.isOsmFileFound;


/**
 * The MapActivity is, currently, the OSM-MapView, where the admin can create and delete fingerprints.
 */
public class MapActivity extends AbstractMapActivity implements DownloadListener {

    public static final String GROUND_TRUTH_LAT_LONG = "groundTruthLongLat";
    private static final String TAG = MapActivity.class.getName();
    private static boolean wasTracking = false;


    /**
     * Scale will be shown in the Map Data dialog
     */
    protected float scale;

    /**
     * Base angle will be shown in the Map Data dialog
     */
    protected int baseAngle;

    protected int multilevelOrder;

    /**
     * This flag tells us whether we are in tracking-mode or in fingerprint-edit-mode.
     */
    private boolean tracking = false;

    private GroundTruthOverlay groundTruthOverlay;
    private Button groundTruthMakeWayButton;
    private boolean useBluetoothScanning;
    private boolean useWifiScanning;
    private boolean scanWifiFingerprint;
    private boolean scanBtleFingerprint;

    private ScanningDialog scanningDialog;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        setAdminApp(true); // Set the admin app flag to true because this is the admin app.

        super.onCreate(savedInstanceState, R.layout.mapview, R.id.osmmapview);

        final String serverEndpoint = getResources().getString(de.tarent.invio.android.base.R.string.server_endpoint);
        final MapServerClient mapServerClient = new MapServerClientImpl(serverEndpoint);

        setDrawingDebugLines(true);

        groundTruthOverlay = new GroundTruthOverlay(this);

        createGroundTruthMakeWayButton();

        final String mapName = getIntent().getStringExtra("MapName");
        if (mapName != null) {
            new CachedDownloadMapDataTask(this, mapServerClient, mapName, new InvioOsmXmlParser()).execute();
            new CachedDownloadOccupancyGridMapTask(this, mapName).execute();
        }

        setupFingerprintCaptureButton();
    }

    private void setupFingerprintCaptureButton() {
        if (mapView instanceof CaptureMapView) {
            if (fingerprintManager instanceof AdminFingerprintManager) {
                final ImageButton scanButton = (ImageButton) findViewById(R.id.scanButton);
                scanButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(final View v) {
                        if (!((AdminFingerprintManager)fingerprintManager).isCapturing()) {
                            ((CaptureMapView) mapView).captureFingerprint();
                        }
                    }
                });
            }
        }
    }

    private void createGroundTruthMakeWayButton() {
        groundTruthMakeWayButton = (Button) findViewById(R.id.btnGroundTruthWay);
        groundTruthMakeWayButton.setOnClickListener(new View.OnClickListener() {
            boolean makeWayToggle = false;

            @Override
            public void onClick(final View view) {
                makeWayToggle = !makeWayToggle;
                if (makeWayToggle) {
                    groundTruthOverlay.newWay();
                    groundTruthMakeWayButton.setText(R.string.end_way);
                } else {
                    groundTruthOverlay.finalizeWay();
                    groundTruthMakeWayButton.setText(R.string.make_way);
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onDownloadFinished(final DownloadTask task, final boolean success, final Object data) {
        if (success) {
            if (data instanceof Map) {
                final Map<String, Collection> map = (Map<String, Collection>) data;
                final List<Float> indoorScaleList = (List<Float>) map.get(OsmParserKeys.INDOOR_SCALE);
                if ((indoorScaleList != null) && (!indoorScaleList.isEmpty()) && (indoorScaleList.get(0) != null)) {
                    TrackerManager.getInstance().setScale(indoorScaleList.get(0));
                    scale = indoorScaleList.get(0);
                }
                final List<Integer> baseAngleList = (List<Integer>) map.get(OsmParserKeys.NORTH_ANGLE);
                if ((baseAngleList != null) && (!baseAngleList.isEmpty()) && (baseAngleList.get(0) != null)) {
                    baseAngle = baseAngleList.get(0);
                    TrackerManager.getInstance().setNorthAngle(baseAngle);
                }
                final List<Integer> multlevelOrderList = (List<Integer>) map.get(OsmParserKeys.MULTILEVEL_ORDER);
                if ((multlevelOrderList != null) && (!multlevelOrderList.isEmpty())
                        && (multlevelOrderList.get(0) != null)) {
                    multilevelOrder = multlevelOrderList.get(0);
                }
            } else if (data instanceof Bitmap) {
                final Bitmap ogm = (Bitmap) data;
                final boolean gridMapWasSet = TrackerManager.getInstance().setOccupancyGridMap(ogm);

                if (!gridMapWasSet) {
                    showOccupancyGridMapTooBigDialog();
                }
            }
        } else {
            Log.e(TAG, "Scale download signals failure. Dead reckoning will not work.");

            if (!isOsmFileFound() && isOsmDownloadFinished()) {
                showOccupancyGridMapNotFoundDialog();
            }
        }
    }

    private void showOccupancyGridMapTooBigDialog() {
        final String title = getString(R.string.dialog_ogm_too_big_title);
        final String message = getString(R.string.dialog_ogm_too_big_message);

        showOccupancyGridMapDialog(title, message);
    }

    private void showOccupancyGridMapNotFoundDialog() {
        final String title = getString(R.string.dialog_ogm_not_found_title);
        final String message = getString(R.string.dialog_ogm_not_found_message);

        showOccupancyGridMapDialog(title, message);
    }

    private void showOccupancyGridMapDialog(final String title, final String message) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getString(R.string.dialog_button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Dialog just needs to close, nothing else needs to be done.
                    }
                });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override // NOSONAR - Method cannot be shortened!!!
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.track_me:
                toggleTracking(true);
                return true;
            case R.id.upload_fingerprints:
                uploadFingerprints();
                return true;
            case R.id.menu_showMapData:
                showMapData();
                return true;
            case R.id.sensor_log_toggle:
                toggleCaptureMode();
                SensorDataLogHelper.toggleLogging(getApplicationContext(), mapName);
                return true;
            case R.id.dev_submenu:
                final Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
            case R.id.show_version:
                showVersionInformation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toggleCaptureMode() {
        final CaptureMapView captureMapView = (CaptureMapView) mapView;
        final List<Overlay> overlays = mapView.getOverlays();
        if (overlays.contains(fingerprintManager.getOverlay())) {
            overlays.remove(fingerprintManager.getOverlay());
            overlays.add(groundTruthOverlay);
            groundTruthMakeWayButton.setVisibility(View.VISIBLE);
            captureMapView.setCaptureMode(CaptureMapView.CaptureMode.GroundTruth);
            setDrawingDebugLines(false);
        } else {
            overlays.add(fingerprintManager.getOverlay());
            overlays.remove(groundTruthOverlay);
            groundTruthOverlay.writeToFile(new File(SensorDataLogHelper.LOG_PATH
                    + "/" + SensorDataLogHelper.getHistogramLogStartTime()
                    + "/" + GROUND_TRUTH_LAT_LONG
                    + SensorDataLogHelper.TXT_SUFFIX));
            groundTruthOverlay.clear();
            groundTruthMakeWayButton.setVisibility(View.INVISIBLE);
            setDrawingDebugLines(true);
            captureMapView.setCaptureMode(CaptureMapView.CaptureMode.Fingerprint);
        }
        mapView.postInvalidate();
    }

    @Override
    protected void onStop() {
        super.onStop();
        wasTracking = tracking; // NOSONAR - TODO: To avoid the dodgy write, should we put this in a Bundle?

        if (tracking) {
            toggleTracking(false); // Because the activity is stopping, we don't necessarily want to see the message.
            // This should probably be tested though.
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        applySettings();

        if (wasTracking && !tracking) {
            wasTracking = false;
            toggleTracking(false); // Because the activity is resuming, we don't necessarily want to see the message.
            // This should probably be tested though.
        }
    }

    /**
     * Here we read the settings from the {@link android.content.SharedPreferences} and then apply them.
     */
    private void applySettings() { //NOSONAR: Method length is fine.
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        handleWifiScanStatus(sharedPref);
        handleBluetoothScanStatus(sharedPref);

        final String keyBluetooth = getString(R.string.key_pref_use_bluetooth);
        useBluetoothScanning = sharedPref.getBoolean(keyBluetooth, false);

        final String keyWifi = getString(R.string.key_pref_use_wifi);
        useWifiScanning = sharedPref.getBoolean(keyWifi, true);

        final String keyScanWifi = getString(R.string.key_pref_scan_wifi);
        scanWifiFingerprint = sharedPref.getBoolean(keyScanWifi, true);

        final String keyScanBtle = getString(R.string.key_pref_scan_bluetooth);
        scanBtleFingerprint = sharedPref.getBoolean(keyScanBtle, true);

        if (useWifiScanning) {
            TrackerManager.getInstance().setSensorMode(TrackerManager.SensorMode.SENSOR_MODE_WIFI);
            TrackerManager.getInstance().setFingerprints(getFingerprintManager().getWifiFingerprintsWithProbability());
        } else if (useBluetoothScanning) {
            TrackerManager.getInstance().setSensorMode(TrackerManager.SensorMode.SENSOR_MODE_BLUETOOTH_LE_IBEACON);
            TrackerManager.getInstance().setFingerprints(getFingerprintManager().getBluetoothLEFingerprintsWithProbability());
        } else {
            //no sensors active, no scanning
            TrackerManager.getInstance().stopTracking();
        }

        setTrackerPreferences();

        //restart the tracker to load the new config
        if (TrackerManager.getInstance().isTracking()) {
            TrackerManager.getInstance().stopTracking();
            TrackerManager.getInstance().startTracking(this);
        }

        // Enable or disable showing the particles of the particle filter
        final String keyShowParticles = getString(R.string.key_pref_showparticles);
        final boolean showParticles = sharedPref.getBoolean(keyShowParticles, true);
        getUserPositionManager().setDrawParticles(showParticles);

        mapView.invalidate();
    }

    private void handleWifiScanStatus(final SharedPreferences preferences) {
        final Context context = App.getContext();
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final int wifiState = wifiManager.getWifiState();

        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_UNKNOWN:
                final SharedPreferences.Editor editor = preferences.edit();
                final String wifiPref = context.getString(R.string.key_pref_scan_wifi);
                editor.putBoolean(wifiPref, false);
                editor.apply();
        }
    }

    private void handleBluetoothScanStatus(final SharedPreferences preferences) {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            final Context context = App.getContext();
            final SharedPreferences.Editor editor = preferences.edit();
            final String wifiPref = context.getString(R.string.key_pref_scan_bluetooth);
            editor.putBoolean(wifiPref, false);
            editor.apply();
        }
    }

    private void setTrackerPreferences() {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final Map<String, ?> keys = prefs.getAll();

        for (final Map.Entry<String, ?> entry : keys.entrySet()) {
            final String key = entry.getKey();
            final String value = entry.getValue().toString();

            //removing the tracker prefix to avoid collision with possible other shared preference names
            if (key.startsWith(SettingsActivity.TRACKER_SHARED_PREFERENCES_PREFIX)) {
                final String configKey = key.replaceFirst(SettingsActivity.TRACKER_SHARED_PREFERENCES_PREFIX, "");
                TrackerManager.getInstance().setConfigParameter(configKey, value);
            }
        }
    }

    /**
     * Show the map data information such as scale and base angle in a dialog.
     */
    protected void showMapData() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Map Data");
        builder.setMessage("Scale: " + scale
                + "\n" + "Base angle: " + baseAngle);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /*
     * This method is only relevant for the multilevel context and not relevant for the admin app.
     */
    @Override
    public String getMultilevelDefaultMapShortName() {
        //Admin app does not have multilevel functionality
        return null;
    }

    @Override
    public void createLevelButtons(final List<IndoorMap> mapsList) {
        //Admin app does not have multilevel functionality
    }

    @Override
    public void configurePOISearch() {
        //Admin app does not have POI search functionality
    }

    @Override
    public void onMapSwitched(final IndoorMap map) {
        //Admin app does not have multilevel functionality
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void createFingerprintManager() {
        fingerprintManager = new AdminFingerprintManager(this, getMapServerClient(), mapView, mapName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void restoreFingerprintManagerFromBundle(final Bundle savedInstanceState) {
        fingerprintManager = new AdminFingerprintManager(this, getMapServerClient(), mapView, mapName, false);

        final String wifiFingerprintsJson = savedInstanceState.getString(WIFI_FINGERPRINTS_JSON);
        final String bluetoothFingerprintsJson = savedInstanceState.getString(BLUETOOTH_FINGERPRINTS_JSON);
        fingerprintManager.setWifiFingerprintsJson(wifiFingerprintsJson);
        fingerprintManager.setBluetoothLEFingerprintsJson(bluetoothFingerprintsJson);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createMultiLevelMap(final Bundle savedInstanceState) {

    }

    private void toggleTracking(final boolean showMessage) {
        tracking = !tracking;

        if (tracking && (useWifiScanning || useBluetoothScanning)) {
            TrackerManager.getInstance().startTracking(this);
        } else {
            TrackerManager.getInstance().stopTracking();
        }

        if (showMessage) {
            if (tracking) {
                showToast("Tracking enabled [" + TrackerManager.getInstance().version() + "]");
            } else {
                showToast("Tracking disabled");
            }

        }

        mapView.postInvalidate();
    }

    /**
     * Uploads the fingerprints.
     */
    protected void uploadFingerprints() {
        ((AdminFingerprintManager) fingerprintManager).uploadFingerprints();
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MapActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Shows the version information dialog.
     */
    protected void showVersionInformation() {
        final VersionDialog d = new VersionDialog(this);
        final Dialog dialog = d.buildVersionInformationDialog();
        dialog.show();
    }

    public ScanningDialog getScanningDialog() {
        return scanningDialog;
    }

    public void setScanningDialog(final ScanningDialog scanningDialog) {
        this.scanningDialog = scanningDialog;
    }

    public boolean isTracking() {
        return tracking;
    }

    /**
     * Add a Ground Truth Point to the Ground Truth Overlay
     *
     * @param currentPoint IGeoPoint location
     */
    public void addGroundTruthPoint(final IGeoPoint currentPoint) {
        groundTruthOverlay.addPoint(currentPoint);
        mapView.postInvalidate();
    }

    public boolean scanWifiFingerprint() {
        return scanWifiFingerprint;
    }

    public boolean scanBtleFingerprint() {
        return scanBtleFingerprint;
    }
}

package de.tarent.invio.android.admin.fingerprint;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import de.tarent.invio.android.admin.R;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.fingerprint.FingerprintItem;
import de.tarent.invio.entities.Fingerprint;

import java.util.Map;

/**
 * The FingerprintDialog shows a data stored in a fingerprint and allows the user to delete the fingerprint.
 */
public class FingerprintDialog extends Dialog {
    private static final String WIFI_TAB_ID = "tabWifi";
    private static final String BTLE_TAB_ID = "tabBtle";

    /**
     * We need the manager if the user wants to remove the fingerprint.
     */
    private AdminFingerprintManager manager;

    /**
     * The dialog belongs to one specific fingerprint.
     */
    private FingerprintItem item;

    /**
     * Default Constructor for the FragmentManager
     */
    public FingerprintDialog(final Context context) {
        super(context);
        buildDialog();
    }

    private void buildDialog() {
        setContentView(R.layout.fingerprint_dialog_layout);

        buildTabs();

        final Button closeButton = (Button) findViewById(R.id.button_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                closeDialog();
            }
        });

        final Button deleteButton = (Button) findViewById(R.id.button_delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                deleteFingerprint();
            }
        });
    }

    private void buildTabs() {
        final TabHost tabHost = (TabHost) findViewById(R.id.tab_host);
        tabHost.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
        tabHost.setup();

        final TabHost.TabSpec wifiTab = tabHost.newTabSpec(WIFI_TAB_ID);
        wifiTab.setIndicator("WIFI");
        wifiTab.setContent(R.id.wifi_text_view);
        tabHost.addTab(wifiTab);

        final TabHost.TabSpec btleTab = tabHost.newTabSpec(BTLE_TAB_ID);
        btleTab.setIndicator("BTLE");
        btleTab.setContent(R.id.btle_text_view);
        tabHost.addTab(btleTab);

        final TabWidget tw = (TabWidget) tabHost.findViewById(android.R.id.tabs);
        for (int i = 0; i < tw.getChildCount(); i++) {
            final View tabView = tw.getChildTabViewAt(i);
            final TextView textView = (TextView) tabView.findViewById(android.R.id.title);
            textView.setTextSize(18);
        }

        setOnTabChangedListener(tabHost);
    }

    private void setOnTabChangedListener(TabHost tabHost) {
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(final String tabId) {
                if (tabId.equals(WIFI_TAB_ID)) {
                    final ScrollView wifiView = (ScrollView) findViewById(R.id.wifi_scroll_view);
                    wifiView.setVisibility(View.VISIBLE);

                    final ScrollView btleView = (ScrollView) findViewById(R.id.btle_scroll_view);
                    btleView.setVisibility(View.GONE);
                } else {
                    final ScrollView wifiView = (ScrollView) findViewById(R.id.wifi_scroll_view);
                    wifiView.setVisibility(View.GONE);

                    final ScrollView btleView = (ScrollView) findViewById(R.id.btle_scroll_view);
                    btleView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void setTitle() {
        super.setTitle(buildTitle());
    }

    /**
     * Sets the messages for the two views (wifi and btle).
     */
    public void setMessage() {
        final TextView wifiView = (TextView) findViewById(R.id.wifi_text_view);
        wifiView.setText(Html.fromHtml(buildWifiMessage()));

        final TextView btleView = (TextView) findViewById(R.id.btle_text_view);
        btleView.setText(Html.fromHtml(buildBtleMessage()));
    }

    private void deleteFingerprint() {
        manager.removeFingerprintFromOverlay(item);
        dismiss();
    }

    private void closeDialog() {
        dismiss();
    }

    /**
     * The title of the dialog shows the ID of the fingerprint and its coordinates.
     *
     * @return the title string
     */
    private String buildTitle() {
        final Fingerprint fingerprint = getFingerprint();

        if (fingerprint == null) {
            dismiss();
            return null;
        }

        final String title = fingerprint.getId() + ": " +
                fingerprint.getPoint().getLatitudeE6() + ", " +
                fingerprint.getPoint().getLongitudeE6();

        return title;
    }

    private Fingerprint getFingerprint() {
        // Try and get a wifi fingerprint first.
        Fingerprint fingerprint = item.getWifiFingerprint();

        if (fingerprint == null) {
            // If the wifi fingerprint is null, it might mean that the fingerprint is actually a bluetooth fingerprint.
            fingerprint = item.getBluetoothLEFingerprint();
        }
        return fingerprint;
    }

    /**
     * The message is the body of the dialog. It shows the list of access points and their signal strength levels
     * at this point.
     *
     * @return the message body
     */
    private String buildWifiMessage() {
        final StringBuilder message = new StringBuilder();

        if (item.getWifiFingerprint() != null) {
            appendFingerprint(message, item.getWifiFingerprint());
            if (item.getWifiFingerprint().getHistogram().isEmpty()) {
                message.append(App.getContext().getString(R.string.dialog_fingerprint_no_wifi_signals_found))
                        .append("<br/><br/>");
            }
        }

        return message.toString();
    }

    private String buildBtleMessage() {
        final StringBuilder message = new StringBuilder();

        if (item.getBluetoothLEFingerprint() != null) {
            appendFingerprint(message, item.getBluetoothLEFingerprint());
            if (item.getBluetoothLEFingerprint().getHistogram().isEmpty()) {
                message.append(App.getContext().getString(R.string.dialog_fingerprint_no_bluetooth_signals_found));
            }
        }

        return message.toString();
    }

    private void appendFingerprint(final StringBuilder appendTo, final Fingerprint fingerprint) {
        for (final String bssid : fingerprint.getHistogram().keySet()) {
            String name = bssid;
            if(manager != null && manager.getTransmitterNames() != null) {
                final String tms = manager.getTransmitterNames().get(bssid);
                if (tms != null && !tms.isEmpty()) {
                    name = tms;
                }
            }
            appendTo.append("<br/><b>").append(name).append("</b><br/>");
            final Map<Integer, Float> levels = fingerprint.getHistogram().get(bssid);
            for (final Map.Entry<Integer, Float> level : levels.entrySet()) {
                appendTo.append("  ").append(level.getKey()).append(App.getContext()
                        .getString(R.string.dialog_fingerprint_dB_tag))
                        .append(level.getValue()).append("<br/>");
            }
        }
    }

    public void setManager(final AdminFingerprintManager manager) {
        this.manager = manager;
    }

    public void setItem(final FingerprintItem item) {
        this.item = item;
    }
}

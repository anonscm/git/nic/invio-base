package de.tarent.invio.android.admin.fingerprint;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.widget.ProgressBar;
import android.widget.Toast;
import de.tarent.invio.android.admin.MapActivity;
import de.tarent.invio.android.admin.R;
import de.tarent.invio.android.admin.ScanningDialog;
import de.tarent.invio.android.base.AbstractMapActivity;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.collector.BluetoothLECollector;
import de.tarent.invio.android.base.collector.CollectorListener;
import de.tarent.invio.android.base.collector.HistogramCollector;
import de.tarent.invio.android.base.collector.SensorCollector;
import de.tarent.invio.android.base.collector.WifiCollector;
import de.tarent.invio.android.base.fingerprint.FingerprintItem;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.mapserver.MapServerClient;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;

import java.util.ArrayList;
import java.util.List;


/**
 * The FingerprintManager holds the wifi-fingerprints and the overlay to display them.
 * It is also responsible for uploading and downloading fingerprints to and from the mapserver.
 */
public class AdminFingerprintManager extends FingerprintManager {
    public static final int SCAN_COUNT = 15;    // number of wifi scans to make
    public static final int SCAN_TIME = 15;     // seconds to scan for ibeacons

    /**
     * This is the point where the user wants to create the next fingerprint. It has to be stored somewhere because
     * the actual creation is triggered later, asynchronously by the wifiCollector.
     */
    private IGeoPoint currentFingerprintLocation;

    private FingerprintItem currentFingerprintItem = null;

    private SensorCollector currentSensorCollector = null;
    private boolean isCapturing = false;
    private boolean scanCancelled = false;

    private String fingerprintId;


    /**
     * Construct a new FingerprintManager.
     *
     * @param activity        the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient the {@link MapServerClient}
     * @param mapView         the {@link MapView}
     * @param mapName         the name of the map, as it is used in the mapserver-URLs
     */
    public AdminFingerprintManager(final AbstractMapActivity activity,
                                   final MapServerClient mapServerClient,
                                   final MapView mapView, final String mapName) {
        super(activity, mapServerClient, mapView, mapName);
    }


    /**
     * Construct a new FingerprintManager with possibility not to download the fingerprints.
     *
     * @param activity             the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient      the {@link MapServerClient}
     * @param mapView              the {@link MapView}
     * @param mapName              the name of the map, as it is used in the mapserver-URLs
     * @param downloadFingerprints flag in case we want to construct without downloading the fingerprints
     */
    public AdminFingerprintManager(final AbstractMapActivity activity,
                                   final MapServerClient mapServerClient,
                                   final MapView mapView, final String mapName,
                                   final boolean downloadFingerprints) {
        super(activity, mapServerClient, mapView, mapName, downloadFingerprints, true);
    }


    /**
     * {@inheritDoc}
     */
    @Override // NOSONAR - This method cannot be shortened any more than it already has been.
    public boolean onItemSingleTapUp(final int index, final FingerprintItem item) {
        final FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        final Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        ft.commit();

        final FingerprintDialog dialog = new FingerprintDialog(this.activity);
        dialog.setManager(this);

        // We need to get some information from the item which is why we need to set the title and
        // messages after setting the item. Then we show the dialog.
        dialog.setItem(item);
        dialog.setTitle();
        dialog.setMessage();
        dialog.show();
        return true;
    }


    /**
     * Creates a wifi fingerprint by using the users finger position.
     *
     * @param point the {@link IGeoPoint}
     */
    public void createWifiFingerprint(final IGeoPoint point) {
        createFingerprintItem(point);

        currentSensorCollector = new WifiCollector(HistogramCollector.CollectionMode.FINGERPRINT);
        currentSensorCollector.startSensors(new CollectorListener() {
            @Override
            public void onCollectionStart() {
                isCapturing = true;
            }

            @Override
            public void onCollectionIteration(final int iteration, final int maxIterations) {
                updateProgress(iteration, maxIterations);
            }

            @Override
            public void onCollectionStop() {
                isCapturing = false;
                if (currentFingerprintItem != null) {
                    final HistogramCollector collector = (WifiCollector) currentSensorCollector;
                    final Fingerprint fingerprint = createFingerprint(collector);
                    if (fingerprint != null) {
                        currentFingerprintItem.setWifiFingerprint(fingerprint);
                    }
                    mapView.postInvalidate();
                }
                ((MapActivity) activity).getScanningDialog().setWifiProgressBar(null);
                dismissProgressDialog();
                stopCapturingFingerprint(false);
            }
        }, SCAN_COUNT);
    }


    private Fingerprint createFingerprint(final HistogramCollector collector) {
        Histogram histogram = collector.getHistogram();
        if (histogram == null) {
            histogram = new Histogram();
        }

        histogram.setId(fingerprintId);

        return new Fingerprint(histogram, new InvioGeoPoint(
                currentFingerprintLocation.getLatitudeE6(),
                currentFingerprintLocation.getLongitudeE6()), SCAN_COUNT, SCAN_TIME);
    }

    private void createFingerprintItem(final IGeoPoint point) {
        if (currentFingerprintLocation == null
                || currentFingerprintLocation.getLatitudeE6() != point.getLatitudeE6()
                || currentFingerprintLocation.getLongitudeE6() != point.getLongitudeE6()) {
            currentFingerprintLocation = point;
            currentFingerprintItem = new FingerprintItem();
            currentFingerprintItem.setGeoLocation(currentFingerprintLocation.getLatitudeE6(),
                    currentFingerprintLocation.getLongitudeE6());
            overlay.addItem(currentFingerprintItem);
        }
    }


    private void updateProgress(final int iteration, final int maxIterations) {
        final MapActivity mapActivity = ((MapActivity) activity);
        mapActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ScanningDialog scanningDialog = mapActivity.getScanningDialog();
                final ProgressBar wifiProgressBar = scanningDialog.getWifiProgressBar();
                final ProgressBar btleProgressBar = scanningDialog.getBtleProgressBar();

                if (wifiProgressBar != null) {
                    final Double progress = (double) iteration / maxIterations * 100;
                    scanningDialog.setWifiProgressFraction(iteration, maxIterations);
                    scanningDialog.setWifiProgressPercent(progress.intValue());
                    wifiProgressBar.setProgress(progress.intValue());
                } else if (btleProgressBar != null) {
                    // We need +1 on the iteration here because bluetooth starts on 0 for some reason.
                    // TODO: Fix this issue.
                    final Double progress = (double) (iteration + 1) / maxIterations * 100;
                    scanningDialog.setBtleProgressFraction((iteration + 1), maxIterations);
                    scanningDialog.setBtleProgressPercent(progress.intValue());
                    btleProgressBar.setProgress(progress.intValue());
                }
            }
        });
    }

    /**
     * Creates a bluetooth le fingerprint by using the users finger position.
     *
     * @param point the {@link IGeoPoint}
     * @return true if bluetooth is supported and enabled, otherwise false
     */
    public boolean createBluetoothLEFingerprint(final IGeoPoint point) {
        //only create bluetooth le fingerprints if we are on a device that has bluetooth le (Nexus 7, SGS4, ...)
        if (isBluetoothEnabled()) {
            createFingerprintItem(point);
            createBtleSensorCollector();
            return true;
        }
        return false;
    }


    private void createBtleSensorCollector() {
        final BluetoothLECollector btleCollector
                = BluetoothLECollector.createCollector(HistogramCollector.CollectionMode.FINGERPRINT);
        btleCollector.setProximityUUID(TrackerManager.IBEACON_PROXIMITY_UUID);
        currentSensorCollector = btleCollector;
        currentSensorCollector.startSensors(new CollectorListener() {
            @Override
            public void onCollectionStart() {
                isCapturing = true;
            }

            @Override
            public void onCollectionIteration(final int iteration, final int maxIterations) {
                updateProgress(iteration, maxIterations);
            }

            @Override
            public void onCollectionStop() {
                isCapturing = false;
                if (currentFingerprintItem != null) {
                    final HistogramCollector collector = (BluetoothLECollector) currentSensorCollector;
                    final Fingerprint fingerprint = createFingerprint(collector);
                    if (fingerprint != null) {
                        currentFingerprintItem.setBluetoothLEFingerprint(fingerprint);
                    }
                    mapView.postInvalidate();
                }
                ((MapActivity) activity).getScanningDialog().setBtleProgressBar(null);
                dismissProgressDialog();
                stopCapturingFingerprint(false);
            }
        }, SCAN_TIME);
    }

    private void dismissProgressDialog() {
        final MapActivity mapActivity = ((MapActivity) activity);
        final ScanningDialog scanningDialog = mapActivity.getScanningDialog();
        if (scanningDialog != null) {
            if (scanningDialog.getWifiProgressBar() == null && scanningDialog.getBtleProgressBar() == null) {
                scanningDialog.dismiss();
            }
        }
    }


    /**
     * Upload the current list of Fingerprints to the mapserver. The old list on the server will be overwritten.
     */
    public void uploadFingerprints() {
        if (isNetworkAvailable()) {
            // Upload the fingerprints in the background:
            // TODO: we need a timeout here as well. Maybe, now that we're back at httpcomponents 4.0.1, we can
            //       have a "normal" setTimeout-method in our client-lib.
            new UploadFingerprintsTask(this, activity, mapServerClient, mapName).execute();
        } else {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, "No network: upload failed", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    /**
     * Create an overlay to display icons for the fingerprints. In the admin-app we want to see the fingerprints,
     * so we need a different overlay than what we use in the customer-apps. This one here simply works like a normal
     * ItemizedIconOverlay, letting it draw its icons as it was meant to be.
     *
     * @param activity the activity, used for the context that the DefaultResourceProxyImpl of the overlay needs.
     * @return the new ItemizedIconOverlay
     */
    protected ItemizedIconOverlay<FingerprintItem> createFingerprintOverlay(final Activity activity) {
        return new ItemizedIconOverlay<FingerprintItem>(new ArrayList<FingerprintItem>(),
                activity.getResources().getDrawable(R.drawable.fingerprint),
                this,
                new DefaultResourceProxyImpl(activity)) {
        };
    }


    /**
     * Stops the fingerprint from capturing by stopping the running sensor and deleting the fingerprint item.
     *
     * @param stopAll true: delete the fingerprint item; false: only stop the sensors
     */
    public void stopCapturingFingerprint(final boolean stopAll) {
        stopCapturingFingerprint(stopAll, false);
    }

    /**
     *  Stops the fingerprint from capturing by stopping the running sensor and deleting the fingerprint item.
     *
     * @param stopAll       true: delete the fingerprint item; false: only stop the sensors
     * @param scanCancelled true: set {@link #scanCancelled} to true and {@link #currentFingerprintLocation} to null
     */
    public void stopCapturingFingerprint(final boolean stopAll, final boolean scanCancelled) {
        if (!isCapturing) {
            return;
        }

        if (currentSensorCollector != null) {
            currentSensorCollector.stopSensors();
        }

        if (stopAll) {
            if (currentFingerprintItem != null) {
                getOverlay().removeItem(currentFingerprintItem);
                currentFingerprintItem = null;
            }

            disableFingerprinting();
        }

        if (scanCancelled) {
            this.currentFingerprintLocation = null;
            this.scanCancelled = true;
        }

        this.isCapturing = false;
    }


    private void disableFingerprinting() {
        currentFingerprintItem = null;
        currentSensorCollector = null;
        mapView.postInvalidate();
        dismissProgressDialog();
    }

    /**
     * Determines and sets the next {@link #fingerprintId} for the fingerprint that is about to be scanned. This will
     * always be the max of both wifi and btle ids +1.
     */
    public void determineFingerprintId() {
        final Integer maxWifiId = extractMaxFingerprintId(getWifiFingerprints());
        final Integer maxBtleId = extractMaxFingerprintId(getBluetoothLEFingerprints());

        final int maxId = Math.max((maxWifiId == null ? 0 : maxWifiId), (maxBtleId == null ? 0 : maxBtleId));

        fingerprintId = "FP-" + (maxId + 1);
    }

    private Integer extractMaxFingerprintId(final List<Fingerprint> fingerprints) {
        Integer maxId = null;

        for (final Fingerprint fingerprint : fingerprints) {
            if (fingerprint == null) {
                continue;
            }

            final int fingerprintId = Integer.parseInt(fingerprint.getId().substring(3)); // ID without leading "FP-"

            if (maxId == null || fingerprintId > maxId) {
                maxId = fingerprintId;
            }
        }

        return maxId;
    }

    public boolean isCapturing() {
        return isCapturing;
    }

    public boolean isScanCancelled() {
        return scanCancelled;
    }

    public void setScanCancelled(final boolean scanCancelled) {
        this.scanCancelled = scanCancelled;
    }
}

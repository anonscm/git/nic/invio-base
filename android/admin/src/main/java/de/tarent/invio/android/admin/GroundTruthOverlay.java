package de.tarent.invio.android.admin;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.MotionEvent;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Ground truth overlay for creating ground truth data.
 */
public class GroundTruthOverlay extends Overlay {

    private List<GroundTruthPoint> currentWay;
    private boolean wayEditingMode;
    private List<GroundTruthPoint> points = new ArrayList<>();
    private List<List<GroundTruthPoint>> ways = new ArrayList<>();
    private BitmapDrawable groundTruthIcon;
    private Point pointBuffer = new Point();
    private Paint paint = new Paint();
    private Paint wayPaint = new Paint();
    private Paint circlePaint = new Paint();
    private float minWayDistanceSquared = 5.0f;

    /**
     * Constructs a Ground truth overlay for creating ground truth data.
     *
     * @param ctx Android Context
     */
    public GroundTruthOverlay(final Context ctx) {
        super(ctx);

        groundTruthIcon = (BitmapDrawable) ctx.getResources().getDrawable(R.drawable.ground_truth_point);
        wayPaint.setAntiAlias(true);
        wayPaint.setColor(Color.BLUE);
        wayPaint.setStrokeWidth(4.0f);
    }

    /**
     * Add a Point to the Ground Truth Overlay
     *
     * @param point the location of the Point
     */
    public void addPoint(final IGeoPoint point) {
        points.add(new GroundTruthPoint(point.getLatitudeE6(), point.getLongitudeE6()));
    }

    /**
     * Add a new Way to the Ground Truth Overlay
     */
    public void newWay() {
        currentWay = new ArrayList<>();
        ways.add(currentWay);
        wayEditingMode = true;
    }

    /**
     * Finalize the current way
     */
    public void finalizeWay() {
        wayEditingMode = false;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent e, final MapView mapView) {
        if (wayEditingMode && (e.getAction() == MotionEvent.ACTION_MOVE || e.getAction() == MotionEvent.ACTION_DOWN)) {
            IGeoPoint lastPoint = null;
            if (currentWay.size() > 0) {
                lastPoint = currentWay.get(currentWay.size() - 1);
            }

            final IGeoPoint point = mapView.getProjection().fromPixels((int) e.getX(), (int) e.getY());
            Point lastPointPixels = null;

            if (lastPoint != null) {
                lastPointPixels = mapView.getProjection().toPixels(lastPoint, new Point());
            }

            if (lastPoint == null
                    || Math.abs(lastPointPixels.x - (int) e.getX()) > minWayDistanceSquared
                    || Math.abs(lastPointPixels.y - (int) e.getY()) > minWayDistanceSquared) {
                currentWay.add(new GroundTruthPoint(point.getLatitudeE6(), point.getLongitudeE6()));
                mapView.postInvalidate();
            }
            return true;
        }
        return false;
    }

    /**
     * Deletes all Ground Truth data from the overlay
     */
    public void clear() {
        points.clear();
        ways.clear();
    }


    /**
     * Write the Ground Truth Data to a File
     * <p/>
     * Format:
     * <p/>
     * point timestamp lat long
     * way-start timestamp lat long
     * way-point timestamp lat long
     * way-end timestamp lat long
     *
     * @param file the File to write to
     */
    public void writeToFile(final File file) {
        try {
            if (points.size() > 0 || ways.size() > 0) {
                final FileWriter fw = new FileWriter(file); //NOSONAR default encoding is fine

                for (final GroundTruthPoint point : points) {
                    fw.write("point " + point.timestamp + " " + point.getLongitude() + " "
                            + point.getLatitude() + "\n");
                }
                writeWays(fw);
                fw.close();
            }
        } catch (final IOException e) {
            Log.d(this.getClass().getSimpleName(), e.toString());

        }
    }

    private void writeWays(final FileWriter fw) throws IOException {
        for (final List<GroundTruthPoint> way : ways) {
            for (int i = 0; i < way.size(); i++) {
                final GroundTruthPoint point = way.get(i);
                if (i == 0) {
                    fw.write("way-start " + point.timestamp + " " +
                            point.getLongitude() + " " + point.getLatitude() + "\n");
                } else if (i == way.size() - 1) {
                    fw.write("way-end " + point.timestamp + " " +
                            point.getLongitude() + " " + point.getLatitude() + "\n");
                } else {
                    fw.write("way-point " + point.timestamp + " " +
                            point.getLongitude() + " " + point.getLatitude() + "\n");
                }
            }
        }
    }

    @Override
    protected void draw(final Canvas canvas, final MapView mapView, final boolean b) {
        for (int i = 0; i < points.size(); i++) {
            drawGroundTruthIcon(canvas, mapView, points.get(i));
        }

        for (int i = 0; i < ways.size(); i++) {
            final List<GroundTruthPoint> way = ways.get(i);
            for (int j = 0; j < way.size() - 1; j++) {
                if (way.size() > 1) {
                    final GroundTruthPoint p1 = way.get(j);
                    final GroundTruthPoint p2 = way.get(j + 1);

                    drawLine(canvas, mapView, p1, p2);
                }
            }

            if (way.size() > 1) {
                drawCircle(canvas, mapView, way.get(0));
                drawCircle(canvas, mapView, way.get(way.size() - 1));
            }
        }
    }

    private void drawGroundTruthIcon(final Canvas canvas, final MapView mapView, final IGeoPoint point) {
        mapView.getProjection().toPixels(point, pointBuffer);
        canvas.drawBitmap(groundTruthIcon.getBitmap(),
                pointBuffer.x - groundTruthIcon.getBitmap().getWidth() / 2,
                pointBuffer.y - groundTruthIcon.getBitmap().getHeight(), paint);
    }

    private void drawLine(final Canvas canvas, final MapView mapView, final IGeoPoint p1, final IGeoPoint p2) {
        mapView.getProjection().toPixels(p1, pointBuffer);
        final float p1x = pointBuffer.x;
        final float p1y = pointBuffer.y;

        mapView.getProjection().toPixels(p2, pointBuffer);
        final float p2x = pointBuffer.x;
        final float p2y = pointBuffer.y;

        canvas.drawLine(p1x, p1y, p2x, p2y, wayPaint);
    }

    private void drawCircle(final Canvas canvas, final MapView mapView, final IGeoPoint point) {
        mapView.getProjection().toPixels(point, pointBuffer);
        final float px = pointBuffer.x;
        final float py = pointBuffer.y;

        canvas.drawCircle(px, py, 7.0f, circlePaint);
    }

    /**
     * Holds the data for the ground truth {@link GeoPoint}.
     */
    private static final class GroundTruthPoint extends GeoPoint {
        long timestamp;

        private GroundTruthPoint(final int latitudeE6, final int longitudeE6) {
            super(latitudeE6, longitudeE6);
            timestamp = System.currentTimeMillis();
        }
    }
}

package de.tarent.invio.android.admin;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.tracker.TrackerConfigParameter;

import java.util.Arrays;
import java.util.Comparator;


/**
 * The purpose of this activity is to show the preference fragment.
 */
public class SettingsActivity extends PreferenceActivity {

    /**
     * Shared preference tracker prefix, used to avoid collision with property names from the tracker and
     * general shared preference keys
     */
    public static final String TRACKER_SHARED_PREFERENCES_PREFIX = "tracker_";

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SettingsFragment settingsFragment = new SettingsFragment();
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, settingsFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset:
                resetPreferences(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private static String getDisplayName(TrackerConfigParameter parameter) {
        String displayName = parameter.description;
        if (displayName.length() == 0) {
            displayName = parameter.name;
        } else {
            if (!displayName.contains(parameter.name)) {
                displayName = displayName + " (" + parameter.name + ")";
            }
        }
        return displayName;
    }

    /**
     * The settings fragment for managing general shared preference settings and
     * Tracker related settings
     */
    public static class SettingsFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener {

        private static final int WIFI_INTENT = 1;
        private static final int BTLE_INTENT = 2;

        private SwitchPreference wifiPreference = null;
        private SwitchPreference btlePreference = null;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);
            addTrackerPreferences();
        }

        private void addTrackerPreferences() {
            final TrackerConfigParameter[] configParameters = TrackerManager.getInstance().getConfigParameters();

            //alphabetic sort
            Arrays.sort(configParameters, new Comparator<TrackerConfigParameter>() {
                @Override
                public int compare(final TrackerConfigParameter lhs, final TrackerConfigParameter rhs) {
                    return lhs.description.compareTo(rhs.description);
                }
            });

            for (final TrackerConfigParameter parameter : configParameters) {
                final Preference preference;

                switch (parameter.type) {
                    case TrackerConfigParameter.BOOL:
                        final CustomSwitchPreference boolPreference = new CustomSwitchPreference(getActivity());
                        boolPreference.setChecked(Boolean.valueOf(parameter.currentValue));
                        boolPreference.setKey(TRACKER_SHARED_PREFERENCES_PREFIX + parameter.name);
                        boolPreference.setTitle(getDisplayName(parameter));
                        preference = boolPreference;
                        break;
                    default:
                        preference = new TrackerEditTextPreference(getActivity(), parameter);
                        break;
                }

                getPreferenceScreen().addPreference(preference);
            }
        }

        @Override
        public void onResume() {
            super.onResume();

            handleWifiStatus();
            handleBluetoothStatus();

            getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();

            getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
            if (requestCode == WIFI_INTENT && wifiPreference != null) {
                final WifiManager wifiManager = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);
                final int wifiState = wifiManager.getWifiState();

                switch (wifiState) {
                    case WifiManager.WIFI_STATE_DISABLED:
                    case WifiManager.WIFI_STATE_DISABLING:
                    case WifiManager.WIFI_STATE_UNKNOWN:
                        wifiPreference.setChecked(false);
                }
            } else if (requestCode == BTLE_INTENT && btlePreference != null) {
                if (resultCode > -1) {
                    btlePreference.setChecked(false);
                }

            }
        }

        @Override
        public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
            final Preference preference = findPreference(key);
            if (preference != null) {
                if (preference instanceof TrackerEditTextPreference) {
                    preference.setSummary(sharedPreferences.getString(key,
                            ((TrackerEditTextPreference) preference).parameter.currentValue));
                } else if (preference instanceof SwitchPreference) {
                    final SwitchPreference switchPreference = (SwitchPreference) preference;
                    final Context context = App.getContext();
                    if (key.equals(context.getString(R.string.key_pref_scan_wifi))) {
                        if (sharedPreferences.getBoolean(key, false)) {
                            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                            final int wifiState = wifiManager.getWifiState();

                            switch (wifiState) {
                                case WifiManager.WIFI_STATE_DISABLED:
                                case WifiManager.WIFI_STATE_DISABLING:
                                case WifiManager.WIFI_STATE_UNKNOWN:
                                    final Intent enableWifiIntent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
                                    startActivityForResult(enableWifiIntent, WIFI_INTENT);
                            }
                        }

                        wifiPreference = switchPreference;

                    } else if (key.equals(context.getString(R.string.key_pref_scan_bluetooth))) {
                        final BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                        if (!mBluetoothAdapter.isEnabled() && sharedPreferences.getBoolean(key, false)) {
                            final Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBluetoothIntent, BTLE_INTENT);
                        }

                        btlePreference = switchPreference;
                    } else {
                        switchPreference.setChecked(sharedPreferences.getBoolean(key, false));
                    }
                }
            }
        }

        private void handleWifiStatus() {
            final Context context = App.getContext();
            final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final int wifiState = wifiManager.getWifiState();

            switch (wifiState) {
                case WifiManager.WIFI_STATE_DISABLED:
                case WifiManager.WIFI_STATE_DISABLING:
                case WifiManager.WIFI_STATE_UNKNOWN:
                    final SwitchPreference wifiPreference
                            = (SwitchPreference) findPreference(context.getString(R.string.key_pref_scan_wifi));
                    wifiPreference.setChecked(false);
            }
        }

        private void handleBluetoothStatus() {
            final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (!bluetoothAdapter.isEnabled()) {
                final Context context = App.getContext();
                final SwitchPreference btlePreference
                        = (SwitchPreference) findPreference(context.getString(R.string.key_pref_scan_bluetooth));
                btlePreference.setChecked(false);
            }
        }
    }


    /**
     * internal class for editable tracker preferences
     */
    private static class TrackerEditTextPreference extends EditTextPreference {
        private View mDialogView;
        private TrackerConfigParameter parameter;

        public TrackerEditTextPreference(final Context context, final TrackerConfigParameter parameter) {
            super(context);

            this.parameter = parameter;
            setDialogLayoutResource(R.layout.tracker_preference_dialog);

            switch (parameter.type) {
                case TrackerConfigParameter.INT:
                    getEditText().setInputType(InputType.TYPE_CLASS_NUMBER
                            | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    limitInputRange();
                    break;
                case TrackerConfigParameter.DOUBLE:
                    getEditText().setInputType(InputType.TYPE_CLASS_NUMBER
                            | InputType.TYPE_NUMBER_FLAG_DECIMAL
                            | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    limitInputRange();
                    break;
                default:
                    getEditText().setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
                    break;
            }

            setKey(TRACKER_SHARED_PREFERENCES_PREFIX + parameter.name);

            setTitle(getDisplayName(parameter));
            setSummary(parameter.currentValue);

            setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(final Preference preference, final Object newValue) {
                    final String newValueString = newValue.toString();

                    if (parameter.type == TrackerConfigParameter.DOUBLE ||
                        parameter.type == TrackerConfigParameter.INT) {
                        //reject the new value if its an invalid double (51. for example)
                        final int length = newValueString.length();
                        if (newValueString.lastIndexOf('.') == length - 1 || length == 0) {
                            return false;
                        }
                    }

                    return true;
                }
            });
        }

        @Override
        protected void onSetInitialValue(final boolean restoreValue, final Object defaultValue) {
            super.onSetInitialValue(restoreValue, defaultValue);

            setSummary(getValue());
        }

        private String getValue() {
            return getSharedPreferences().getString(getKey(), parameter.currentValue);
        }

        private void limitInputRange() {
            getEditText().setFilters(new InputFilter[]{new InputFilter() {
                @Override
                public CharSequence filter(final CharSequence source, final int start, final int end,
                                           final Spanned dest, final int dstart, final int dend) {
                    try {
                        final String replacement = source.subSequence(start, end).toString();
                        final String newVal = dest.subSequence(0, dstart).toString()
                                + replacement
                                + dest.subSequence(dend, dest.length()).toString();

                        final double value = Double.valueOf(newVal);
                        final double clamped = Math.max(Math.min(parameter.maxValue, value), parameter.minValue);
                        return clamped == value ? null : "";
                    } catch (final NumberFormatException e) {
                        return "";
                    }
                }
            }});
        }

        @Override
        protected void onBindDialogView(final View view) {
            super.onBindDialogView(view);

            mDialogView = view;

            if (parameter != null) {
                if (parameter.type != TrackerConfigParameter.STRING) {
                    final TextView minValueTextView = (TextView) mDialogView.findViewById(R.id.minValue);
                    minValueTextView.setText(getContext().getString(R.string.preference_min_value, parameter.minValue));

                    final TextView maxValueTextView = (TextView) mDialogView.findViewById(R.id.maxValue);
                    maxValueTextView.setText(getContext().getString(R.string.preference_max_value, parameter.maxValue));
                }

                final TextView defaultValueTextView = (TextView) mDialogView.findViewById(R.id.defaultValue);
                defaultValueTextView.setText(getContext()
                        .getString(R.string.preference_default_value, parameter.defaultValue));
            }
        }

        @Override
        protected void onAddEditTextToDialogView(final View dialogView, final EditText editText) {
            final ViewGroup container = (ViewGroup) dialogView
                    .findViewById(R.id.edittext_container);
            if (container != null) {
                container.addView(editText, ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                editText.setText(getValue());
                editText.setSelection(editText.getText().length());
            }
        }
    }

    /**
     * Resets the general and tracker preferences to the default values
     *
     * @param context a valid android context
     */
    public static void resetPreferences(final Context context) {
        final TrackerConfigParameter[] configParameters = TrackerManager.getInstance().getConfigParameters();

        final SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefs.clear();

        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);

        for (final TrackerConfigParameter parameter : configParameters) {
            if (parameter.type == TrackerConfigParameter.BOOL) {
                prefs.putBoolean(TRACKER_SHARED_PREFERENCES_PREFIX
                        + parameter.name, Boolean.valueOf(parameter.defaultValue));
            } else {
                prefs.putString(TRACKER_SHARED_PREFERENCES_PREFIX + parameter.name, parameter.defaultValue);
            }
        }

        // reset has_gyroscope flag to device default setting
        prefs.putBoolean(TRACKER_SHARED_PREFERENCES_PREFIX + TrackerManager.HAS_GYROSCOPE,
                TrackerManager.getInstance().hasGyroscope());

        prefs.commit();
    }
}

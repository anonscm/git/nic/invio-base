package de.tarent.invio.android.admin;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import de.tarent.invio.android.admin.fingerprint.AdminFingerprintManager;

/**
 * A scanning dialog that shows two progress bars, one for wifi and one for bluetooth.
 */
public class ScanningDialog extends Dialog {

    private MapActivity mapActivity = null;

    private ProgressBar wifiProgressBar;
    private TextView wifiProgressFraction;
    private TextView wifiProgressPercent;
    private ProgressBar btleProgressBar;
    private TextView btleProgressPercent;
    private TextView btleProgressFraction;

    private boolean scanningWifi = true;
    private boolean scanningBtle = true;

    /**
     * Constructor.
     *
     * @param context the {@link Context}. This should be an instance of the {@link MapActivity}.
     */
    public ScanningDialog(final Context context) {
        super(context);

        if (context instanceof MapActivity) {
            this.mapActivity = (MapActivity) context;
        }

        setContentView(R.layout.scanning_dialog);
        setCanceledOnTouchOutside(false);
        setTitle(R.string.scanning_dialog_title);

        scanningWifi = mapActivity.scanWifiFingerprint();
        scanningBtle = mapActivity.scanBtleFingerprint();
        initWifiProgress();
        initBtleProgress();
        initCancelScanButton();

        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(final DialogInterface dialog) {
                stopScanning();
            }
        });
        mapActivity.setScanningDialog(this);
    }

    private void initWifiProgress() {
        wifiProgressBar = (ProgressBar) findViewById(R.id.wifiProgress);
        wifiProgressPercent = (TextView) findViewById(R.id.wifiProgressPercent);
        wifiProgressFraction = (TextView) findViewById(R.id.wifiProgressFraction);

        if (!scanningWifi) {
            final TextView wifiTitle = (TextView) findViewById(R.id.wifiTitle);
            wifiTitle.setVisibility(View.GONE);
            wifiProgressBar.setVisibility(View.GONE);
            wifiProgressBar = null;
            wifiProgressFraction.setVisibility(View.GONE);
            wifiProgressPercent.setVisibility(View.GONE);
        }
    }

    private void initBtleProgress() {
        btleProgressBar = (ProgressBar) findViewById(R.id.btleProgress);
        btleProgressPercent = (TextView) findViewById(R.id.btleProgressPercent);
        btleProgressFraction = (TextView) findViewById(R.id.btleProgressFraction);

        if (!scanningBtle) {
            final TextView btleTitle = (TextView) findViewById(R.id.btleTitle);
            btleTitle.setVisibility(View.GONE);
            btleProgressBar.setVisibility(View.GONE);
            btleProgressBar = null;
            btleProgressFraction.setVisibility(View.GONE);
            btleProgressPercent.setVisibility(View.GONE);
        }
    }

    private void initCancelScanButton() {
        final Button cancelButton = (Button) findViewById(R.id.cancelScanButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopScanning();
            }
        });
    }

    private void stopScanning() {
        final AdminFingerprintManager adminFingerprintManager
                = (AdminFingerprintManager) mapActivity.getFingerprintManager();
        adminFingerprintManager.stopCapturingFingerprint(true, true);
        dismiss();
    }

    public ProgressBar getWifiProgressBar() {
        return wifiProgressBar;
    }

    public void setWifiProgressBar(final ProgressBar wifiProgressBar) {
        this.wifiProgressBar = wifiProgressBar;
    }

    /**
     * Sets the {@link #wifiProgressFraction} text. This value consists of the current scan iteration and the max
     * iteration. These values are used to create a fraction string (current / max).
     *
     * @param current the current iteration
     * @param max     the maximum iteration
     */
    public void setWifiProgressFraction(final int current, final int max) {
        wifiProgressFraction.setText(current + " / " + max);
    }

    /**
     * Sets the {@link #wifiProgressPercent} text. Only an integer is required and a percent sign (%) is added to the
     * value.
     *
     * @param progressPercent the progress percent
     */
    public void setWifiProgressPercent(final int progressPercent) {
        wifiProgressPercent.setText(progressPercent + "%");
    }

    public ProgressBar getBtleProgressBar() {
        return btleProgressBar;
    }

    public void setBtleProgressBar(final ProgressBar btleProgressBar) {
        this.btleProgressBar = btleProgressBar;
    }

    /**
     * Sets the {@link #wifiProgressFraction} text. This value consists of the current scan iteration and the max
     * iteration. These values are used to create a fraction string (current / max).
     *
     * @param current the current iteration
     * @param max     the maximum iteration
     */
    public void setBtleProgressFraction(final int current, final int max) {
        btleProgressFraction.setText(current + " / " + max);
    }

    /**
     * Sets the {@link #btleProgressPercent} text. Only an integer is required and a percent sign (%) is added to the
     * value.
     *
     * @param progressPercent the progress percent
     */
    public void setBtleProgressPercent(final int progressPercent) {
        btleProgressPercent.setText(progressPercent + "%");
    }
}

package de.tarent.invio.android.admin;

import android.app.AlertDialog;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;
import de.tarent.invio.android.admin.fingerprint.AdminFingerprintManager;
import de.tarent.invio.android.base.App;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;

/**
 * The CaptureMapView encapsulates the MapView so that we can add our own touch events to the view.
 */
public class CaptureMapView extends MapView {
    private MapActivity mapActivity;
    private CaptureMode captureMode = CaptureMode.Fingerprint;

    // when recording fingerprints, this indicates whether we're measuring wifi (step 0) or btle (step 1)

    // this is a hack and should be removed when we rework the fingerprint recording to a single
    // progress dialog with an explicit cancel button.
    private enum RecordingMode { WIFI, BTLE }
    private RecordingMode recordingMode;

    /**
     * Constructor.
     *
     * @param context the {@link Context}
     * @param attrs   the {@link AttributeSet}
     */
    public CaptureMapView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        mapActivity = (MapActivity) context;
    }

    public void setCaptureMode(final CaptureMode captureMode) {
        this.captureMode = captureMode;
    }

    /**
     * Start capturing a fingerprint in the middle of the framelayout (which is the middle of the crosshair).
     */
    public void captureFingerprint() {
        final Projection projection = getProjection();

        final FrameLayout frameLayout = (FrameLayout) mapActivity.findViewById(R.id.framelayout);
        final int centerX = frameLayout.getWidth() / 2;
        final int centerY = frameLayout.getHeight() / 2;

        final IGeoPoint currentPoint = projection.fromPixels(centerX, centerY);
        if (captureMode == CaptureMode.Fingerprint) {
            startCapturing(currentPoint);
        } else if (captureMode == CaptureMode.GroundTruth) {
            mapActivity.addGroundTruthPoint(currentPoint);
        }
    }

    /**
     * Start the fingerprint capture process.
     *
     * @param currentPoint the current point where the user ended the long press
     */
    public void startCapturing(final IGeoPoint currentPoint) {
        final AdminFingerprintManager adminFingerprintManager =
                (AdminFingerprintManager) mapActivity.getFingerprintManager();

        if (adminFingerprintManager != null && !adminFingerprintManager.isCapturing() &&
                (mapActivity.scanWifiFingerprint() || mapActivity.scanBtleFingerprint())) {

            adminFingerprintManager.setScanCancelled(false);
            mapActivity.setScanningDialog(new ScanningDialog(mapActivity));
            mapActivity.getScanningDialog().show();

            adminFingerprintManager.determineFingerprintId();

            final WifiManager wifi = (WifiManager) App.getContext().getSystemService(Context.WIFI_SERVICE);

            if (wifi.isWifiEnabled() && wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLED
                    && mapActivity.scanWifiFingerprint()) {

                this.recordingMode = RecordingMode.WIFI;
                adminFingerprintManager.createWifiFingerprint(currentPoint);
            }

            if (mapActivity.scanBtleFingerprint()) {
                initScanBtleTask(currentPoint, adminFingerprintManager);
            }

        } else {
            showWifiAndBtleScanningOffDialog();
        }
    }

    private void initScanBtleTask(final IGeoPoint currentPoint, final AdminFingerprintManager adminFingerprintManager) {
        final AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                if (Looper.myLooper() == null) {
                    Looper.prepare();
                }

                while (adminFingerprintManager.isCapturing()) {
                    // Loop through while the adminFingerprintManager is still capturing.
                    try {
                        Thread.sleep(1000);
                    } catch (final InterruptedException e) {
                        Log.e("CaptureMapView", e.toString());
                    }
                }

                CaptureMapView.this.recordingMode = RecordingMode.BTLE;

                if (!adminFingerprintManager.isCapturing() && mapActivity.scanBtleFingerprint()) {
                    if (!adminFingerprintManager.isScanCancelled()) {
                        mapActivity.getScanningDialog().setWifiProgressBar(null);
                        adminFingerprintManager.createBluetoothLEFingerprint(currentPoint);
                    }
                }

                return null;
            }
        };

        task.execute();
    }

    private void showWifiAndBtleScanningOffDialog() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mapActivity);
        alertDialogBuilder.setTitle(R.string.wifi_btle_deactivated_title);
        alertDialogBuilder.setMessage(R.string.wifi_btle_deactivated_message);
        alertDialogBuilder.setPositiveButton(R.string.dialog_button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * The {@link CaptureMode}.
     */
    public enum CaptureMode {
        Fingerprint,
        GroundTruth,
    }
}

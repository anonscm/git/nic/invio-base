package de.tarent.invio.android.base.fingerprint;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.R;
import de.tarent.invio.entities.Fingerprint;

/**
 * Drawable class for drawing the fingerprint icon on the canvas.
 */
public class FingerprintDrawable extends Drawable {
    private static final int PADDING_HORIZONTAL_ICON = 1;
    private static final int PADDING_HORIZONTAL_TEXT = 2;
    private static final int PADDING_VERTICAL = 3;

    private static final Bitmap FINGERPRINT_BITMAP = BitmapFactory.decodeResource(
            App.getContext().getResources(), R.drawable.fingerprint);
    private static final Bitmap WIFI_BITMAP = BitmapFactory.decodeResource(
            App.getContext().getResources(), R.drawable.wifi);
    private static final Bitmap BLUETOOTH_BITMAP = BitmapFactory.decodeResource(
            App.getContext().getResources(), R.drawable.bluetooth);

    /**
     * The alpha colour value of 255 will set the full colour, which is black in our case.
     */
    private static final int ICON_ALPHA = 255;

    /**
     * The alpha colour value of 80 will set partial colour, which is grey in our case. A higher value will be closer
     * to black and a lower value will be closer to white.
     */
    private static final int ICON_ALPHA_DISABLED = 80;

    /**
     * this rect is the region of the image without the arrow
     * if you replace the fingerprint image you probably need to adjust these bounds
     * the region operates in a normalized number space of [0, 1]
     * so that it is independent of the resolution of the image
     */
    private static final RectF IMAGE_CROP = new RectF(
            0.0f,
            0.0f,
            1.0f,
            0.66f);

    private String wifiHistogramCount = "";
    private String bluetoothHistogramCount = "";
    private boolean hasWifiFingerprint = false;
    private boolean hasBluetoothFingerprint = false;

    private Paint paint = null;
    private int textHeight;
    private Rect croppedBounds;
    private final Rect reuseRect = new Rect();

    /**
     * Composes a {@link Drawable} object from one or two fingerprints to be used with a MapView as a marker.
     *
     * @param wifiFingerprint      the wifi fingerprint, can be null
     * @param bluetoothFingerprint the bluetooth fingerprint, can be null
     */
    public FingerprintDrawable(final Fingerprint wifiFingerprint, final Fingerprint bluetoothFingerprint) {
        if (wifiFingerprint != null && wifiFingerprint.getHistogram() != null) {
            hasWifiFingerprint = true;
            wifiHistogramCount = Integer.toString(wifiFingerprint.getHistogram().size());
        }

        if (bluetoothFingerprint != null && bluetoothFingerprint.getHistogram() != null) {
            hasBluetoothFingerprint = true;
            bluetoothHistogramCount = Integer.toString(bluetoothFingerprint.getHistogram().size());
        }

        paint = new Paint();
        paint.setTextSize(20);
        paint.setAntiAlias(true);
        paint.setFakeBoldText(true);

        computeTextHeight();
    }

    private void computeTextHeight() {
        final Rect outRect = new Rect();
        final String glyphs = wifiHistogramCount + bluetoothHistogramCount;
        paint.getTextBounds(glyphs, 0, glyphs.length(), outRect);
        textHeight = outRect.height();
    }

    @Override
    protected void onBoundsChange(final Rect bounds) {
        super.onBoundsChange(bounds);

        croppedBounds = new Rect(
                bounds.left + Math.round((float) bounds.width() * IMAGE_CROP.left),
                bounds.top + Math.round((float) bounds.height() * IMAGE_CROP.top),
                bounds.left + Math.round((float) bounds.width() * IMAGE_CROP.right),
                bounds.top + Math.round((float) bounds.height() * IMAGE_CROP.bottom));
    }

    @Override
    public int getIntrinsicWidth() {
        return FINGERPRINT_BITMAP.getWidth();
    }

    @Override
    public int getIntrinsicHeight() {
        return FINGERPRINT_BITMAP.getHeight();
    }

    @Override
    public int getMinimumWidth() {
        return FINGERPRINT_BITMAP.getWidth();
    }

    @Override
    public int getMinimumHeight() {
        return FINGERPRINT_BITMAP.getHeight();
    }

    @Override
    public void draw(final Canvas canvas) {
        final Rect bounds = getBounds();
        canvas.drawBitmap(FINGERPRINT_BITMAP, null, bounds, paint);

        final int offsetX = PADDING_HORIZONTAL_TEXT;
        final int textX = croppedBounds.centerX() + offsetX;
        final int halfTextHeight = textHeight / 2;

        drawWifiBitmap(canvas);
        canvas.drawText(wifiHistogramCount, textX, reuseRect.exactCenterY() + halfTextHeight, paint);

        drawBluetoothBitmap(canvas);
        canvas.drawText(bluetoothHistogramCount, textX, reuseRect.exactCenterY() + halfTextHeight, paint);
    }

    private void drawWifiBitmap(final Canvas canvas) {
        reuseRect.left = croppedBounds.centerX() - WIFI_BITMAP.getWidth() - PADDING_HORIZONTAL_ICON;
        reuseRect.right = croppedBounds.centerX() - PADDING_HORIZONTAL_ICON;
        reuseRect.top = croppedBounds.centerY() - WIFI_BITMAP.getHeight() - PADDING_VERTICAL;
        reuseRect.bottom = croppedBounds.centerY() - PADDING_VERTICAL;

        final int oldAlpha = paint.getAlpha();
        final int alpha = hasWifiFingerprint ? ICON_ALPHA : ICON_ALPHA_DISABLED;
        paint.setAlpha(alpha);

        canvas.drawBitmap(WIFI_BITMAP, null, reuseRect, paint);

        paint.setAlpha(oldAlpha);
    }

    private void drawBluetoothBitmap(final Canvas canvas) {
        reuseRect.left = croppedBounds.centerX() - BLUETOOTH_BITMAP.getWidth() - PADDING_HORIZONTAL_ICON;
        reuseRect.right = croppedBounds.centerX() - PADDING_HORIZONTAL_ICON;
        reuseRect.top = croppedBounds.centerY() + PADDING_VERTICAL;
        reuseRect.bottom = croppedBounds.centerY() + BLUETOOTH_BITMAP.getHeight() + PADDING_VERTICAL;

        final int oldAlpha = paint.getAlpha();
        final int alpha = hasBluetoothFingerprint ? ICON_ALPHA : ICON_ALPHA_DISABLED;
        paint.setAlpha(alpha);

        canvas.drawBitmap(BLUETOOTH_BITMAP, null, reuseRect, paint);

        paint.setAlpha(oldAlpha);
    }

    @Override
    public void setAlpha(final int alpha) {

    }

    @Override
    public void setColorFilter(final ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}

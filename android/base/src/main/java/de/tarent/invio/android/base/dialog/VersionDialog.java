package de.tarent.invio.android.base.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.widget.Toast;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.map.MapParams;

/**
 * This class contains methods to generate a version {@link Dialog} and its message.
 *
 * @author Atanas Alexandrov, tarent solutions GmbH
 * @author Andreas Grau, tarent solutions GmbH, 17.03.14
 */
public class VersionDialog {

    private Activity activity;

    /**
     * Create a new version {@link Dialog} for a specific {@link Activity}.
     *
     * @param activity The {@link Activity} the version {@link Dialog} should be used in.
     */
    public VersionDialog(final Activity activity) {
        this.activity = activity;
    }

    /**
     * Build the version {@link Dialog} .
     *
     * @return The version {@link Dialog}.
     */
    public Dialog buildVersionInformationDialog() {
        final String dialogMessage = getVersionMessage();

        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(activity, AlertDialog.THEME_HOLO_DARK);
        builder.setTitle(R.string.version_title)
                .setMessage(dialogMessage)
                .setNeutralButton(R.string.dialog_button_close, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        // User cancelled the dialog
                    }
                });

        if (!MapParams.isAdmin) { // TODO: PLEASE REMOVE AFTER PENNY POC!!!!!!
            builder.setPositiveButton("Toggle User Position", new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialog, final int id) {
                    MapParams.showingUserPosition = !MapParams.showingUserPosition;
                    Toast.makeText(activity, "Showing User Position: " + MapParams.showingUserPosition,
                            Toast.LENGTH_LONG).show();
                        }
                    });
        }

        return builder.create();
    }

    /**
     * Gets the message that will be shown in the version dialog box.
     *
     * @return the version message
     */
    protected String getVersionMessage() {
        final Resources res = activity.getResources();
        final String appName = res.getString(R.string.app_name);
        final String versionNumber = res.getString(R.string.version);
        final String buildId = res.getString(R.string.build_id);
        final String buildDate = res.getString(R.string.build_date);
        final String trackerNumber = TrackerManager.getInstance().version();
        final String appVersion = res.getString(R.string.version_app, appName, versionNumber, buildId, buildDate);
        final String trackerVersion = res.getString(R.string.version_tracker, trackerNumber);

        return appVersion + "\n\n" + trackerVersion;
    }
}
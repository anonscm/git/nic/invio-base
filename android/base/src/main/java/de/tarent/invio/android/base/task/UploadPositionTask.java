package de.tarent.invio.android.base.task;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.map.MapParams;
import de.tarent.invio.android.base.utils.DeviceInformation;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This is the AsyncTask which uploads the position to the server, in the background.
 */
public class UploadPositionTask extends AsyncTask {

    private static final String TAG = "UploadPositionTask";

    private static UploadPositionTask instance;

    private final MapServerClient mapServerClient;

    private final String mapName;

    private Timer timer;

    private final Activity activity;

    private String customerStatus = "SHOPPING";

    private OnUploadPositionListener onUploadPositionListener = null;


    /**
     * Construct a new UploadPositionTask, use getInstance() if possible
     *
     * @param mapServerClient the {@link de.tarent.invio.mapserver.MapServerClient}
     * @param mapName         the name of the map to which we shall upload the fingerprints
     * @param activity        the activity to get the context
     */
    public UploadPositionTask(final MapServerClient mapServerClient,
                              final String mapName,
                              final Activity activity) {
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
        this.activity = activity;
    }


    /**
     * Returns the current instance or replaces it with a new one if the given values differ
     *
     * @param mapServerClient the {@link de.tarent.invio.mapserver.MapServerClient}
     * @param mapName         the name of the map to which we shall upload the fingerprints
     * @param activity        the activity to get the context
     * @return the current instance
     */
    public static UploadPositionTask getInstance(final MapServerClient mapServerClient,
                                                 final String mapName,
                                                 final Activity activity) {
        if (instance == null) {
            instance = new UploadPositionTask(mapServerClient, mapName, activity);
        }
        return instance;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected Object doInBackground(final Object... params) {
        timer = new Timer("postionUploadTimer");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final InvioGeoPoint position = TrackerManager.getInstance().getCurrentPosition();
                if (position != null && mapServerClient != null) {
                    doUpload(position);
                }
            }
        }, 0, 200);
        return null;
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (timer != null) {
            timer.cancel();
        }
    }


    private void doUpload(final InvioGeoPoint position) {
        try {
            final int longitudeE6 = position.getLongitudeE6();
            final int latitudeE6 = position.getLatitudeE6();

            if (latitudeE6 > 0 && longitudeE6 > 0) {
                final String clientId = new DeviceInformation(activity).getCombinedDeviceIdAsSHA1String();

                if (onUploadPositionListener != null){
                    onUploadPositionListener.onUploadPosition();
                }

                mapServerClient.uploadPositionData(
                        mapName, clientId, latitudeE6, longitudeE6, MapParams.getLevel(), customerStatus
                );
            }
        } catch (final InvioException e) {
            Log.e(TAG, "Failed to upload user position: " + e);
        } catch (final IOException e) {
            Log.e(TAG, "Failed to upload user position: " + e);
        }
    }


    public void setCustomerStatus(final String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public void setOnUploadPositionListener(final OnUploadPositionListener onUploadPositionListener) {
        this.onUploadPositionListener = onUploadPositionListener;
    }

    /**
     * Listener that gets called when the user position gets uploaded to the remote server
     */
    public interface OnUploadPositionListener {
        /**
         * Gets called when a new user position gets uploaded
         */
        void onUploadPosition();
    }

}

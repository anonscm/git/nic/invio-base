package de.tarent.invio.android.base.notifications;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.utils.HttpUtils;
import de.tarent.invio.entities.InvioGeoPoint;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Map;

/**
 * Class that represents a custom push notification to display various text messages.
 */
public class PushNotification implements Comparable<PushNotification> {
    private static final int MAX_IMAGE_WIDTH = 512;
    private static final int MAX_IMAGE_HEIGHT = 512;

    private static final String DEFAULT_NOTIFICATION_LANGUAGE = "en";

    // these fields are used by gson via reflection, so all Sonar complaints are invalid
    private String id; //NOSONAR
    private boolean active; //NOSONAR
    private int priority; //NOSONAR
    private PushNotificationRectangle rectangle; //NOSONAR
    private Map<String, String> text; //NOSONAR
    private PushNotificationSettings settings; //NOSONAR
    private String imageUrl = null; //NOSONAR

    private transient Bitmap image = null;

    private int fireCount = 0;

    /**
     * Displays the push notification using the standard android dialog window.
     * <p/>
     * Currently only displays a maximum of 1 time per app lifetime.
     *
     * @param activity The activity in which the dialog should be shown.
     */
    public void fire(final Activity activity) {
        if (text == null || activity == null) { //NOSONAR
            return;
        }

        // get the device locale and language
        final Locale locale = activity.getResources().getConfiguration().locale;
        String language = locale != null ? locale.getLanguage() : "";
        if (language.length() == 0) {
            language = DEFAULT_NOTIFICATION_LANGUAGE;
        }

        // get notification for this language
        String str = text.get(language);
        if (str == null) {
            str = text.get(DEFAULT_NOTIFICATION_LANGUAGE);
        }

        if (str != null) {
            final String notificationText = str;

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialog(activity, notificationText);
                }
            });

            ++fireCount;

            if (fireCount >= settings.getFrequencyCap()) {
                active = false;
            }
        }
    }

    private void showDialog(final Context context, final String text) {
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.notification_dialog, null);

        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        if(image != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(image);
        } else {
            imageView.setVisibility(View.GONE);
        }

        final TextView textView = (TextView)view.findViewById(R.id.textView);
        textView.setText(Html.fromHtml(text));

        new AlertDialog.Builder(context)
                .setPositiveButton(context.getString(R.string.dialog_button_ok), null)
                .setCancelable(true)
                .setView(view)
                .create()
                .show();
    }

    /**
     * Downloads the image from the imageUrl to memory and downsample it if the image is too big.
     */
    public void downloadImage() {
        try {
            if(imageUrl != null) {
                final OkHttpClient httpClient = HttpUtils.createOkHttpClient(true);
                final BitmapFactory.Options options = new BitmapFactory.Options();

                //first only decode bounds of image
                decodeImage(httpClient, options, true);

                //calculate a matching sample size to downsample the image if necessary
                options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight,
                        MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT);

                //now download and decode the whole image
                image = decodeImage(httpClient, options, false);
            }
        } catch (final IOException e) {
            Log.e(getClass().getSimpleName(), "Caught exception: " + e.getMessage());
            Log.e(getClass().getSimpleName(), "Download of image from + " + imageUrl + " failed");
        }
    }

    private Bitmap decodeImage(final OkHttpClient httpClient,
                               final BitmapFactory.Options options,
                               final boolean justDecodeBounds) throws IOException {
        options.inJustDecodeBounds = justDecodeBounds;

        final Request request = new Request.Builder().url(imageUrl).build();
        final InputStream is = httpClient.newCall(request).execute().body().byteStream();

        final Bitmap bitmap = BitmapFactory.decodeStream(is, null, options);
        IOUtils.closeQuietly(is);

        return bitmap;
    }

    /**
     * @return true if the notification has an image and it is downloaded, otherwise false
     */
    public boolean isImageDownloaded(){
        return imageUrl == null || image != null;
    }

    /**
     * Calculates a sample size (always a power of two). Used for downsampling of images.
     *
     * Example: Input a 1024x1024 image (srcWidth = 1024, srcHeight = 1024)
     * Desired image width 256x256 (dstWidth = 256, dstHeight = 256)
     *
     * Output: a sample size of 4
     *
     * @param srcWidth the source width
     * @param srcHeight the source height
     * @param dstWidth the desired width
     * @param dstHeight the desired height
     *
     * @return a power of two sample size
     */
    private int calculateSampleSize(final int srcWidth, final int srcHeight,
                                    final int dstWidth, final int dstHeight) {
        int sampleSize = 1;

        while ((srcWidth / sampleSize) > dstWidth
                && (srcHeight / sampleSize) > dstHeight) {
            sampleSize *= 2;
        }

        return sampleSize;
    }

    /**
     * Method to check if this notification is valid for a given point
     * i.e., is the point within notification rectangle, and is the notification active
     *
     * @param geoPoint some InvioGeoPoint
     * @return true if the geoPoint is in the rectangle and the notification is active, otherwise false.
     */
    public boolean isValidForLocation(final InvioGeoPoint geoPoint) {
        return active && rectangle != null && rectangle.containsPoint(geoPoint); //NOSONAR
    }

    @Override
    public int compareTo(final PushNotification other) {
        return priority - other.priority;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final PushNotification that = (PushNotification) o;

        return !(id != null ? !id.equals(that.id) : that.id != null); //NOSONAR
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public PushNotificationRectangle getRectangle() {
        return rectangle;
    }

    public PushNotificationSettings getSettings() {
        return settings;
    }

    public void setSettings(final PushNotificationSettings settings) {
        this.settings = settings;
    }
}

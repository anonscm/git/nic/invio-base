package de.tarent.invio.android.base.task;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * The {@link ZipMapDataTask} will try to parse the concrete map data from the unzipped group directory.
 */
public class ZipMapDataTask extends DownloadTask<Void, Void, Map<String, Collection>> {

    /**
     * The concrete map directory inside the unzipped group data directory
     */
    private final File unzippedMapDir;

    /**
     * The {@link OsmParser}
     */
    private final OsmParser parser;


    /**
     * Constructor.
     *
     * @param downloadListener where to push the map data
     * @param unzippedMapDir   directory of the concrete unzipped map
     * @param parser           the parser that shall be used to parse the map data
     */
    public ZipMapDataTask(final DownloadListener<Map<String, Collection>> downloadListener,
                          final File unzippedMapDir,
                          final OsmParser parser) {
        downloadListeners.add(downloadListener);
        this.unzippedMapDir = unzippedMapDir;
        this.parser = parser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Collection> doInBackground(final Void... params) {
        final Map<String, Collection> result = new HashMap<String, Collection>();
        try {
            parser.parse(getXmlStream());
            result.putAll(parser.getResults());
            success = true;
        } catch (final IOException e) {
            success = false;
            // TODO: what should we do?
        }

        return result;
    }

    /**
     * Get the .osm file from the unzipped directory as a stream
     *
     * @return .osm file as a stream
     * @throws FileNotFoundException if file were not found
     */
    protected InputStream getXmlStream() throws FileNotFoundException {
        final String mapDataPath = unzippedMapDir + File.separator + "data" + File.separator +
                unzippedMapDir.getName() + ".osm";
        final File mapDataFile = new File(mapDataPath);
        if (mapDataFile.exists()) {
            return new FileInputStream(mapDataFile);
        } else {
            throw new FileNotFoundException("ERROR: No map data found! Was expected here: " + mapDataPath);
        }

    }
}

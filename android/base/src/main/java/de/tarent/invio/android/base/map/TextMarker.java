package de.tarent.invio.android.base.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.text.TextPaint;
import android.util.TypedValue;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import de.tarent.invio.android.base.App;

/**
 * Generates a {@link Marker} that displays text to use on the google map.
 */
public class TextMarker {

    private static final int PADDING = 2;
    private static final int DEFAULT_TEXT_SIZE = 20;

    private String text;
    private TextPaint paint;
    private Marker marker;

    /**
     * Constructor.
     *
     * @param map  the {@link GoogleMap}
     * @param text the text
     */
    public TextMarker(final GoogleMap map, final String text) {
        this(map, text, DEFAULT_TEXT_SIZE, Color.WHITE);
    }

    /**
     * Constructor.
     *
     * @param map       the {@link GoogleMap}
     * @param text      the text
     * @param textSize  the text size
     * @param textColor the text colour
     */
    public TextMarker(final GoogleMap map, final String text, final int textSize, final int textColor) {
        paint = new TextPaint();

        setTextSize(textSize);

        paint.setAlpha(255);
        paint.setAntiAlias(true);
        paint.setShadowLayer(1, 1, 1, Color.BLACK);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
        paint.setColor(textColor);
        this.marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(0, 0))
                .anchor(0.5f, 0.5f)
                .visible(false));

        this.text = text;

        invalidate();
    }

    private void invalidate() {
        if (text != null) {
            final Paint.FontMetricsInt fontMetrics = paint.getFontMetricsInt();

            final int width = (int) paint.measureText(text) + PADDING * 2;
            final int height = (int) paint.getTextSize() + PADDING * 2;

            final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);

            final Canvas canvas = new Canvas(bitmap);
            canvas.drawARGB(0, 0, 0, 0);

            int descent = fontMetrics.descent;

            if (text.matches("[0-9]+")) {
                descent = fontMetrics.descent / 2;
            }

            canvas.drawText(text, PADDING, bitmap.getHeight() - descent - PADDING, paint);

            marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap));
            marker.setVisible(true);
        }
    }

    /**
     * Sets the {@link #marker}s position.
     *
     * @param position the {@link LatLng} position
     */
    public void setPosition(final LatLng position) {
        marker.setPosition(position);
    }

    public String getText() {
        return text;
    }

    /**
     * Returns whether or not the {@link #marker} is visible.
     *
     * @return returns true if the {@link #marker} is visible
     */
    public boolean isVisible() {
        return marker.isVisible();
    }

    /**
     * Sets the {@link #marker} visibility.
     *
     * @param visible the visibility
     */
    public void setVisible(final boolean visible) {
        marker.setVisible(visible);
    }

    /**
     * Returns the {@link #marker}s id.
     *
     * @return the {@link #marker}s id
     */
    public String getId() {
        return marker.getId();
    }

    /**
     * Returns the {@link #marker}s rotation.
     *
     * @return the {@link #marker}s rotation
     */
    public float getRotation() {
        return marker.getRotation();
    }

    /**
     * Returns the {@link #marker}s alpha.
     *
     * @return the {@link #marker}s alpha
     */
    public float getAlpha() {
        return marker.getAlpha();
    }

    /**
     * Returns whether or not the {@link #marker} is draggable.
     *
     * @return returns true if the {@link #marker} is draggable
     */
    public boolean isDraggable() {
        return marker.isDraggable();
    }

    /**
     * Sets the {@link #marker}s flat.
     *
     * @param flat the new flat
     */
    public void setFlat(final boolean flat) {
        marker.setFlat(flat);
    }

    /**
     * Sets the {@link #marker}s anchor.
     *
     * @param anchorU the first anchor
     * @param anchorV the second anchor
     */
    public void setAnchor(final float anchorU, final float anchorV) {
        marker.setAnchor(anchorU, anchorV);
    }

    /**
     * Returns the {@link #marker}s position.
     *
     * @return the {@link #marker}s position
     */
    public LatLng getPosition() {
        return marker.getPosition();
    }

    /**
     * Returns whether or not the {@link #marker} is flat.
     *
     * @return returns true if the {@link #marker} is flat
     */
    public boolean isFlat() {
        return marker.isFlat();
    }

    /**
     * Sets the {@link #marker}s text size.
     *
     * @param textSize the text size
     */
    private void setTextSize(final int textSize) {
        final int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                textSize, App.getContext().getResources().getDisplayMetrics());

        paint.setTextSize(pixel);
    }

    /**
     * Sets the {@link #marker}s rotation.
     *
     * @param rotation the rotation
     */
    public void setRotation(final float rotation) {
        marker.setRotation(rotation);
    }

    /**
     * Sets whether or not the {@link #marker} should be draggable.
     *
     * @param draggable true if the {@link #marker} should be draggable
     */
    public void setDraggable(final boolean draggable) {
        marker.setDraggable(draggable);
    }

    /**
     * Sets the {@link #marker}s alpha.
     *
     * @param alpha the new alpha
     */
    public void setAlpha(final float alpha) {
        marker.setAlpha(alpha);
    }

    Marker getMarker() {
        return marker;
    }
}

package de.tarent.invio.android.base.utils;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utils for working with zip files
 */
public final class ZipUtils {

    private ZipUtils() {

    }

    /**
     * Compresses a directory to a .zip file.
     *
     * @param folder the directory you want to compress
     * @param zipFile the resulting name of the zip file
     * @throws IOException exception
     */
    public static void zipDirectory(final File folder, final File zipFile) throws IOException {
        final ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
        processFolder(folder, zipOutputStream, folder.getPath().length() + 1);
        zipOutputStream.close();
    }

    private static void processFolder(final File folder, final ZipOutputStream zipOutputStream, final int prefixLength)
            throws IOException {
        for (final File file : folder.listFiles()) {
            if (file.isFile()) {
                final ZipEntry zipEntry = new ZipEntry(file.getPath().substring(prefixLength));
                zipOutputStream.putNextEntry(zipEntry);

                final FileInputStream inputStream = new FileInputStream(file);
                IOUtils.copy(inputStream, zipOutputStream);
                zipOutputStream.closeEntry();
            } else if (file.isDirectory()) {
                processFolder(file, zipOutputStream, prefixLength);
            }
        }
    }
}

package de.tarent.invio.android.base.utils;

import de.tarent.invio.entities.InvioGeoPoint;

/**
 * An edge represents a straight line between two {@link InvioGeoPoint}'s points.
 *
 * @author Atanas Alexandrov, tarent solutions GmbH
 */
public class Edge {

    private final InvioGeoPoint pointA;
    private final InvioGeoPoint pointB;

    /**
     * Create an Edge defined by the given points A and B.
     *
     * @param pointA the startingpoint of the edge
     * @param pointB the endpoint of the edge
     */
    public Edge(final InvioGeoPoint pointA, final InvioGeoPoint pointB) {
        this.pointA = pointA;
        this.pointB = pointB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass())) {
            return false;
        }
        final Edge edge = (Edge) o;
        if ((pointA != null) ? (!pointA.equals(edge.pointA)) : (edge.pointA != null)) {
            return false;
        }
        if ((pointB != null) ? (!pointB.equals(edge.pointB)) : (edge.pointB != null)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = (pointA != null) ? pointA.hashCode() : 0;
        result = 31 * result + ((pointB != null) ? pointB.hashCode() : 0);
        return result;
    }


    public InvioGeoPoint getPointA() {
        return pointA;
    }

    public InvioGeoPoint getPointB() {
        return pointB;
    }

}
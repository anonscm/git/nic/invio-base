package de.tarent.invio.android.base.map;

import android.util.Log;
import de.tarent.invio.android.base.config.MapProperties;
import de.tarent.invio.android.base.task.DownloadListener;
import de.tarent.invio.android.base.task.DownloadTask;

/**
 * Class to manage the {@link MapProperties}. Implements {@link DownloadListener} so that the map properties can
 * be set after the download is completed. This will also prevent the main thread from continuing while the download is
 * happening.
 */
public class MapPropertiesManager implements DownloadListener<MapProperties> {

    private static final String TAG = "MapPropertyManager";

    /**
     * The {@link MapProperties}.
     */
    protected MapProperties mapProperties;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDownloadFinished(final DownloadTask task, final boolean success, final MapProperties data) {
        if (success) {
            setMapProperties(data);
        } else {
            Log.e(TAG, "Map properties download failed.");
        }
    }

    public MapProperties getMapProperties() {
        return mapProperties;
    }

    public void setMapProperties(final MapProperties mapProperties) {
        this.mapProperties = mapProperties;
    }
}

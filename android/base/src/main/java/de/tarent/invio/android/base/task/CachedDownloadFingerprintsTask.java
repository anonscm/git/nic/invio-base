package de.tarent.invio.android.base.task;

import android.os.Environment;
import android.util.Log;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * The CachedDownloadFingerprintsTask tries do download the fingerprints and store it locally. When the download failed
 * the file that was stored last time will be used. So it is more of a "fallback" than a "cache"...
 */
public class CachedDownloadFingerprintsTask extends DownloadFingerprintsTask {

    private static final String APP_FOLDER = "sellfio_" + App.getContext().getString(R.string.app_name);

    private static final String TRANSMITTER_DATA_FILE_NAMES = "transmitter_data";
    private static final String FINGERPRINTS_DATA_FILE_WIFI = "fingerprints_data";
    private static final String FINGERPRINTS_DATA_FILE_BLUETOOTH_LE = "fingerprints_btle_data";

    private static final String DOWNLOAD_FAILED = "Download/persistence of fingerprints failed for ";

    /**
     * Construct a new {@link CachedDownloadFingerprintsTask}, which belongs to a specific FingerprintManager
     *
     * @param fingerprintManager the owner of this task
     * @param mapServerClient    the client which should be used for talking to the map-server
     * @param mapName            the name of the map for which we shall download the fingerprints
     */
    public CachedDownloadFingerprintsTask(final FingerprintManager fingerprintManager,
                                          final MapServerClient mapServerClient,
                                          final String mapName) {
        super(fingerprintManager, mapServerClient, mapName);
    }

    /**
     * Get the json with the fingerprints from somewhere. It will first try download it from the server and store it
     * locally. Regardless of the download success it will then provide the locally stored file, which may or may not
     * have been updated.
     * TODO: maybe check for updates before downloading the whole file again...
     *
     * @return the InputStream from which the json can be read.
     * @throws java.io.FileNotFoundException when the cached json file cannot be read
     */
    @Override
    protected InputStream getWifiFingerprintInputStream() throws FileNotFoundException {
        final File cache = new File(Environment.getExternalStorageDirectory() + File.separator +
                APP_FOLDER + File.separator + mapName + File.separator + FINGERPRINTS_DATA_FILE_WIFI);

        try {
            final String json = mapServerClient.downloadWifiFingerprintsDataWithCount(mapName);
            persistFingerprintsJson(json, cache);
        } catch (final IOException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        } catch (final InvioException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        }

        return new FileInputStream(cache);
    }


    /**
     * Get the json with the transmitter names from somewhere. It will first try download it from the server and store it
     * locally. Regardless of the download success it will then provide the locally stored file, which may or may not
     * have been updated.
     * TODO: maybe check for updates before downloading the whole file again...
     *
     * @return the InputStream from which the json can be read.
     * @throws java.io.FileNotFoundException when the cached json file cannot be read
     */
    @Override
    protected InputStream getTransmitterNamesInputStream() throws FileNotFoundException {
        final File cache = new File(Environment.getExternalStorageDirectory() + File.separator +
                APP_FOLDER + File.separator + mapName + File.separator + TRANSMITTER_DATA_FILE_NAMES);

        try {
            final String json = mapServerClient.downloadTransmitterNames(mapName);
            persistFingerprintsJson(json, cache);
        } catch (final IOException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        } catch (final InvioException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        }

        return new FileInputStream(cache);
    }


    @Override
    protected InputStream getWifiFingerprintProbabilityInputStream() throws FileNotFoundException {
        final File cache = new File(Environment.getExternalStorageDirectory() + File.separator +
                APP_FOLDER + File.separator + mapName + File.separator + FINGERPRINTS_DATA_FILE_WIFI);

        try {
            final String json = mapServerClient.downloadWifiFingerprintsData(mapName);
            persistFingerprintsJson(json, cache);
        } catch (final IOException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        } catch (final InvioException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        }

        return new FileInputStream(cache);
    }

    /**
     * Get the json with the fingerprints from somewhere. It will first try download it from the server and store it
     * locally. Regardless of the download success it will then provide the locally stored file, which may or may not
     * have been updated.
     * TODO: maybe check for updates before downloading the whole file again...
     *
     * @return the InputStream from which the json can be read.
     * @throws java.io.FileNotFoundException when the cached json file cannot be read
     */
    @Override
    protected InputStream getBluetoothLEFingerprintInputStream() throws FileNotFoundException {
        final File cache = new File(Environment.getExternalStorageDirectory() + File.separator +
                APP_FOLDER + File.separator + mapName + File.separator + FINGERPRINTS_DATA_FILE_BLUETOOTH_LE);

        try {
            final String json = mapServerClient.downloadBluetoothLEFingerprintsDataWithCount(mapName);
            persistFingerprintsJson(json, cache);
        } catch (final IOException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        } catch (final InvioException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        }

        return new FileInputStream(cache);
    }


    @Override
    protected InputStream getBluetoothLEFingerprintProbabilityInputStream() throws FileNotFoundException {
        final File cache = new File(Environment.getExternalStorageDirectory() + File.separator +
                APP_FOLDER + File.separator + mapName + File.separator + FINGERPRINTS_DATA_FILE_BLUETOOTH_LE);

        try {
            final String json = mapServerClient.downloadBluetoothLEFingerprintsData(mapName);
            persistFingerprintsJson(json, cache);
        } catch (final IOException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        } catch (final InvioException e) {
            Log.e(this.getClass().getName(), DOWNLOAD_FAILED + mapName, e);
        }

        return new FileInputStream(cache);
    }



    private void persistFingerprintsJson(final String json, final File cache) throws IOException {
        ensureThatCacheDirExists(cache);
        final FileOutputStream out = new FileOutputStream(cache);
        try {
            out.write(json.getBytes("UTF-8"));
        } finally {
            out.close();
        }
    }

    private void ensureThatCacheDirExists(final File cache) throws IOException {
        final File cacheDir = cache.getParentFile();
        if (!cacheDir.exists()) {
            if (cache.getParentFile().mkdirs()) {
                Log.d(getClass().getName(), "Created directory " + APP_FOLDER);
            } else {
                Log.e(getClass().getName(), "CacheDir could not be created. Can't save fingerprints_data.");
                throw new IOException("CacheDir could not be created. Can't save fingerprints_data.");
            }
        }
    }

}

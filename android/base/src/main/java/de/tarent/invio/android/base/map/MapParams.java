package de.tarent.invio.android.base.map;

/**
 * This class provides static information needed across various parts of the application concerning map properties.
 */
public final class MapParams {
    // TODO: PLEASE REMOVE AFTER PENNY POC!!!!!!
    public static boolean showingUserPosition = false; //NOSONAR - This is only temporary code.
    public static boolean isAdmin = false; //NOSONAR - This is only temporary code.

    static int level;
    private static MapParams instance = new MapParams();

    private MapParams() {
    }

    public static MapParams getInstance() {
        return instance;
    }

    public static void setIsAdmin(final boolean isAdmin) {
        MapParams.isAdmin = isAdmin;
    }

    public static int getLevel() {
        return level;
    }

    public static void setLevel(final int level) {
        MapParams.level = level;
    }
}

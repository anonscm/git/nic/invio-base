package de.tarent.invio.android.base.collector;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanFilter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.histogram.HistogramBuilder;
import de.tarent.invio.entities.Histogram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.UUID;

/**
 * The BluetoothLECollector records BluetoothLE-histograms.
 * The BluetoothLECollector has two modes:
 * - fingerprint-mode, for the offline-phase, where it does a number of scans, produces one histogram, and then stops.
 * - continuous-mode, for the online-phase, where it goes on scanning and producing histograms until it is stopped.
 * The mode is selected via the constructor's collectionMode argument.
 * <p/>
 * TODO:
 * - the number of scans could be made configurable.
 * - there should be a configurable timeout, in case the scanning takes much longer than expected.
 */
public abstract class BluetoothLECollector implements HistogramCollector {

    protected Context context;
    protected HistogramBuilder histogramBuilder;
    protected BluetoothAdapter bluetoothAdapter;

    protected int maxSeconds;               // how many seconds to scan. -1 for infinite scans
    protected double secondsCounter = 0;
    protected double secondsIncrement = 0;

    protected DeviceType deviceType = DeviceType.iBeacon;

    /**
     * The maximum age in milliseconds that a scan result may have to be included in the histogram.
     * A maxAge of 0 is used to disable age-filtering.
     */
    protected int maxAge;
    protected boolean bluetoothEnabled = false;
    protected CollectorListener collectorListener;
    protected boolean isScanning = false;

    protected Histogram lastHistogram;
    protected UUID proximityUUID = null;
    protected Timer timer = new Timer();

    protected Map<String, Integer> counts = new HashMap<String, Integer>();

    protected List<ScanFilter> scanFilters = new ArrayList<ScanFilter>();

    /**
     * Construct a new BluetoothLECollector
     *
     * @param collectionMode the collection mode to use
     */
    protected BluetoothLECollector(CollectionMode collectionMode) {
        this.context = App.getContext();
        this.maxAge = collectionMode == CollectionMode.CONTINUOUS ? CONTINUOUS_MAXAGE : 0;

        enableBluetoothLE();
    }

    /**
     * factory method for a BluetoothLECollector that uses the APIs appropriate for the device
     * @param collectionMode the collectionMode to use
     * @return a BluetoothLECollector instance
     */
    public static BluetoothLECollector createCollector(CollectionMode collectionMode)
    {
        final boolean android5 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

        if (android5) {
            return new BluetoothLECollectorLollipop(collectionMode);
        } else {
            return new BluetoothLECollectorJellyBean(collectionMode);
        }
    }

    public void setDeviceType(final DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public void setProximityUUID(final UUID proximityUUID) {
        this.proximityUUID = proximityUUID;
    }


    @Override
    public Histogram getHistogram() {
        return lastHistogram;
    }


    @TargetApi(18)
    private void enableBluetoothLE() {
        if (isBluetoothLESupported(context)) {

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
                bluetoothEnabled = true;
            }
        }
    }

    private boolean isBluetoothLESupported(final Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        }
        return false;
    }

    
    /**
     * Enum to differentiate between the two device types: BluetoothLE and iBeacons.
     */
    public enum DeviceType {
        BluetoothLE,
        iBeacon
    }
}

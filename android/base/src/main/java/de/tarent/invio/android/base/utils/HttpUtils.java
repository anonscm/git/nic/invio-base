package de.tarent.invio.android.base.utils;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import de.tarent.invio.android.base.App;

import java.io.File;

/**
 * Utility class for managing http connections and clients
 */
public final class HttpUtils {
    private static Cache httpCache = null;

    private static final int HTTP_CACHE_MAX_SIZE = 10 * 1024 * 1024; //10mb

    private HttpUtils(){

    }

    /**
     * Utility function to create a OkHttpClient.
     *
     * @param useCache Set to true, if you want http caching to be enabled.
     *
     * @return a new OkHttpClient
     */
    public static OkHttpClient createOkHttpClient(final boolean useCache) {
        final OkHttpClient client = new OkHttpClient();

        final File httpCacheDirectory = new File(App.getContext().getCacheDir(), "OkHttpCache");

        if (httpCache == null) {
            httpCache = new Cache(httpCacheDirectory, HTTP_CACHE_MAX_SIZE);
        }

        if(httpCache != null && useCache) {
            client.setCache(httpCache);
        }

        return client;
    }
}

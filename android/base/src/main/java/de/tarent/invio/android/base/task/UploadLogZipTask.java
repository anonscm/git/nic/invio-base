package de.tarent.invio.android.base.task;

import android.os.AsyncTask;
import android.util.Log;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;

import java.io.File;
import java.io.IOException;

/**
 * This is the AsyncTask which uploads the position to the server, in the background.
 */
public class UploadLogZipTask extends AsyncTask {

    private static final String TAG = "UploadLogZipTask";

    private final MapServerClient mapServerClient;

    private final String mapName;

    private final File zipFile;


    /**
     * Construct a new UploadLogZipTask,
     *
     * @param mapServerClient the {@link de.tarent.invio.mapserver.MapServerClient}
     * @param mapName         the name of the map to which we shall upload the zip file
     * @param zipFile         the zip file to be uploaded
     */
    public UploadLogZipTask(final MapServerClient mapServerClient, final String mapName, final File zipFile) {
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
        this.zipFile = zipFile;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected Object doInBackground(final Object... params) {
        try {
            mapServerClient.uploadLogZip(mapName, zipFile);
        } catch (InvioException e) {
            Log.e(TAG, "Failed to upload zipfile: " + e);
        } catch (IOException e) {
            Log.e(TAG, "Failed to upload zipfile: " + e);
        }

        return null;
    }

}

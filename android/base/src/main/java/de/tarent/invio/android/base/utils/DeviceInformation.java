package de.tarent.invio.android.base.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static android.os.Build.SERIAL;

/**
 * Class that for now generates the device id.
 */
public class DeviceInformation {

    private static final String TAG = DeviceInformation.class.getCanonicalName();

    /**
     * The algorithm we use for encoding.
     */
    protected String algorithm = "SHA-1";

    /**
     * The name of the charset we use for encoding.
     */
    protected String charsetName = "UTF-8";

    private final Context context;

    /**
     * Constructs a new {@link de.tarent.invio.android.base.utils.DeviceInformation} class.
     *
     * @param context the application context
     */
    public DeviceInformation(final Context context) {
        this.context = context;
    }

    /**
     * Here we try to produce a unique id for each device combining several identifiers into one string:
     * <ul>
     * <li>androidID - can be null without Google account</li>
     * <li>buildSerial - can be null or redundant</li>
     * <li>wifi mac address - can be null if wifi is off</li>
     * </ul>
     * As you can see some cases the combined ID can be null.
     *
     * @return combined device ID or null if all identifiers have returned null.
     */
    public String getCombinedDeviceIdAsSHA1String() {
        final String androidId = getAndroidId();
        final String buildSerial = getSerial();
        final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        final String wifiMacAddress = wifiManager.getConnectionInfo().getMacAddress();

        final StringBuilder result = new StringBuilder();
        if (androidId != null) {
            result.append(androidId);
        }
        if (buildSerial != null) {
            result.append(buildSerial);
        }
        if (wifiMacAddress != null) {
            result.append(wifiMacAddress.replaceAll(":", "_"));
        }

        //If sha1 was not null, then return this as a result. 
        final String sha1 = generateSHA1String(result.toString());
        if (sha1 != null) {
            return sha1;
        }

        return result.toString();
    }

    /**
     * Produce a SHA1 string from string input.
     *
     * @param input string as an input
     * @return SHA1 string
     */
    private String generateSHA1String(final String input) {
        final MessageDigest digest;
        final byte[] data;
        try {
            digest = MessageDigest.getInstance(algorithm);
            digest.reset();
            data = digest.digest(input.getBytes(charsetName));
        } catch (final NoSuchAlgorithmException e) {
            Log.e(TAG, "No such algorithm: " + algorithm);
            return null;
        } catch (final UnsupportedEncodingException e) {
            Log.e(TAG, "UTF-8 is not supported.");
            return null;
        }

        return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
    }

    /**
     * Returns the device's serial number.
     *
     * @return the serial number
     */
    protected String getSerial() {
        return SERIAL;
    }

    /**
     * Gets the android id from the context.
     *
     * @return the android id
     */
    protected String getAndroidId() {

        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}

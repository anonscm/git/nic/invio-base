package de.tarent.invio.android.base.collector;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;

/**
 * SensorCollector that captures accelerometer, gyro and magnetometer data
 */
public class SensorDataCollector implements SensorEventListener, SensorCollector {

    /**
     * The values for the linear acceleration.
     */
    public float[] valuesLinearAcceleration = new float[3]; //NOSONAR - Does not need to br private.

    /**
     * The values for the accelerometer.
     */
    public float[] valuesAccelerometer = new float[3]; //NOSONAR - Does not need to br private.

    /**
     * The values for the magnetic field.
     */
    public float[] valuesMagneticField = new float[3]; //NOSONAR - Does not need to br private.

    private final SensorManager sensorManager;
    private boolean hasGyro;
    private int scanCount = 0;
    private int maxScanCount;
    private int iteration = 0;
    private CollectorListener captureListener;

    /**
     * Constructs a new {@link SensorDataCollector} instance.
     */
    public SensorDataCollector() {
        final Context context = App.getContext();
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        final PackageManager packageManager = context.getPackageManager();
        hasGyro = packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
    }

    /**
     * Starts listening to following sensors in order to use the dead reckoning:
     * <ul>
     * <li>Sensor.TYPE_ACCELEROMETE. Die Deltas werden R</li>
     * <li>Sensor.TYPE_MAGNETIC_FIELD</li>
     * <li>Sensor.TYPE_LINEAR_ACCELERATION</li>
     * </ul>
     * <p/>
     * Note, that TYPE_LINEAR_ACCELERATION may not be present on every device. TYPE_ACCELEROMETER will be substituted
     * in those cases.
     *
     * @param captureListener the {@link CollectorListener}
     * @param scanCount       the scan count as an int
     */
    public void startSensors(final CollectorListener captureListener, final int scanCount) {
        this.maxScanCount = scanCount;
        this.scanCount = scanCount;
        this.iteration = 0;
        this.captureListener = captureListener;

        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_NORMAL);

        if (captureListener != null) {
            captureListener.onCollectionStart();
        }
    }

    /**
     * Stop listening to the sensors and unregister them.
     */
    public void stopSensors() {
        sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        sensorManager.unregisterListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (scanCount > 0 || scanCount == -1) {
            if (!handleSensorType(event)) {
                return;
            }

            if (captureListener != null) {
                captureListener.onCollectionIteration(iteration, maxScanCount);
            }

            if (scanCount != -1) {
                scanCount--;
            }

            iteration++;
        } else {
            stopSensors();
        }
    }

    /**
     * Handles the sensor type from the given event.
     *
     * @param event the {@link SensorEvent} to be handled
     * @return true if the event was handled, false if it was not
     */
    private boolean handleSensorType(final SensorEvent event) {
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                handleAccelerometer(event);
                return true;
            case Sensor.TYPE_MAGNETIC_FIELD:
                handleMagneticField(event);
                return true;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                handleLinearAcceleration(event);
                return true;
            default:
                return false;
        }
    }

    private void handleAccelerometer(final SensorEvent event) {
        System.arraycopy(event.values, 0, valuesAccelerometer, 0, event.values.length);
        SensorDataLogHelper.logSensorData(valuesAccelerometer, Sensor.TYPE_ACCELEROMETER);
        if (!hasGyro) {
            // In that case we misuse the values from the accelerometer as if they came from
            // the linear accelerometer, which we don't have:
            System.arraycopy(event.values, 0, valuesLinearAcceleration, 0, event.values.length);
        }
    }

    private void handleMagneticField(final SensorEvent event) {
        System.arraycopy(event.values, 0, valuesMagneticField, 0, event.values.length);
        SensorDataLogHelper.logSensorData(valuesMagneticField, Sensor.TYPE_MAGNETIC_FIELD);
    }

    private void handleLinearAcceleration(final SensorEvent event) {
        System.arraycopy(event.values, 0, valuesLinearAcceleration, 0, event.values.length);
        SensorDataLogHelper.logSensorData(valuesLinearAcceleration, Sensor.TYPE_LINEAR_ACCELERATION);
    }

    @Override
    public void onAccuracyChanged(final Sensor sensor, final int i) {
    }
}

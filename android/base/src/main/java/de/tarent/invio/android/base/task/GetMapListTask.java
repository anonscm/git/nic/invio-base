package de.tarent.invio.android.base.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.response.MapListResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the AsyncTask which fetches the list of maps from the server, in the background. After it has finished
 * it will add the names to our list-adapter.
 */
public class GetMapListTask extends AsyncTask {

    /**
     * The tag for log-messages from this class:
     */
    private static final String TAG = "GetMapListTask";

    /**
     * In this list we will store the map-names that we got from the server. This will happen in the background.
     * Then, onPostExecute will be called automatically, from the main-thread, and then the mapList will be applied.
     */
    protected List<String> mapList = new ArrayList<String>();

    protected List<String> filteredList = new ArrayList<String>();

    private final MapServerClient mapServerClient;

    private ProgressDialog progressDialog;

    private ArrayAdapter<String> adapter;

    private String mapsPrefix;

    /**
     * Constructor.
     *
     * @param mapServerClient the {@link MapServerClient}
     * @param progressDialog  the {@link #progressDialog}
     * @param adapter         the {@link #adapter}
     * @param mapsPrefix      the prefix for the map name. The list will contain map names beginning with this prefix.
     */
    public GetMapListTask(final MapServerClient mapServerClient, final ProgressDialog progressDialog,
                          final ArrayAdapter<String> adapter, final String mapsPrefix) {

        this.mapServerClient = mapServerClient;
        this.progressDialog = progressDialog;
        this.adapter = adapter;
        this.mapsPrefix = mapsPrefix;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object doInBackground(final Object... params) {
        try {
            final MapListResponse response = mapServerClient.getMapList(5000);
            mapList = response.getMapList();
        } catch (final IOException e) {
            Log.e(TAG, "Failed to download the map-list: " + e);
        }
        progressDialog.dismiss();

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPostExecute(final Object result) {
        if (mapList != null) {
            adapter.addAll(filterMapList(mapList));
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * In order NOT to show every map on the server we filter the maps depending on configured prefix (such as
     * "sellfio_" or "invio_" or "cutomerX_"). In case of the prefix "tarentdebug" we show all the maps for testing
     * purposes.
     *
     * @param mapList the full list containing map names
     * @return a filtered list of the map names starting with mapsPrefix.
     */
    private List<String> filterMapList(final List<String> mapList) {
        filteredList = new ArrayList<String>();

        if (mapsPrefix == null || mapsPrefix.isEmpty()) {
            Log.e(TAG, "Could not filter map list, because the prefix is null or empty");
            return new ArrayList<String>();
        }

        if (mapsPrefix.equals("tarentdebug")) {
            filteredList = mapList;
            return filteredList;
        }

        if (mapList == null || mapList.isEmpty()) {
            Log.e(TAG, "Server returned no or empty map list.");
            return new ArrayList<String>();
        }

        for (final String mapName : mapList) {
            if (mapName.startsWith(mapsPrefix)) {
                filteredList.add(mapName.replaceFirst(mapsPrefix, ""));
            }
        }
        return filteredList;
    }

}

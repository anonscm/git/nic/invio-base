package de.tarent.invio.android.base.utils;

import android.app.Activity;
import android.content.res.Configuration;
import android.view.WindowManager;

import static android.content.Context.WINDOW_SERVICE;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR;
import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_180;
import static android.view.Surface.ROTATION_270;
import static android.view.Surface.ROTATION_90;

/**
 * Screen orientation class that holds useful methods regarding the orientation of the screen such as: locking,
 * unlocking, etc.
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public final class ScreenOrientation {

    /**
     * Private constructor to prevent instantiation.
     */
    private ScreenOrientation() {

    }

    /**
     * Unlocks the locked screen orientation changes by setting the requested orientation back to
     * ActivityInfo.SCREEN_ORIENTATION_SENSOR.
     *
     * @param activity the activity to unlock the screen orientation of
     */
    public static void unlockScreenOrientation(final Activity activity) {
        activity.setRequestedOrientation(SCREEN_ORIENTATION_SENSOR);
    }

    /**
     * Locks the screen orientation for the given activity.
     *
     * @param activity the activity to lock the screen orientation of
     */
    public static void lockScreenOrientation(final Activity activity) {
        if (getDeviceDefaultOrientation(activity) == ORIENTATION_LANDSCAPE) {
            lockOrientationOnLandscapeDefault(activity);
        } else {
            lockOrientationOnPortraitDefault(activity);
        }
    }

    /**
     * Locks the screen orientation when landscape the the default orientation.
     *
     * @param activity the activity to lock the screen orientation of
     */
    private static void lockOrientationOnLandscapeDefault(final Activity activity) {

        switch (((WindowManager) activity.getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation()) {

            case ROTATION_0:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_LANDSCAPE);
                break;

            case ROTATION_90:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                break;

            case ROTATION_180:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                break;

            default:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    /**
     * Locks the screen orientation when portrait the the default orientation.
     *
     * @param activity the activity to lock the screen orientation of
     */
    private static void lockOrientationOnPortraitDefault(final Activity activity) {
        switch (((WindowManager) activity.getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getRotation()) {

            case ROTATION_90:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_LANDSCAPE);
                break;

            case ROTATION_180:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_PORTRAIT);
                break;

            case ROTATION_270:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
                break;

            default:
                activity.setRequestedOrientation(SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    /**
     * Gets the device's default screen orientation.
     *
     * @param activity the activity to get the default orientation from
     * @return the device's default orientation
     */
    static int getDeviceDefaultOrientation(final Activity activity) {
        final WindowManager windowManager = (WindowManager) activity.getSystemService(WINDOW_SERVICE);

        final Configuration config = activity.getResources().getConfiguration();

        final int rotation = windowManager.getDefaultDisplay().getRotation();

        if (((rotation == ROTATION_0 || rotation == ROTATION_180) && config.orientation == ORIENTATION_LANDSCAPE) ||
                ((rotation == ROTATION_90 || rotation == ROTATION_270) && config.orientation == ORIENTATION_PORTRAIT)) {

            return ORIENTATION_LANDSCAPE;
        } else {

            return ORIENTATION_PORTRAIT;
        }
    }
}

package de.tarent.invio.android.base.collector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.histogram.HistogramBuilder;
import de.tarent.invio.android.base.utils.InvioScanResult;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;
import de.tarent.invio.entities.Histogram;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The WifiCollector records wifi-histograms.
 * The WifiCollector has two modes:
 * - fingerprint-mode, for the offline-phase, where it does a number of scans, produces one histogram, and then stops.
 * - continuous-mode, for the online-phase, where it goes on scanning and producing histograms until it is stopped.
 * The mode is selected via the constuctor's collectionMode argument
 * <p/>
 * TODO:
 * - the number of scans could be made configurable.
 * - there should be a configurable timeout, in case the scanning takes much longer than expected.
 */
public class WifiCollector implements HistogramCollector {
    private static final String TAG = "WifiCollector";

    private Context context;

    private HistogramBuilder histogramBuilder;

    private WifiManager wifi;

    private CollectorListener collectorListener;

    private int scanCount = 0;
    private int iteration = 0;

    /**
     * The maximum age in milliseconds that a scanresult may have to be included in the histogram.
     * A maxAge of 0 is used to disable age-filtering.
     */
    private int maxAge;

    /**
     * The WifiReceiver will receive the scanresults from the WifiManager. We register it with the activity and
     * unregister it again, when we are done.
     */
    private WifiReceiver wifiReceiver;
    private int maxScanCount;
    private Histogram lastHistogram = null;

    /**
     * Construct a new WifiCollector
     *
     * @param collectionMode the collection mode to use
     */
    public WifiCollector(CollectionMode collectionMode) {
        this.context = App.getContext();
        this.maxAge = collectionMode == CollectionMode.CONTINUOUS ? CONTINUOUS_MAXAGE : 0;
    }

    @Override
    public void startSensors(final CollectorListener collectorListener, final int scanCount) {
        Log.d("WifiLog", "StartSensorsWIFI");
        this.maxScanCount = scanCount;
        this.scanCount = scanCount;
        this.iteration = 0;
        this.collectorListener = collectorListener;

        histogramBuilder = new HistogramBuilder("Wifi Histogram " + new Date().toString(), maxAge);
        lastHistogram = null;
        wifiReceiver = new WifiReceiver();

        final HandlerThread handlerThread = new HandlerThread("WifiCollector-HandlerThread");
        handlerThread.start();
        final Looper looper = handlerThread.getLooper();
        final Handler handler = new Handler(looper);
        context.registerReceiver(wifiReceiver,
                new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION), null, handler);

        wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifi.startScan();

        if (collectorListener != null) {
            collectorListener.onCollectionStart();
        }
    }

    /**
     * Stop scanning. Don't receive scan results. That means we don't start any new scans either, because that would
     * happen in onReceive.
     * TODO: test what happens when stopSensors is called before startSensors.
     */
    @Override
    public void stopSensors() {
        Log.d("WifiLog", "StopSensorsWIFI");
        if (wifiReceiver == null) {
            return;
        }

        if (collectorListener != null) {
            collectorListener.onCollectionStop();
        }

        try {
            context.unregisterReceiver(wifiReceiver);
            scanCount = 0;
            lastHistogram = null;
            wifiReceiver = null;
        } catch (final IllegalArgumentException e) {
            Log.e(TAG, "The receiver was not registered or already unregistered: " + e.getMessage());
        }
    }

    @Override
    public Histogram getHistogram() {
        return lastHistogram;
    }

    /**
     * The callback class that will receive the SCAN_RESULTS_AVAILABLE_ACTION notifications. It will integrate the
     * scan results into the collector's HistogramBuilder and will start a new scan, until the number of planned scans
     * is reached.
     */
    class WifiReceiver extends BroadcastReceiver {
        /**
         * This method will be called by the Android-System when the WifiManager has completed a scan.
         *
         * @param ctx    ignored. We already have our parent-Activity.
         * @param intent ignored. We are only registered for one type of action and we know what to do.
         */
        @Override
        public void onReceive(final Context ctx, final Intent intent) {
            final List<android.net.wifi.ScanResult> scan = wifi.getScanResults();
            if (scanCount > 0 || scanCount == -1) {
                histogramBuilder.addScanResults(copyScanResults(scan));

                lastHistogram = histogramBuilder.build();
                SensorDataLogHelper.logHistogram(lastHistogram, SensorDataLogHelper.HistogramLogType.WIFI);

                wifi.startScan();

                if (scanCount != -1) {
                    scanCount--;
                }

                iteration++;

                if (collectorListener != null) {
                    collectorListener.onCollectionIteration(iteration, maxScanCount);
                }
            } else {
                stopSensors();
            }
        }

        /**
         * Copy a list of android ScanResult into a list of our own WifiScanResult.
         *
         * @param scanResults the List of scan results that the WifiManager supplied.
         * @return a List of our platform independent WifiScanResult
         */
        private List<InvioScanResult> copyScanResults(final List<ScanResult> scanResults) {
            final List<InvioScanResult> invioScanResults = new ArrayList<InvioScanResult>();
            for (final ScanResult scan : scanResults) {
                invioScanResults.add(new InvioScanResult(scan.BSSID, scan.level));
            }
            return invioScanResults;
        }
    }
}

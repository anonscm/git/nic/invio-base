package de.tarent.invio.android.base.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.tarent.invio.android.base.BaseActivity;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.task.UploadLogZipTask;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.tracker.TrackerConfigParameter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * This class provides static functionality for sensor data logging. It can append histograms to an existing log file,
 * creating it if there is none. Also other sensor data can be dumped here for the same purpose
 */
public final class SensorDataLogHelper {

    public static final String LOG_PATH = Environment.getExternalStorageDirectory() + File.separator + "invioLogs"
            + File.separator;

    public static final String TXT_SUFFIX = ".txt";
    public static final String JSON_SUFFIX = ".json";
    private static final String TAG = "SensorDataLogHelper";
    private static String histogramLogStartTime = "";
    private static boolean active = false;
    private static int idCountWifi = 1;
    private static int idCountBtle = 1;

    private static String mapName;

    /**
     * Private default constructor to prevent instantiation.
     */
    private SensorDataLogHelper() {

    }

    /**
     * Toggles the logging on or off depending on the {@link #active} status.
     *
     * @param context the application context to show the toast
     * @param mapName the map name
     */
    public static void toggleLogging(final Context context, final String mapName) {
        if (active) {
            stopLogging();
            Toast.makeText(context, context.getString(R.string.toggle_logging_off), Toast.LENGTH_LONG).show();
        } else {
            SensorDataLogHelper.mapName = mapName;
            startLogging();
            logConfig();
            Toast.makeText(context, context.getString(R.string.toggle_logging_on), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Appends the given histogram to a central log file
     *
     * @param histogram        the histogram to be logged
     * @param histogramLogType the type of the log
     */
    public static void logHistogram(final Histogram histogram, final HistogramLogType histogramLogType) {
        if (active) {
            final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();

            final int idCount = getIdCount(histogramLogType);

            //TODO: All the stuff added to the string should be removed after the parser is modified
            final String json = "{\"histogram\":" + gson.toJson(histogram) + ",\"id\":\"FP-" + idCount +
                    "\",\"timestamp\":" + System.currentTimeMillis() + ",\"point\":{\"latitude\":0," +
                    "\"longitude\":0}" + "},";
            switch (histogramLogType) {
                case WIFI:
                    final String fileNameWifi = getLogNameWifi();
                    writeLog(json, fileNameWifi);
                    idCountWifi++;
                    break;
                case BluetoothLE:
                    final String fileNameBtle = getLogNameBtle();
                    writeLog(json, fileNameBtle);
                    idCountBtle++;
                    break;
                default:
                    Log.d(TAG, "Neither wifi nor bluetooth are being logged.");
            }
        }
    }

    private static int getIdCount(final HistogramLogType histogramLogType) {
        int idCount = idCountWifi;
        if (histogramLogType == HistogramLogType.BluetoothLE) {
            idCount = idCountBtle;
        }
        return idCount;
    }

    /**
     * Appends the given sensor data to a central log file
     *
     * @param sensorData float array containing the sensor results
     * @param type       {@link android.hardware.Sensor}-type value determining
     *                   which type of sensor is providing the information
     */
    public static void logSensorData(final float[] sensorData, final int type) {
        if (active) {
            final StringBuilder buffer = new StringBuilder();

            buffer.append(System.currentTimeMillis()).append(" ").append(type);
            for (final float measurement : sensorData) {
                buffer.append(" ").append(measurement);
            }
            buffer.append("\n");

            final String logEntry = buffer.toString();

            writeLog(type, logEntry);
        }
    }

    private static void writeLog(final int type, final String logEntry) {
        switch (type) {
            case Sensor.TYPE_ACCELEROMETER:
                writeLog(logEntry, getAccelerometerLogName());
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                writeLog(logEntry, getLinearAccelerationLogName());
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                writeLog(logEntry, getMagneticFieldLogName());
                break;
            default:
                break;
        }
    }

    private static String getMagneticFieldLogName() {
        return "magneticFieldLog"  + TXT_SUFFIX;
    }

    private static String getLinearAccelerationLogName() {
        return "linearAccelerationLog" + TXT_SUFFIX;
    }

    private static String getAccelerometerLogName() {
        return "accelerometerLog" + TXT_SUFFIX;
    }

    public static String getHistogramLogStartTime() {
        return histogramLogStartTime;
    }

    private static String getFingerprintsLogName() {
        return "fingerprints" + JSON_SUFFIX;
    }

    private static void logFingerprints() {
        final String fpJson = TrackerManager.getInstance().getFingerprintsJson();
        if (fpJson != null) {
            writeLogLine(fpJson, getFingerprintsLogName());
        }
    }

    private static void writeLogLine(final String line, final String fileName) {
        writeLog(line + "\n", fileName);
    }

    private static void logConfig() {
        final String configFile = "config" + TXT_SUFFIX;

        logFingerprints();

        writeLogLine("#Tracker [" + TrackerManager.getInstance().version() + "] config "
                + histogramLogStartTime, configFile);
        writeLogLine("", configFile);

        final TrackerConfigParameter[] parameters = TrackerManager.getInstance().getConfigParameters();

        for(TrackerConfigParameter parameter : parameters){
            writeLogLine(parameter.name + "=" + parameter.currentValue, configFile);
        }
    }

    /**
     * Appends the given position to the position log.
     *
     * @param latitudeE6  latitudeE6
     * @param longitudeE6 longitudeE6
     */
    public static void logPosition(final int latitudeE6, final int longitudeE6) {
        if (active) {

            final String logEntry = System.currentTimeMillis() + " " + (double) longitudeE6 / 1E6 + " "
                    + (double) latitudeE6 / 1E6 + "\n";

            writeLog(logEntry, "positionsLongLat" + TXT_SUFFIX);
        }
    }

    private static void writeLog(final String line, final String fileName) {
        final File logFile = createLogFile(fileName, createLogDirectory());

        FileOutputStream out = null;
        PrintWriter printWriter = null;

        try {
            out = new FileOutputStream(logFile, true);
            printWriter = new PrintWriter(new OutputStreamWriter(out, Charset.forName("UTF-8")));
            printWriter.print(line);
            printWriter.flush();
            printWriter.close();
            out.close();
        } catch (final FileNotFoundException e) {
            Log.e(TAG, "Log file could not be found!");
        } catch (final IOException e) {
            Log.e(TAG, "Could not close FileOutputStream to Log File!");
        } finally {
            // This ensures that no matter what happens while flushing or closing the streams, they are closed for sure.
            IOUtils.closeQuietly(printWriter);
            IOUtils.closeQuietly(out);
        }
    }

    private static File createLogFile(final String fileName, final boolean directoryCreated) {
        final File logFile = new File(LOG_PATH + "/" + getHistogramLogStartTime() + "/" + fileName);
        if (!logFile.exists() && directoryCreated) {
            try {
                logFile.createNewFile();
            } catch (final IOException e) {
                Log.e(TAG, "Log file could not be written!");
            }
        }
        return logFile;
    }

    private static File getLogDirectory(){
        return new File(LOG_PATH + "/" + getHistogramLogStartTime());
    }

    private static boolean createLogDirectory() {
        final File logDirectory = getLogDirectory();
        return logDirectory.exists() || logDirectory.mkdirs();
    }

    private static void startLogging() {
        if (!active) {
            setTimeLogStarted();
            active = true;
            writeLog("[", getLogNameWifi());
            writeLog("[", getLogNameBtle());
        }
    }

    private static void setTimeLogStarted() {
        final long currentTimeMillis = System.currentTimeMillis();
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTimeMillis);

        histogramLogStartTime = formatter.format(calendar.getTime());
    }

    private static void stopLogging() {
        stopLogging(HistogramLogType.WIFI);
        stopLogging(HistogramLogType.BluetoothLE);

        try {
            final String fileName = LOG_PATH + "/" + getHistogramLogStartTime() + ".zip";
            ZipUtils.zipDirectory(getLogDirectory(), new File(fileName));
            final UploadLogZipTask uploadTask = new UploadLogZipTask(BaseActivity.getMapServerClient(),
                                                               SensorDataLogHelper.mapName,
                                                               new File(fileName));
            uploadTask.execute();
        } catch (final IOException e){
            Log.e(TAG, "Could not zip directory: " + e.getMessage());
        }
    }

    private static void stopLogging(final HistogramLogType type) {
        try {
            //Remove the last "," from the histogram Log and place a "]" there so that it can be parsed properly
            final File histogramLog = getHistogramLog(type);

            String histogramLogString = FileUtils.readFileToString(histogramLog);
            histogramLogString = histogramLogString.substring(0, histogramLogString.length() - 1) + "]";
            histogramLog.delete(); //NOSONAR: We don't need to know if it was deleted or not.

            handleWriteLogType(type, histogramLog, histogramLogString);
        } catch (final FileNotFoundException e) {
            Log.e(TAG, "Log file could not be found!");
        } catch (final IOException e) {
            Log.e(TAG, "Log file could not be read!");
        }
        active = false;
    }

    private static void handleWriteLogType(final HistogramLogType type, final File histogramLog,
                                           final String histogramLogString) {
        if (type == HistogramLogType.WIFI) {
            writeLog(histogramLogString, getLogNameWifi());
            deleteEmptyLog(histogramLog, type);
            idCountWifi = 1;
        } else if (type == HistogramLogType.BluetoothLE) {
            writeLog(histogramLogString, getLogNameBtle());
            deleteEmptyLog(histogramLog, type);
            idCountBtle = 1;
        }
    }

    private static void deleteEmptyLog(final File histogramLog, final HistogramLogType type) {
        if (type == HistogramLogType.WIFI && idCountWifi == 1) {
            histogramLog.delete(); //NOSONAR: We don't need to know if it was deleted or not.

        } else if (type == HistogramLogType.BluetoothLE && idCountBtle == 1) {
            histogramLog.delete(); //NOSONAR: We don't need to know if it was deleted or not.
        }
    }

    private static File getHistogramLog(final HistogramLogType type) {
        if (type == HistogramLogType.WIFI) {
            return new File(LOG_PATH + "/" + getHistogramLogStartTime() + "/" + getLogNameWifi());
        } else if (type == HistogramLogType.BluetoothLE) {
            return new File(LOG_PATH + "/" + getHistogramLogStartTime() + "/" + getLogNameBtle());
        }
        return null;
    }

    private static String getLogNameWifi() {
        return "histogramLog_wifi" + JSON_SUFFIX;
    }

    private static String getLogNameBtle() {
        return "histogramLog_btle" + JSON_SUFFIX;
    }

    /**
     * Enum to differentiate between wifi and bluetooth while logging.
     */
    public enum HistogramLogType {
        WIFI,
        BluetoothLE
    }
}

package de.tarent.invio.android.base.notifications;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.BaseActivity;
import de.tarent.invio.android.base.R;
import de.tarent.invio.entities.InvioGeoPoint;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.List;

/**
 * Singleton PushNotificationManager
 * <p/>
 * Downloads and manages push notifications across the app.
 * <p/>
 * The json format this manager uses is documented here:
 * https://evolvis.org/plugins/mediawiki/wiki/nic/index.php/Penny_Notifications
 */
public final class PushNotificationManager {
    private static PushNotificationManager instance = new PushNotificationManager();

    private List<PushNotification> notifications = null;
    private PushNotificationSettings defaults = null; //NOSONAR field is used by gson over reflection

    private PushNotificationManager() {
        new DownloadPushNotificationJsonTask().execute();
    }

    /**
     * Gets the instance of the {@link PushNotificationManager}.
     *
     * @return the instance
     */
    public static PushNotificationManager getInstance() {
        return instance;
    }

    /**
     * Check all notifications for a given user position.
     *
     * @param activity a valid activity context (the current active activity) to display dialogs
     * @param location a user position
     */
    public void check(final Activity activity, final InvioGeoPoint location) {
        if (notifications != null) {
            for (final PushNotification notification : notifications) {
                if (notification.isValidForLocation(location) && notification.isImageDownloaded()) {
                    notification.fire(activity);
                    break;
                }
            }
        }
    }

    /**
     * Download task for async downloading of the json file
     */
    private class DownloadPushNotificationJsonTask extends AsyncTask<Void, Void, Void> {
        PushNotificationJsonData data = null;

        /**
         * perform download of the notifications.json file
         *
         * @param voids void
         * @return the json content
         */
        protected Void doInBackground(final Void... voids) {
            downloadNotifications();

            if (data != null) {
                defaults = data.defaults;
                notifications = data.notifications;

                downloadImages();
            }

            return null;
        }

        private void downloadNotifications() {
            try {
                final String mapDataTemplate = "/maps/{mapName}/data/";
                final String endpoint = App.getContext().getString(R.string.server_endpoint);
                // TODO: This is not the right way to get he map name. Maybe it should be taken form the IndoorMap.
                final String mapName = BaseActivity.getMapProperties().getUploadLocation();
                final String stringUrl = endpoint.replace("/mapserver", "") +
                        mapDataTemplate.replace("{mapName}", mapName) +
                        "notifications.json";

                final URL url = new URL(stringUrl);
                final InputStream in = url.openStream();
                final String json = IOUtils.toString(in);
                IOUtils.closeQuietly(in);

                if (json != null) {
                    data = new Gson().fromJson(json, PushNotificationJsonData.class);

                    if (data.defaults == null) {
                        data.defaults = new PushNotificationSettings();
                    }

                    validateNotifications();
                    //sort notifications by priority, so our matching code can simply terminate on the first match.
                    Collections.sort(data.notifications);
                }
            } catch (final IOException e) {
                Log.e(getClass().getSimpleName(), "Caught exception: " + e.getMessage());
                Log.e(getClass().getSimpleName(), "Download of notifications.json failed");
            } catch (final JsonSyntaxException e) {
                Log.e(getClass().getSimpleName(), "Caught exception: " + e.getMessage());
                Log.e(getClass().getSimpleName(), "Parsing of notifications.json failed");
            }
        }

        private void downloadImages() {
            if (data != null && data.notifications != null) {
                for (final PushNotification notification : data.notifications) {
                    notification.downloadImage();
                }
            }
        }

        /**
         * make sure all notifications are valid, i.e. describe a top-left-to-bottom-right rectangle
         * all invalid notifications are deactivated
         * <p/>
         * additionally, because of the requirements of SEL-492, all frequency caps are set to 1 here.
         */
        private void validateNotifications() {
            for (final PushNotification notification : data.notifications) {
                final PushNotificationRectangle rectangle = notification.getRectangle();
                final boolean ok = rectangle.maxLatitude > rectangle.minLatitude &&
                        rectangle.maxLongitude > rectangle.minLongitude;

                notification.setActive(notification.isActive() && ok);

                final PushNotificationSettings settings = notification.getSettings();
                if (settings != null) {
                    //set all frequency caps to 1 - see SEL-492
                    settings.setFrequencyCap(1);
                } else {
                    notification.setSettings(defaults);
                }
            }
        }

        /**
         * Gson does not support injecting into existing objects, so we need this "holder" class
         */
        private class PushNotificationJsonData {
            private List<PushNotification> notifications; //NOSONAR
            private PushNotificationSettings defaults; //NOSONAR
        }
    }
}

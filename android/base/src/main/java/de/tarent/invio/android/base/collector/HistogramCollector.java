package de.tarent.invio.android.base.collector;

import de.tarent.invio.entities.Histogram;

/**
 * A sensor collector used to collect the data for a histogram (wifi or btle).
 */

public interface HistogramCollector extends SensorCollector {

    int CONTINUOUS_MAXAGE = 4000;

    /**
     * collection modes for histograms
     */
    enum CollectionMode {
        FINGERPRINT,
        CONTINUOUS
    };

    /**
     * Gets the histogram.
     *
     * @return the {@link Histogram}
     */
    Histogram getHistogram();

}

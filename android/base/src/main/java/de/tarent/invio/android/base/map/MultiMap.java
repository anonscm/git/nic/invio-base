package de.tarent.invio.android.base.map;

import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tarent.invio.android.base.AbstractMapActivity;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.android.base.json.InvioGeoPointDeserializer;
import de.tarent.invio.android.base.task.DownloadListener;
import de.tarent.invio.android.base.task.DownloadTask;
import de.tarent.invio.android.base.task.InvioOsmXmlParser;
import de.tarent.invio.android.base.task.ZipFingerprintsTask;
import de.tarent.invio.android.base.task.ZipMapDataTask;
import de.tarent.invio.android.base.task.ZipMapResourceTask;
import de.tarent.invio.android.base.tileprovider.TileProviderFactory;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.InvioGeoPoint;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.tarent.invio.android.base.map.MapParams.setLevel;

/**
 * A MultiMap bundles one or more instances of IndoorMap together to form a multi-level building.
 */
public class MultiMap implements DownloadListener {

    private static final int DOWNLOADS_PER_MAP = 3;

    protected File groupDir;

    protected AbstractMapActivity activity;

    protected MapView mapView;

    protected Map<String, IndoorMap> maps;

    protected List<IndoorMap> mapsList;

    IndoorMap userSelectedMap;

    private MapTileProviderBase provider;

    private int successfulPartDownloads = 0;

    private int partDownloads = 0;

    private MultilevelMapsListener multilevelMapsListener;

    /**
     * Constructor.
     *
     * @param groupDir directory of the unzipped group data file
     * @param activity the calling activity for context
     * @param mapView  the corresponding mapview
     */
    public MultiMap(final File groupDir, final AbstractMapActivity activity, final MapView mapView) {
        this.groupDir = groupDir;
        this.activity = activity;
        this.mapView = mapView;
        setLevel(0);
        createMaps();
    }

    /**
     * This constructor should be used in one special case if some class is not related to the map view but must get
     * a list of downloaded indoor maps to be able to start the tracker.
     *
     * @param groupDir               directory of the unzipped group data file
     * @param multilevelMapsListener listener which should be notified when the list of the indoor maps were created.
     */
    public MultiMap(final File groupDir, final MultilevelMapsListener multilevelMapsListener) {
        this.multilevelMapsListener = multilevelMapsListener;
        this.groupDir = groupDir;
        createMaps();
    }

    /**
     * Listener interface to get the complete multilevel list result.
     */
    public interface MultilevelMapsListener {
        /**
         * Notification containing a list with indoor maps (multilevel).
         *
         * @param mapsList a list of indoor maps
         */
        void onMultilevelMapsListCreated(final List<IndoorMap> mapsList);
    }

    /**
     * Detaches the created provider. The calling activity must call this method on every onPause() to avoid leaks.
     */
    public void detach() {
        if (provider != null) {
            provider.detach();
        }
    }

    public int getLevel() {
        return MapParams.getLevel();
    }

    /**
     * Create indoor maps per directory inside the unzipped group data directory.
     */
    private void createMaps() {
        mapsList = new ArrayList<>();
        if (groupDir.exists() && groupDir.isDirectory()) {
            final File[] mapDirs = groupDir.listFiles();
            if (mapDirs != null) {
                for (final File mapDir : mapDirs) {
                    final IndoorMap map = new IndoorMap(mapDir);
                    mapsList.add(map);
                }

                for (final IndoorMap map : mapsList) {
                    final ZipMapDataTask mapDataTask = new ZipMapDataTask(map, map.getMapDirectory(),
                            new InvioOsmXmlParser());
                    mapDataTask.addDownloadListener(this);
                    mapDataTask.execute();
                    final ZipMapResourceTask mapResourceTask = new ZipMapResourceTask(this, map);
                    mapResourceTask.execute();
                    final ZipFingerprintsTask fingerprintsTask = new ZipFingerprintsTask(map, map);
                    fingerprintsTask.addDownloadListener(this);
                    fingerprintsTask.execute();
                }
            } else {
                Log.e("sellfio-MultiMap", "mapDirs is null, stuff won't work...");
            }
        }
    }

    /**
     * Switches the map in the UI and sets all data needed for uploadPositionTask (fingerprints, ways, scale ..)
     *
     * @param map {@link de.tarent.invio.android.base.map.IndoorMap} to show
     */
    private void switchMapForReal(final IndoorMap map) {
        final Handler mainHandler = new Handler(Looper.getMainLooper());
        mainHandler.post(getNewRunnable(map));
    }

    /**
     * Finds the map based on the given short name.
     *
     * @param shortName short id of map
     * @return map
     */
    public IndoorMap findMapByName(final String shortName) {
        return maps.get(shortName);
    }

    // TODO: strange name...
    private Runnable getNewRunnable(final IndoorMap map) {
        return new Runnable() {
            @Override
            public void run() {
                initializeBaseAngle(map);
                initializeScale(map);
                initializeFingerprints(map);
                configureMapView(map);
                mapView.invalidate();

                activity.setWifiOff(false);
                activity.dismissProgressDialog();
                activity.onMapSwitched(map);
            }
        };
    }

    private void initializeFingerprints(final IndoorMap map) {
        final FingerprintManager fingerprintManager = activity.getFingerprintManager();
        if (fingerprintManager != null) {
            fingerprintManager.removeAllFingerprintFromOverlay();
            fingerprintManager.setWifiFingerprintsJson(map.getFingerprintsJson());
        }
    }

    private void initializeScale(final IndoorMap map) {
        final Float scale = map.getScale();
        if (scale != null) {
            TrackerManager.getInstance().setScale(scale);
        }
    }

    private void initializeBaseAngle(final IndoorMap map) {
        final Integer angle = map.getBaseAngle();
        if (angle != null) {
            TrackerManager.getInstance().setNorthAngle(angle);
        }
    }

    /**
     * Switch to the level of the building on which we currently are. Includes autodetection.
     *
     * @param map {@link de.tarent.invio.android.base.map.IndoorMap} to show
     */
    public void switchMap(final IndoorMap map) {
        if (!activity.isWifiOff()) {
            activity.showProgressDialog();
        }
        userSelectedMap = map;

        if (userSelectedMap != null) {
            switchMapForReal(userSelectedMap);
        } else {
            setLevel(TrackerManager.getInstance().findClosestLevel());
            switchMapForReal(mapsList.get(MapParams.getLevel()));
        }
    }

    /**
     * Return an {@link IndoorMap} by its short name
     *
     * @param shortName of the map
     * @return the {@link IndoorMap}
     */
    public IndoorMap getMapByShortName(final String shortName) {
        return maps.get(shortName);
    }


    /**
     * Create Fingerprints from json-string.
     *
     * @param json the json that contains the serialized fingerprints
     * @return the List of Fingerprints
     */
    //TODO Refactoring: THIS CODE IS STRICTLY DUPLICATE FROM FingerprintManager
    protected List<Fingerprint> parseJson(final String json) {
        // We need this TypeToken-thingy because it is not possible to have a Class-object for a generic type.
        // But we must tell gson what kind of object it is supposed to create. That's how we can do is:
        final Type genericFingerprintArrayListType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());

        return gsonBuilder.create().fromJson(json, genericFingerprintArrayListType);
    }

    private void configureMapView(final IndoorMap map) {
        mapView.setTileSource(makeTileSource(map));
        mapView.setMinZoomLevel(map.getMinZoomLevel());
        mapView.setMaxZoomLevel(map.getMaxZoomLevel());
        // TODO: does it even make sense to arrive at this place without a boundingbox? Maybe we get called
        //       too early, if that happens? Anyway, the MapView doesn't like "null"...
        final BoundingBoxE6 boundingBox = map.getBoundingBox();
        if (boundingBox != null) {
            mapView.setMinZoomLevel(getZoomLevel(map.getMinZoomLevel()));
            mapView.zoomToBoundingBox(boundingBox);
            mapView.setScrollableAreaLimit(boundingBox);

            TrackerManager.getInstance().setBoundingBox(boundingBox);
        }
    }

    private int getZoomLevel(final int minZoomLevel) {
        final Display display = activity.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);

        int screenSize = Math.min(size.x, size.y);
        int zoomLvl = 1; // added 1 -> map does not fit completely into display, but nicer this way

        while (screenSize > 512) {
            screenSize = screenSize / 2;
            zoomLvl++;
        }

        return minZoomLevel + zoomLvl;
    }

    /**
     * Get the appropriate tile source for the current level.
     *
     * @return the ITileSource for the current level
     */
    private ITileSource makeTileSource(final IndoorMap map) {
        // TODO: maybe we need to dispose of the old provider/source?
        final TileProviderFactory factory = new TileProviderFactory(activity);
        provider = factory.buildWebTileProvider(
                activity.getResources().getString(de.tarent.invio.android.base.R.string.map_provider_url_schema),
                map.getMapDirectory().getName());
        return provider.getTileSource();
    }


    @Override
    public void onDownloadFinished(final DownloadTask task, final boolean success, final Object data) {
        //Count all part downloads - map data, fingerprints and tilemapresource, no matter if it was successful or not.
        partDownloads++;

        //Then count only the successful downloads
        if (success) {
            if (task instanceof ZipMapDataTask) {
                successfulPartDownloads++;
            } else if (task instanceof ZipMapResourceTask) {
                successfulPartDownloads++;
            } else if (task instanceof ZipFingerprintsTask) {
                successfulPartDownloads++;
            }
        }

        //Inform the user, when some parts are missing!
        //TODO Multilevel: what to do if some fingerprints are missing?
        if (partDownloads == (mapsList.size() * DOWNLOADS_PER_MAP)) {
            if (successfulPartDownloads < partDownloads) {
                //TODO: Some parts are missing! Things may not work properly
                Toast.makeText(App.getContext(),
                        App.getContext().getResources().getString(R.string.missing_multi_map_parts),
                        Toast.LENGTH_LONG).show();
            }
            //Notify the listener and give it what it needs and has been long waiting for ;)
            if (multilevelMapsListener != null) {
                multilevelMapsListener.onMultilevelMapsListCreated(mapsList);
                return;
            }

            //Configure searchview in order to be able to search for POIs (products or exhibitors)
            activity.configurePOISearch();

            createHashMapFromMapsList();
            switchMap(null);
            //Create the level buttons in the map view
            activity.createLevelButtons(mapsList);
        }
    }

    /**
     * Now that we have downloaded and parsed short names, create a map with short name as a key
     * <p/>
     * and {@link IndoorMap} objects as values.
     */
    private void createHashMapFromMapsList() {
        maps = new HashMap<>();
        for (final IndoorMap map : mapsList) {
            maps.put(map.getShortName(), map);
        }
    }
}

package de.tarent.invio.android.base.task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Cached download task for the occupancy grid map.
 */
public class CachedDownloadOccupancyGridMapTask extends DownloadTask<Void, Void, Bitmap> {

    /**
     * False if no OSM file was found on the server (error on download). True if it exists.
     */
    private static boolean osmFileFound = false;

    /**
     * False until the download is finished.
     */
    private static boolean osmDownloadFinished = false;

    private static final String TAG = "DownloadOccupancyGridMapTask";

    private final String mapName;

    /**
     * Constructor.
     *
     * @param listener the {@link DownloadListener}
     * @param mapName the map name as a string
     */
    public CachedDownloadOccupancyGridMapTask(final DownloadListener<Bitmap> listener, final String mapName) {
        this.mapName = mapName;
        downloadListeners.add(listener);
    }

    @Override
    protected Bitmap doInBackground(final Void... params) {
        Bitmap result = null;
        try {
            result = downloadOccupancyGridMap();
            osmFileFound = true;
            success = true;
        } catch (final IOException e) {
            osmFileFound = false;
            success = false;
            Log.e(TAG, "Error downloading grid map: " + e.getMessage());
        }

        osmDownloadFinished = true;
        return result;
    }

    private Bitmap downloadOccupancyGridMap() throws IOException {
        final String mapDataTemplate = "/maps/{mapName}/data/";
        final String endpoint = App.getContext().getString(R.string.server_endpoint);
        final String url = endpoint.replace("/mapserver", "") + mapDataTemplate.replace("{mapName}", mapName)
                + "ogm_" + mapName + ".png";
        final InputStream is = (InputStream) new URL(url).getContent();
        final BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inPreferredConfig = Bitmap.Config.ALPHA_8;

        final Bitmap occupancyGridMap = BitmapFactory.decodeStream(is, null, opts);

        return occupancyGridMap;
    }

    public static boolean isOsmFileFound() {
        return osmFileFound;
    }

    public static boolean isOsmDownloadFinished() {
        return osmDownloadFinished;
    }
}

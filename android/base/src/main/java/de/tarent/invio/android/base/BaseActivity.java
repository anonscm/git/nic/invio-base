package de.tarent.invio.android.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import de.tarent.invio.android.base.config.MapProperties;
import de.tarent.invio.android.base.map.MapPropertiesManager;
import de.tarent.invio.android.base.task.DownloadMapPropertiesTask;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.MapServerClientImpl;

import java.util.concurrent.ExecutionException;

/**
 * This class contains information that all of the activities will need.
 */
public class BaseActivity extends Activity {

    /**
     * True if there was an error fetching the properties from the server and the default properties had to be loaded.
     * False if there were no problems and the default properties were not loaded.
     */
    protected static boolean defaultProperties = false;

    /**
     * The {@link MapServerClient}.
     */
    static MapServerClient mapServerClient; //NOSONAR - Does not need to be private.

    /**
     * The {@link MapProperties}.
     */
    static MapProperties mapProperties; //NOSONAR - Does not need to be private.

    /**
     * True if the app being started is the admin app. False if not. This will prevent the properties being downloaded
     * for the admin app as it's not actually required.
     */
    private static boolean adminApp = false; //NOSONAR: This field should not be final. Admin app changes it to true.

    private static final String TAG = "BaseActivity";

    private static final MapPropertiesManager MAP_PROPERTY_MANAGER = new MapPropertiesManager();

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createMapServerClient();
        downloadMapInformation();
    }

    /**
     * Creates a map server client if one was not already instantiated.
     */
    protected void createMapServerClient() {
        synchronized (BaseActivity.class) {
            if (mapServerClient == null) {
                final String serverEndpoint = getResources().getString(R.string.server_endpoint);
                mapServerClient = new MapServerClientImpl(serverEndpoint);
            }
        }
    }

    /**
     * Downloads the map information, sets the map properties and verifies the properties at the very end.
     */
    protected void downloadMapInformation() {
        synchronized (BaseActivity.class) {
            //TODO: it seems a bit silly that the downloadMapInformation method can only be used once...
            if (!adminApp && mapProperties == null) {
                try {
                    new DownloadMapPropertiesTask(
                            MAP_PROPERTY_MANAGER, mapServerClient, getString(R.string.map_properties)
                    ).execute().get();
                } catch (final InterruptedException e) {
                    Log.e(TAG, e.toString());
                } catch (final ExecutionException e) {
                    Log.e(TAG, e.toString());
                }
                mapProperties = MAP_PROPERTY_MANAGER.getMapProperties();
                verifyMapProperties();
            }
        }
    }

    /**
     * Verifies the map properties. If null, creates a new instance and if the properties aren't set, set the properties
     * with the default values.
     */
    protected void verifyMapProperties() {
        synchronized (BaseActivity.class) {
            if (mapProperties == null) {
                mapProperties = new MapProperties();
            }

            if (!mapProperties.allPropertiesSet()) {
                mapProperties.setGroupName(getString(R.string.map_group_name));
                mapProperties.setShortName(getString(R.string.multilevel_default_map_short_name));
                mapProperties.setUploadLocation(getString(R.string.map_cart_upload));
                defaultProperties = true;
            }
        }
    }

    public static MapProperties getMapProperties() {
        return mapProperties;
    }

    public static void setMapProperties(final MapProperties properties) {
        mapProperties = properties;
    }

    public static void setAdminApp(final boolean adminApp) {
        BaseActivity.adminApp = adminApp;
    }

    public static MapServerClient getMapServerClient() {
        return mapServerClient;
    }
}

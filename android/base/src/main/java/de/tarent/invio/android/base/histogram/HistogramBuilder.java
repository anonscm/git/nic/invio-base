package de.tarent.invio.android.base.histogram;

import de.tarent.invio.android.base.utils.InvioScanResult;
import de.tarent.invio.entities.Histogram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * The HistogramBuilder is used to incrementally build up a Histogram from InvioScanResults.
 * It counts the measured signal strength signalStrengths and converts these numbers into fractions for the final
 * Histogram.
 * <p/>
 * <b>Please note that there is no "reset"-method.</b>
 */
public class HistogramBuilder {

    private final String id;

    /**
     * The maximum age in milliseconds that a scan result my have. Older scans are not included in the final histogram.
     */
    private int maxAge;

    /**
     * In this map we store the scan results, identified by currentTimeMillis.
     */
    private SortedMap<Long, List<InvioScanResult>> scanResults;


    /**
     * Construct a new, empty HistogramBuilder.
     *
     * @param id the id/name for the new Histogram.
     */
    public HistogramBuilder(final String id) {
        this(id, 0);
    }

    /**
     * Construct a new, empty HistogramBuilder with a time-constraint. Scan results older than a certain time span will
     * not be included in the histogram. They will be discarded when they have gotten too old.
     *
     * @param id     the id/name for the new Histogram.
     * @param maxAge the maximum age in milliseconds that a scan result may have for it to be included in the histogram.
     *               0 = ignore the age and just include everything.
     */
    public HistogramBuilder(final String id, final int maxAge) {
        this.id = id;
        this.maxAge = maxAge;
        scanResults = new ConcurrentSkipListMap<Long, List<InvioScanResult>>();
    }


    /**
     * Integrate new ScanResults into this Histogram.
     *
     * @param scan the List of ScanResults as supplied by the Android-WifiManager
     * @return this, for chaining (not that anyone would have multiple ScanResults at the same time)
     */
    public HistogramBuilder addScanResults(final List<InvioScanResult> scan) {
        final List<InvioScanResult> scanCopy = new ArrayList<InvioScanResult>();
        scanCopy.addAll(scan);
        scanResults.put(makeUniqueTime(), scanCopy);

        return this;
    }


    /**
     * Build a new Histogram from the currently collected scan results.
     *
     * @return the new Histogram
     */
    public Histogram build() {
        final Histogram histogram = new Histogram(id);

        // Make the mapping of bssid to signal-level-count-curve:
        final Map<String, SignalStrengths> curves = makeCurves();

        for (final Map.Entry<String, SignalStrengths> curve : curves.entrySet()) {
            histogram.put(curve.getKey(), curve.getValue().getSignalStrengths());
        }
        return histogram;
    }


    /**
     * This method generates a timestamp (in ms), which will be unique as far as the collection of scan results in this
     * HistogramBuilder-instance is concerned. To achieve this goal the method might wait for one millisecond.
     * That should only happen during testing though, because it is unlikely that any device can scan so fast.
     *
     * TODO: change this to a loop, so it doesn't fail if the time changes unexpectedly
     *
     * @return the current (return-) time in milliseconds
     */
    private long makeUniqueTime() {
        long time = System.currentTimeMillis();
        if (scanResults.containsKey(time)) {
            // Normally this method should not be called that fast. We must wait, because we want to use the current
            // time as the index to our Map.
            try {
                Thread.sleep(1);
                time = System.currentTimeMillis();
            } catch (final InterruptedException e) {
                throw new IllegalStateException(
                        "HistogramBuilder.addScanResults must not be called twice during one millisecond!");
            }
        }
        return time;
    }

    /**
     * Add up the individual scan results into histogram curves.
     *
     * @return the mapping of bssid->curve
     */
    private Map<String, SignalStrengths> makeCurves() {
        final long oldestValidTime = (maxAge == 0) ? 0 : System.currentTimeMillis() - maxAge;

        final Map<String, SignalStrengths> curves = new HashMap<String, SignalStrengths>();
        // We use an iterator because it allows us to remove entries on the fly without mixing up our loop:
        final Iterator<Map.Entry<Long, List<InvioScanResult>>> iterator = scanResults.entrySet().iterator();

        while (iterator.hasNext()) {
            final Map.Entry<Long, List<InvioScanResult>> scan = iterator.next();
            if (scan.getKey() >= oldestValidTime) {
                include(scan.getValue(), curves);
            } else {
                iterator.remove();
            }
        }

        return curves;
    }


    /**
     * Add a list of scan results to a map of curves.
     *
     * @param invioScanResults the InvioScanResults
     * @param curves           the curves, indexed by bssid
     */
    private void include(final List<InvioScanResult> invioScanResults, final Map<String, SignalStrengths> curves) {
        for (final InvioScanResult accesspoint : invioScanResults) {
            if (!curves.containsKey(accesspoint.getBssid())) {
                final SignalStrengths signalStrengths = new SignalStrengths();
                curves.put(accesspoint.getBssid(), signalStrengths);
            }
            curves.get(accesspoint.getBssid()).add(accesspoint.getSignalStrength());
        }
    }

    /**
     * The internal signalStrength contains a count for each time signal strength (for one AP) was seen.
     */
    private static class SignalStrengths {

        /**
         * The signalStrengths map the signal strength to their number of occurrences.
         */
        private final Map<Integer, Float> signalStrengths;

        /**
         * This is the normal constructor for when we want to start collecting data into a fresh, empty signal strength
         * map.
         */
        public SignalStrengths() {
            this.signalStrengths = new ConcurrentSkipListMap<Integer, Float>();
        }

        /**
         * Add a new measurement and have it counted.
         *
         * @param signalStrength the new signal strength that was measured
         */
        public void add(final int signalStrength) {
            if (this.signalStrengths.containsKey(signalStrength)) {
                this.signalStrengths.put(signalStrength, this.signalStrengths.get(signalStrength) + 1);
            } else {
                this.signalStrengths.put(signalStrength, 1f);
            }

        }

        public Map<Integer, Float> getSignalStrengths() {
            return signalStrengths;
        }

    }

}

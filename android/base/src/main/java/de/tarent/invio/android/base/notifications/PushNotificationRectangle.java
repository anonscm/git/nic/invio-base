package de.tarent.invio.android.base.notifications;

import de.tarent.invio.entities.InvioGeoPoint;

/**
 * Rectangle that defines a rectangular region in latitude and longitude coordinates
 */
public class PushNotificationRectangle {
    public double minLatitude; //NOSONAR field is used by gson over reflection
    public double minLongitude; //NOSONAR
    public double maxLatitude; //NOSONAR
    public double maxLongitude; //NOSONAR

    /**
     * Method to check if a point is in the notification rectangle.
     *
     * @param geoPoint some InvioGeoPoint
     * @return true if the geoPoint is in the rectangle, otherwise false
     */
    public boolean containsPoint(final InvioGeoPoint geoPoint) {
        final double lat = geoPoint.getLatitudeE6() / 1E6;
        final double lon = geoPoint.getLongitudeE6() / 1E6;

        return  lat >= minLatitude && lat < maxLatitude &&
                lon >= minLongitude && lon < maxLongitude;
    }
}

package de.tarent.invio.android.base.utils;

import android.util.Log;

/**
 * Utility class that helps with the management of dialogs.
 */
public final class DialogManager {

    /**
     * Private constructor to prevent instantiation.
     */
    private DialogManager() {

    }

    /**
     * Keeps a dialog opened for the length of time given. The time when the dialog was opened should be recorded.
     * This method is useful for download and upload dialogs so that the user can read what the dialog says instead
     * of the dialog just flashing on the screen once, giving no time to read.
     *
     * @param timeOpenedMillis  the time when the dialog was opened
     * @param keepOpenForMillis the length of time the dialog should stay opened
     */
    public static void keepDialogOpen(final long timeOpenedMillis, final long keepOpenForMillis) {
        final long lengthOfTimeDialogWasOpened = System.currentTimeMillis() - timeOpenedMillis;

        if (lengthOfTimeDialogWasOpened < keepOpenForMillis) {
            try {
                Thread.sleep(keepOpenForMillis - lengthOfTimeDialogWasOpened);
            } catch (final InterruptedException e) {
                Log.e("DialogManager.keepDialogOpen", e.toString());
            }
        }
    }
}

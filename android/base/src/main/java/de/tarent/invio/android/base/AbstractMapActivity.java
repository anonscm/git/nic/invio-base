package de.tarent.invio.android.base;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import de.tarent.invio.android.base.config.Config;
import de.tarent.invio.android.base.config.ValueProvider;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.android.base.map.IndoorMap;
import de.tarent.invio.android.base.position.UserPositionManager;
import de.tarent.invio.android.base.task.CachedDownloadMapResourceTask;
import de.tarent.invio.android.base.task.UploadPositionTask;
import de.tarent.invio.android.base.tileprovider.TileProviderFactory;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.views.MapView;

import java.util.List;

import static de.tarent.invio.android.base.config.Property.MAP_PROVIDER_SCHEMA;


/**
 * TODO: Translate
 * Jede {@link android.app.Activity} des Invio-Projektes sollte von
 * dieser Klasse erben! Sie beinhaltet nötige Initialisierungs-
 * schritte und Methoden, die allgemein von Bedeutung sind.
 */
public abstract class AbstractMapActivity extends BaseActivity {
    /**
     * The key under which we store our fingerprints in the instance-state-bundle.
     */
    protected static final String WIFI_FINGERPRINTS_JSON = "wifiFingerprintsJson";
    protected static final String BLUETOOTH_FINGERPRINTS_JSON = "bluetoothFingerprintsJson";

    protected static final String EDGES_JSON = "edgesJson";

    /**
     * Static variable so that the user is only asked once to turn on his/her wifi during the entire lifecycle.
     */
    protected static boolean askedToEnableWifi = false; //NOSONAR - Cannot be package protected.

    private static UploadPositionTask uploadPositionTask;

    /**
     * The tag for log-messages from this class:
     */
    private static final String TAG = "GetMapListTask";

    /**
     * The name of the map the current map. Will be used in the URL from which to download tiles, fingerprints,
     * etc. from the server.
     */
    protected String mapName;

    /**
     * The {@link MapView}from osmdroid is the main-feature of this activity.
     */
    protected MapView mapView;

    /**
     * The {@link FingerprintManager}.
     */
    protected FingerprintManager fingerprintManager;

    protected ProgressDialog progressDialog;

    protected MapTileProviderBase tileProvider;


    protected boolean multiLevelMap;

    private int mapViewLayoutId;

    private int osmMapViewId;

    private ValueProvider mConfig;

    private boolean wifiOff;

    private String mapUploadLocation;

    private boolean drawingDebugLines = false;
    private UserPositionManager userPositionManager;

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            final WifiManager wifiManager =
                    (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (progressDialog != null && progressDialog.isShowing()
                    && wifiManager.isWifiEnabled() && wifiOff) {
                wifiOff = false;
                dismissProgressDialog();
                AbstractMapActivity.this.unregisterReceiver(this);
                if (TrackerManager.getInstance().isTracking()) {
                    TrackerManager.getInstance().startTracking();
                }
            }
        }
    };

    /**
     * Constructor.
     */
    protected AbstractMapActivity() {
        initialiseConfig();

        this.mConfig = Config.getInstance();
    }

    /**
     * Get the {@link MapView} - the main feature of this activity.
     *
     * @return the {@link MapView}
     */
    public MapView getMapView() {
        return mapView;
    }

    /**
     * Get the {@link de.tarent.invio.android.base.fingerprint.FingerprintManager}
     *
     * @return the {@link de.tarent.invio.android.base.fingerprint.FingerprintManager}
     */
    public FingerprintManager getFingerprintManager() {
        return fingerprintManager; //NOSONAR Initialized in the implemented activities
    }

    /**
     * Detach all tileProviders on stop to avoid leaks
     */
    @Override
    protected void onStop() {
        super.onStop();

        if (tileProvider != null) {
            tileProvider.detach();
        }

        if (fingerprintManager != null) { // NOSONAR - Unwritten public or protected field - Not always null
            fingerprintManager.detach();
        }

        dismissProgressDialog();
    }

    /**
     * This onCreate method is called when the need to distinguish between layouts is present. For example the
     * admin-app requires a toggle button in the {@link MapView} whereas the kunden-app does not.
     *
     * @param savedInstanceState the {@link Bundle}
     * @param mapViewLayoutId    the layout id for the map view
     * @param osmMapViewId       the is for the osm map view
     */
    protected void onCreate(final Bundle savedInstanceState, final int mapViewLayoutId, final int osmMapViewId) {
        this.mapViewLayoutId = mapViewLayoutId;
        this.osmMapViewId = osmMapViewId;

        handleWifiStatus();
        onBaseCreate(savedInstanceState);
    }

    /**
     * Configure map. Download or restore fingerprints and ways. IMPORTANT: fingerprints will be restored if the map is
     * not an multilevel map.
     *
     * @param savedInstanceState the state that was saved in onSaveInstanceState
     */
    protected void onBaseCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mapName = findMapName();

        setContentView(getMapLayout());
        configureMapView();

        if (multiLevelMap) {
            createMultiLevelMap(savedInstanceState);
        } else {
            createOrRecoverFingerprintManager(savedInstanceState);
        }

        userPositionManager = new UserPositionManager(this, mapView);
    }

    /**
     * Checks for Wifi-Status and then informs the user if wifi is disabled.
     */
    protected void handleWifiStatus() {
        final Context ctx = this.getApplicationContext();
        final WifiManager wifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        final int wifiState = wifiManager.getWifiState();

        switch (wifiState) {
            // For all three states (disabled, disabling and unknown) show the same toast
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
            case WifiManager.WIFI_STATE_UNKNOWN:
                wifiOff = true;
                if (!askedToEnableWifi) {
                    askedToEnableWifi = true;
                    showWifiNotEnabledDialog();
                }
                break;

            default:
                // Everything  is fine, if WiFi is enabled
        }
    }

    /**
     * Show dialog if WiFi is disabled.
     */
    protected void showWifiNotEnabledDialog() {
        final AlertDialog alertDialog = createWifiNotEnabledDialog();
        alertDialog.show();
    }

    private AlertDialog createWifiNotEnabledDialog() { //NOSONAR: Method length is fine.
        final AlertDialog alertDialog = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK).create();
        alertDialog.setTitle(getString(R.string.wifi_not_enabled_dialog_title));
        alertDialog.setMessage(getString(R.string.wifi_not_enabled_dialog_message));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                getString(R.string.activate_wifi),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        activateWifi();
                        showProgressDialog();
                        AbstractMapActivity.this.registerReceiver(receiver,
                                new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                getString(R.string.dialog_button_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        wifiOff = true;
                        dialog.dismiss();
                    }
                });
        return alertDialog;
    }

    private void activateWifi() {
        final WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
    }

    public boolean isWifiOff() {
        return wifiOff;
    }

    public void setWifiOff(final boolean wifiState) {
        wifiOff = wifiState;
    }

    /**
     * show ProgressDialog
     */
    public void showProgressDialog() {
        if (progressDialog == null) {
            createProgressDialog();
        }
        try {
            progressDialog.show();
        } catch (final WindowManager.BadTokenException e) {
            Log.e(TAG, "Progress dialog show was triggered while MapActivity was not running!");
        }

    }

    private void createProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(this.getString(R.string.please_wait_message));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    /**
     * dismiss ProgressDialog
     */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    /**
     * Creates or recovers the fingerprint manager.
     *
     * @param savedInstanceState the saved instance state {@link Bundle}
     */
    protected void createOrRecoverFingerprintManager(final Bundle savedInstanceState) {
        //TODO: Need to save map resources in the Bundle
        new CachedDownloadMapResourceTask(this, mapServerClient, mapView, mapName).execute();
        if (savedInstanceState == null) {
            // For single-level-maps we can download the resources here, but for multi-level-maps this will
            // be handled by the MultiMap.
            createFingerprintManager();
        } else {
            //If savedInstanceState is not null then we can restore fingerprints and ways from Bundle and don't
            //need to download it again.
            restoreFingerprintManagerFromBundle(savedInstanceState);
        }
    }

    /**
     * Each subclass should return the name of the short map which will be shown if no level were detected initially.
     *
     * @return short name of the default map (e.g. the entrance of the multilevel building)
     */
    public abstract String getMultilevelDefaultMapShortName();

    /**
     * Create level buttons depending on the list of the maps.
     *
     * @param mapsList containing indoor maps
     */
    public abstract void createLevelButtons(final List<IndoorMap> mapsList);


    /**
     * Each subclass has to configure it own POI (products) search functionality.
     */
    public abstract void configurePOISearch();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (App.getContext().getResources().getBoolean(R.bool.tracking_mode)) {
            userPositionManager.stopUpdating();
        }
    }

    /**
     * Gets called when a map finished loading and is displayed on the screen.
     *
     * @param map which should be shown
     */
    public abstract void onMapSwitched(final IndoorMap map);

    /**
     * This method is called before an activity may be killed to restore the state in the future. This is the case when
     * user presses the home button
     * IMPORTANT: please note that this method will NOT be called it user navigates back from activity or another
     * activity is launched in front of this activity, because there is no need to it.
     *
     * @param outState the Bundle, in which to save our state
     */
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        if (fingerprintManager != null) { // NOSONAR this field is written by the child-classes only and that's ok
            outState.putString(WIFI_FINGERPRINTS_JSON, fingerprintManager.getWifiFingerprintsJson());
            outState.putString(BLUETOOTH_FINGERPRINTS_JSON, fingerprintManager.getBluetoothLEFingerprintsJson());
        }

        super.onSaveInstanceState(outState);
    }

    /**
     * Create a {@link FingerprintManager} using the savedInstanceState.
     */
    protected abstract void createFingerprintManager();

    /**
     * Create new fingerprintManager instance and put the saved fingerprints from bundle.
     *
     * @param savedInstanceState the Bundle from which to read the data that the FingerprintManager needs.
     */
    protected abstract void restoreFingerprintManagerFromBundle(final Bundle savedInstanceState);

    /**
     * Create a MultiMap using the savedInstanceState.
     *
     * @param savedInstanceState the {@link Bundle} with the savedInstanceState
     */
    protected abstract void createMultiLevelMap(final Bundle savedInstanceState);

    /**
     * Liefert den zu verwendenden {@link ValueProvider}.
     *
     * @return den zu verwendenden {@link ValueProvider}
     */
    protected ValueProvider config() {
        return mConfig;
    }


    /**
     * Find the name of the map in the intent that started this activity. The relevant intent-extra will have been set
     * by the MapSelectionActivity, which comes before this activity to let the user chose a map:
     * Here we also detect whether this is a multi-level-map.
     *
     * @return the name of the map
     */
    protected String findMapName() {
        final Intent intent = getIntent();
        final String mapName = intent.getStringExtra("MapName");
        multiLevelMap = intent.getBooleanExtra("multilevelMap", false);

        return mapName;
    }

    /**
     * Gets the layout of the map view.
     *
     * @return the map view's layout
     */
    protected ViewGroup getMapLayout() {
        final LayoutInflater inflater = getLayoutInflater();
        final View v = inflater.inflate(mapViewLayoutId, null);

        return (ViewGroup) v;
    }

    /**
     * Configures the {@link #mapView}.
     *
     * @return the newly configured {@link #mapView}
     */
    protected MapView configureMapView() {
        mapView = (MapView) findViewById(osmMapViewId);

        // enable software rendering for drawing huge paths
        mapView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        // For multi level maps the tileprovider will later be set by the MultiMap.
        // We set it here anyways to disable the default osmdroid world tiles that will show
        // since osmdroid 4.2 and on some devices pre 4.2
        tileProvider = buildTileProvider();
        mapView.setTileSource(tileProvider.getTileSource());

        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);    //zoom with finger

        return mapView;
    }

    private MapTileProviderBase buildTileProvider() {
        final TileProviderFactory factory = new TileProviderFactory(this);

        return factory.buildWebTileProvider(
                config().getPropertyValue(MAP_PROVIDER_SCHEMA),
                mapName);
    }

    private void initialiseConfig() {
        Config.getInstance().setContext(this);
    }

    /**
     * Starts the position upload task.
     * TODO: Add try catch for when mapUploadLocation wasn't set.
     */
    protected void startPositionUpload() {
        uploadPositionTask = UploadPositionTask.getInstance(mapServerClient, mapUploadLocation, this);
        if (uploadPositionTask.getStatus() == AsyncTask.Status.PENDING) {
            try {
                uploadPositionTask.execute();
            } catch (final IllegalStateException e) {
                Log.e(TAG, "UploadPositionTask executed more than once" + e);
            }
        }
    }

    /**
     * Stops the position upload task.
     */
    protected void stopPositionUpload() {
        uploadPositionTask.cancel(true);
    }

    public void setMapUploadLocation(final String mapUploadLocation) {
        this.mapUploadLocation = mapUploadLocation;
    }

    public int getMapViewLayoutId() {
        return mapViewLayoutId;
    }

    public int getOsmMapViewId() {
        return osmMapViewId;
    }

    public boolean isDrawingDebugLines() {
        return drawingDebugLines;
    }

    public void setDrawingDebugLines(final boolean drawingDebugLines) {
        this.drawingDebugLines = drawingDebugLines;
    }

    public UserPositionManager getUserPositionManager() {
        return userPositionManager;
    }
}

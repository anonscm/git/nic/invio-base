package de.tarent.invio.android.base;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.tarent.invio.android.base.collector.BluetoothLECollector;
import de.tarent.invio.android.base.collector.CollectorListener;
import de.tarent.invio.android.base.collector.HistogramCollector;
import de.tarent.invio.android.base.collector.SensorCollector;
import de.tarent.invio.android.base.collector.SensorDataCollector;
import de.tarent.invio.android.base.collector.WifiCollector;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.Histogram;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.Particle;
import de.tarent.invio.tracker.Tracker;
import de.tarent.invio.tracker.TrackerConfigParameter;
import de.tarent.invio.tracker.TrackerException;
import org.osmdroid.util.BoundingBoxE6;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static android.graphics.Color.colorToHSV;

/**
 * Singleton class that manages everything related to tracking the user position.
 */
public final class TrackerManager {

    /**
     * types of sensors we use for tracking
     */
    public enum SensorMode {
        SENSOR_MODE_WIFI,
        SENSOR_MODE_BLUETOOTH_LE_IBEACON,
        @Deprecated
        SENSOR_MODE_BLUETOOTH_LE
    }

    /**
     * The iBeacon's proximity UUID.
     */
    public static final UUID IBEACON_PROXIMITY_UUID = UUID.fromString("bec26202-a8d8-4a94-b0fc-9ac1de37daa6");

    /**
     * The maximum number of pixels allowed in an occupancy grid map (OGM). Any image that contains more pixels will
     * show an error dialog and
     */
    public static final int MAX_OGM_PIXELS = 1000000;

    /**
     * name of the "has_gyroscope" config parameter
     */
    public static final String HAS_GYROSCOPE = "has_gyroscope";

    private static final long RESTART_CAPTURE_DELAY = 5000;


    private static TrackerManager instance = new TrackerManager();


    private boolean isTracking;
    private boolean histogramCaptureOn;

    private List<Fingerprint> fingerprints;

    private Tracker tracker;

    private SensorDataCollector sensorDataCollector;
    private HistogramCollector histogramCollector;

    private int localisationMode;
    private SensorMode sensorMode;

    private float northAngle;
    private float scale;
    private BoundingBoxE6 boundingBox;

    private Timer captureTimer;

    /**
     * Constructor.
     */
    private TrackerManager() {
        tracker = new Tracker();
        tracker.setDebugLogEnabled(false);

        final boolean hasGyro = hasGyroscope();

        if (hasGyro) {
            setConfigParameter(HAS_GYROSCOPE, "true");
        } else {
            setConfigParameter(HAS_GYROSCOPE, "false");
        }

        setSensorMode(SensorMode.SENSOR_MODE_WIFI);
        setLocalisationMode(Tracker.LOCALISATION_PARTICLE_FILTER);
    }


    /**
     * @see de.tarent.invio.tracker.Tracker#start()
     * <p/>
     * Also starts the SensorCollectors.
     */
    public void startTracking() {
        startTracking(null);
    }


    /**
     * @param context the application {@link Context} where the dialog should be shown. Leave null if no dialog should
     *                be shown.
     * <p/>
     * Also starts the SensorCollectors.
     */
    public void startTracking(final Context context) {
        if (fingerprints != null) {
            if (!isTracking) {
                try {
                    tracker.start();
                    isTracking = true;
                    stopSensorCollectors();
                    createSensorCollectors();
                } catch (final TrackerException e) {
                    Log.e("TrackerManager", e.getErrorMessage());

                    if (e.getErrorCode() == TrackerException.OPTIONAL_MISSING) {
                        isTracking = true; // We can still try, when only optional parts are missing...
                    }

                    if (context != null) {
                        showErrorDialog(context, createTrackerErrorMessage(context, e.getErrorCode()));
                    }
                }
            }
        } else {
            Log.e(getClass().getSimpleName(), "Can't start Tracker: No fingerprints!");
        }
    }


    /**
     * Enable/disable the tracker's debug logging
     *
     * @param value for the debug flag
     */
    public void setDebugLogEnabled(final boolean value) {
        if (tracker != null) {
            tracker.setDebugLogEnabled(value);
        }
    }


    /**
     * Does the current device have a gyroscope?
     *
     * @return true if a gyroscope is present, false otherwise
     */
    public boolean hasGyroscope() {
        final PackageManager packageManager = App.getContext().getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);
    }


    /**
     * <p/>
     * Also stops all SensorCollectors.
     */
    public synchronized void stopTracking() {
        if (isTracking) {
            stopSensorCollectors();
            tracker.stop();
            isTracking = false;
            histogramCaptureOn = false;
        }
    }

    /**
     * pause the histogram tracking for RESTART_CAPTURE_DELAY milliseconds
     */
    public synchronized void pauseHistogramCapture() {
        if (isTracking && histogramCaptureOn) {
            histogramCollector.stopSensors();
            histogramCaptureOn = false;
        }

        if (captureTimer != null) {
            captureTimer.cancel();
        }

        captureTimer = new Timer();
        captureTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                startHistogramCapture();
                captureTimer = null;
            }
        }, RESTART_CAPTURE_DELAY);
    }


    private void showErrorDialog(final Context context, final String errorMessage) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_DARK);
        builder.setTitle(context.getString(R.string.dialog_info_title));
        builder.setMessage(errorMessage);
        builder.setPositiveButton(context.getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }


    private String createTrackerErrorMessage(final Context context, final int errorCode) {
        switch (errorCode) {
            case TrackerException.OPTIONAL_MISSING:
                return context.getString(R.string.tracker_exception_optional_missing);
            case TrackerException.MANDATORY_MISSING:
                return context.getString(R.string.tracker_exception_mandatory_missing);
            default:
                return context.getString(R.string.tracker_exception_unknown, errorCode);
        }
    }


    private void startHistogramCapture() {
        if (isTracking && !histogramCaptureOn) {
            histogramCaptureOn = true;
            histogramCollector.startSensors(new CollectorListener() {
                @Override
                public void onCollectionStart() {

                }

                @Override
                public void onCollectionIteration(final int iteration, final int maxIterations) {
                    final Histogram hist = histogramCollector.getHistogram();
                    if (hist != null && hist.size() > 0 && isTracking()) {
                        tracker.addHistogram(hist);
                    }
                }

                @Override
                public void onCollectionStop() {

                }
            }, SensorCollector.SCAN_COUNT_INFINITE);
        }
    }


    private void createSensorCollectors() {
        createSensorCollector();
        createHistogramCollector();
    }


    /**
     * @return the array of {@link Particle}s
     */
    public Particle[] particles() {
        return tracker.particles();
    }


    /**
     * Sets the list of fingerprints.
     *
     * @param fingerprints the list of fingerprints to set
     */
    public void setFingerprints(final List<Fingerprint> fingerprints) {
        if (fingerprints != null) {
            tracker.setFingerprints(fingerprints);
            this.fingerprints = fingerprints;
        }
    }

    /**
     * Gets the fingerprints that are currently used as a json-string
     *
     * @return the json-string.
     */
    public String getFingerprintsJson() {
        final Gson gson = new GsonBuilder().create();
        return gson.toJson(fingerprints);
    }


    /**
     * @param northAngle the north angle as a float
     */
    public void setNorthAngle(final float northAngle) {
        this.northAngle = northAngle;
        tracker.setNorthAngle(northAngle);
    }

    /**
     * @param scale the scale as a float
     */
    public void setScale(final float scale) {
        this.scale = scale;
        tracker.setScale(scale);
    }

    /**
     * Sets the bounding box.
     *
     * @param boundingBox the {@link BoundingBoxE6}
     */
    public void setBoundingBox(final BoundingBoxE6 boundingBox) {
        this.boundingBox = boundingBox;

        final InvioGeoPoint min = new InvioGeoPoint();
        final InvioGeoPoint max = new InvioGeoPoint();
        min.setLatitudeE6(boundingBox.getLatSouthE6());
        min.setLongitudeE6(boundingBox.getLonWestE6());
        max.setLatitudeE6(boundingBox.getLatNorthE6());
        max.setLongitudeE6(boundingBox.getLonEastE6());

        tracker.setBoundingBox(min, max);
    }


    /**
     * Takes the occupancy grid map bitmap and turns it into a matrix of grayscale values [0,1]
     *
     * @param gridMap the occupancy grid map from the server as a bitmap
     * @return whether or not the grid map was set
     */
    public boolean setOccupancyGridMap(final Bitmap gridMap) {
        if (gridMap != null) {
            final int[] pixelValues = new int[gridMap.getWidth() * gridMap.getHeight()];

            if (pixelValues.length > MAX_OGM_PIXELS) { // If Bitmap contains too many pixels, don't set the grid map.
                return false; // Return false because no grid map was set.
            } else {
                final float[] grayscale = new float[pixelValues.length];
                final float[] hsv = new float[3];

                gridMap.getPixels(pixelValues, 0, gridMap.getWidth(), 0, 0, gridMap.getWidth(), gridMap.getHeight());
                for (int i = 0; i < pixelValues.length; i++) {
                    colorToHSV(pixelValues[i], hsv);
                    grayscale[i] = hsv[2];
                }

                tracker.setOccupancyGridMap(grayscale, gridMap.getWidth(), gridMap.getHeight());
            }
        } else {
            Log.w("TrackerManager", "setOccupancyGridMap(null) => no OGM?");
            return false; // Return false because no grid map was set.
        }
        return true;
    }


    /**
     * @param parameter the parameter as a string
     * @param value     the value as a string
     */
    public void setConfigParameter(final String parameter, final String value) {
        tracker.setConfigParameter(parameter, value);
    }


    /**
     * @param parameter the parameter as a string
     * @return the config parameter string
     */
    public String getConfigParameter(final String parameter) {
        return tracker.getConfigParameter(parameter);
    }


    public TrackerConfigParameter[] getConfigParameters(){
        return tracker.getConfig();
    }


    /**
     * Sets the localisation mode.
     *
     * @param mode the mode to set
     */
    public void setLocalisationMode(final int mode) {
        localisationMode = mode;
        tracker.setLocalisationMode(mode);
    }


    /**
     * Gets the current Localisation Mode.
     * <p/>
     * Possible modes are:
     * <ul>
     * <li>@see de.tarent.invio.android.tracker.Tracker#LOCALISATION_WIFI</li>
     * <li>@see de.tarent.invio.android.tracker.Tracker#LOCALISATION_DEAD_RECKONING</li>
     * <li>@see de.tarent.invio.android.tracker.Tracker#LOCALISATION_PARTICLE_FILTER</li>
     * </ul>
     *
     * @return the mode as an integer.
     */
    public int getLocalisationMode() {
        return localisationMode;
    }


    private synchronized void stopSensorCollectors() {
        if (histogramCollector != null) {
            histogramCollector.stopSensors();
        }

        if (sensorDataCollector != null) {
            sensorDataCollector.stopSensors();
        }
    }


    private synchronized void createHistogramCollector() {
        initHistogramCollector();

        startHistogramCapture();
    }


    private void initHistogramCollector() {
        switch (sensorMode) {
            case SENSOR_MODE_WIFI:
                histogramCollector = new WifiCollector(HistogramCollector.CollectionMode.CONTINUOUS);
                break;

            case SENSOR_MODE_BLUETOOTH_LE_IBEACON: { //NOSONAR - I *want* nested blocks
                final BluetoothLECollector collector =
                        BluetoothLECollector.createCollector(HistogramCollector.CollectionMode.CONTINUOUS);
                collector.setDeviceType(BluetoothLECollector.DeviceType.iBeacon);
                collector.setProximityUUID(IBEACON_PROXIMITY_UUID);
                histogramCollector = collector;
                break;
            }

            case SENSOR_MODE_BLUETOOTH_LE: { //NOSONAR - I *want* nested blocks
                final BluetoothLECollector collector =
                        BluetoothLECollector.createCollector(HistogramCollector.CollectionMode.CONTINUOUS);
                collector.setDeviceType(BluetoothLECollector.DeviceType.BluetoothLE);
                histogramCollector = collector;
                break;
            }

            default:
                // just to shut SONAR up
                break;
        }
    }


    private synchronized void createSensorCollector() {
        sensorDataCollector = new SensorDataCollector();
        sensorDataCollector.startSensors(new CollectorListener() {
            @Override
            public void onCollectionStart() {

            }

            @Override
            public void onCollectionIteration(final int iteration, final int maxIterations) {
                if (isTracking()) {
                    final float[] acc = sensorDataCollector.valuesAccelerometer;
                    final float[] linAcc = sensorDataCollector.valuesLinearAcceleration;
                    final float[] mag = sensorDataCollector.valuesMagneticField;

                    tracker.addSensorData(acc[0], acc[1], acc[2],
                            linAcc[0], linAcc[1], linAcc[2],
                            mag[0], mag[1], mag[2]);
                }
            }

            @Override
            public void onCollectionStop() {

            }
        }, SensorCollector.SCAN_COUNT_INFINITE);
    }


    /**
     * @return the {@link InvioGeoPoint}
     */
    public InvioGeoPoint getCurrentPosition() {
        return tracker.getCurrentPosition();
    }


    /**
     * Gets the navigation path from the start point to the end point.
     *
     * @param start the start {@link InvioGeoPoint}
     * @param goal  the end {@link InvioGeoPoint}
     * @return the planned navigation path
     */
    public InvioGeoPoint[] getNavigation(final InvioGeoPoint start, final InvioGeoPoint goal) {
        return tracker.planPath(start, goal);
    }


    /**
     * Gets the planned shortest path from the start point to the end point.
     *
     * @param start the start {@link InvioGeoPoint}
     * @param goals the end {@link InvioGeoPoint}
     * @return the planned shortest path
     */
    public InvioGeoPoint[] getShortestPath(final InvioGeoPoint start, final InvioGeoPoint[] goals) {
        return tracker.planShortestPath(start, goals);
    }


    /**
     * Gets the current sensor mode
     * <p/>
     * Possible modes are:
     *
     * @return the sensor mode
     * @see de.tarent.invio.android.base.TrackerManager.SensorMode
     */
    public SensorMode getSensorMode() {
        return sensorMode;
    }


    /**
     * Sets the sensor mode
     * <p/>
     * Possible modes are:
     *
     * @param sensorMode the sensor mode
     * @see de.tarent.invio.android.base.TrackerManager.SensorMode
     */
    public void setSensorMode(final SensorMode sensorMode) {
        this.sensorMode = sensorMode;
    }


    /**
     * The current state of the Tracker.
     *
     * @return true if tracking, otherwise false.
     */
    public boolean isTracking() {
        return isTracking;
    }


    /**
     * @return the version string
     */
    public String version() {
        return tracker.version();
    }


    /**
     * Returns the closest level you are currently on.
     * <p/>
     * WARNING: STUB IMPLEMENTATION
     * ALWAYS RETURNS 0
     *
     * @return 0
     */
    public int findClosestLevel() {
        //TODO stub, implementation needed in tracker
        return 0;
    }


    /**
     * Gets the instance of the {@link TrackerManager}.
     *
     * @return the {@link TrackerManager}
     */
    public static TrackerManager getInstance() {
        return instance;
    }


    public float getNorthAngle() {
        return northAngle;
    }


    public float getScale() {
        return scale;
    }


    /**
     * get the tracker config parameters as a yaml-compatible string
     * @return a yaml string
     */
    public String getYamlConfigString(){

        final TrackerConfigParameter[] parameters = getConfigParameters();
        final StringBuilder yamlString = new StringBuilder(parameters.length * 20);

        for (final TrackerConfigParameter parameter : parameters) {
            yamlString.append(parameter.name).append( ": ").append(parameter.currentValue).append("\n");
        }

        return yamlString.toString();
    }


    public BoundingBoxE6 getBoundingBox() {
        return boundingBox;
    }

}

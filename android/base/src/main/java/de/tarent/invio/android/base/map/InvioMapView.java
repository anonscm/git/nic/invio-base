package de.tarent.invio.android.base.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import de.tarent.invio.android.base.AbstractMapActivity;
import org.osmdroid.views.MapView;

/**
 * Class to extend the map view to hold our own flags and actions to handle.
 */
public class InvioMapView extends MapView {

    private static boolean focusing = true;

    private static boolean mapSwitch = false;

    private AbstractMapActivity mapActivity;

    private GestureDetector gestureDetector;

    /**
     * Constructor.
     *
     * @param context the {@link Context}
     * @param attrs   the {@link AttributeSet}
     */
    public InvioMapView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        mapActivity = (AbstractMapActivity) context;
        createGestureDetector();
    }

    private void createGestureDetector() {
        gestureDetector = new GestureDetector(mapActivity, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onScroll(final MotionEvent e1, final MotionEvent e2,
                                    final float distanceX, final float distanceY) {
                focusing = false;
                return false;
            }
        });
    }

    @Override
    public boolean onTouchEvent(final MotionEvent e) {
        gestureDetector.onTouchEvent(e);

        return super.onTouchEvent(e);
    }

    public boolean isFocusing() {
        return focusing;
    }

    /**
     * Sets the {@link InvioMapView#focusing} and stops the starts the tracking.
     *
     * @param focusing true if the map should focus on the user position, false if not
     */
    public void setFocusing(final boolean focusing) {
        InvioMapView.focusing = focusing; //NOSONAR - We're allowed to set this static field here.
    }

    public boolean isMapSwitch() {
        return mapSwitch;
    }

    public void setMapSwitch(final boolean mapSwitch) {
        InvioMapView.mapSwitch = mapSwitch; //NOSONAR - We're allowed to set this static field here.
    }
}

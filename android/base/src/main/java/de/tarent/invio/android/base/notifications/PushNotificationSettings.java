package de.tarent.invio.android.base.notifications;

/**
 * Settings for push notifications to control behaviour
 */
public class PushNotificationSettings {
    private int frequencyCap = 1;
    private int frequencyInterval = 60; //NOSONAR field is used by gson over reflection
    private int autoDismissAfter = 5; //NOSONAR
    private int autoDismissOnExit = 5; //NOSONAR

    public int getFrequencyCap() {
        return frequencyCap;
    }

    public void setFrequencyCap(final int frequencyCap) {
        this.frequencyCap = frequencyCap;
    }
}

package de.tarent.invio.android.base.task;

import android.util.Log;
import de.tarent.invio.android.base.utils.Edge;
import de.tarent.invio.entities.InvioGeoPoint;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.io.IOUtils.closeQuietly;

/**
 * XML parser for the .osm file containing map meta data information such as products, shelves, north angle and
 * maps scale. The parsing is done with JDOM2 parser.
 * <p/>
 * TODO: WARNING: It's recommended to use androids own XMLPullParser due to better performance (as google says).
 */
public class InvioOsmXmlParser implements OsmParser, OsmParserKeys {

    private static final String TAG = InvioOsmXmlParser.class.getCanonicalName();

    private static final String METADATA_SCALE_ATTRIBUTE = "indoor_scale";
    private static final String METADATA_NORTH_ATTRIBUTE = "north_angle";
    private static final String METADATA_MULTILEVEL_ORDER = "multilevel_order";

    /**
     * Geo referenced points which may be referenced by some shelf or way.
     */
    protected Map<String, InvioGeoPoint> points;

    private Integer northAngle;
    private Float indoorScale;
    private Integer multilevelOrder;
    private List<Edge> edges;
    //Namespace map containing maps group name, name and short name (for the floor button label).
    private Map<String, String> namespace = new HashMap<String, String>();
    private InvioGeoPoint entryPoint;
    private Set<InvioGeoPoint> selfCheckoutArea;

    /**
     * Here we put the north angle into the result map.
     * It's a list because that's the convention of the {@link de.tarent.invio.android.base.task.DownloadTask},
     * but of course there's only one north per map.
     *
     * @return northList List with only one element, which is the north angle
     */
    private List<Integer> getNorthAngle() {
        final List<Integer> northList = new ArrayList<Integer>();
        northList.add(northAngle);
        return northList;
    }

    /**
     * Here we put the scale of the indoor map into the result map.
     * It's a list because that's the convention of the {@link de.tarent.invio.android.base.task.DownloadTask},
     * but of course there's only one scale value per indoor map.
     *
     * @return scaleList List with only one element, which is the scale of the indoor map
     */
    private List<Float> getIndoorScale() {
        final List<Float> scaleList = new ArrayList<Float>();
        scaleList.add(indoorScale);
        return scaleList;
    }

    /**
     * Here we put namespace {@link java.util.Map} containing group name, name and short name (floor button name)
     * of the indoor map.
     *
     * @return {@link java.util.List} with only one element, which is the Set containing the bounding box of the
     * self checkout area.
     */
    private List<Map<String, String>> getNamespaceMap() {
        final List<Map<String, String>> namespaceList = new ArrayList<Map<String, String>>();
        namespaceList.add(namespace);
        return namespaceList;
    }

    /**
     * Here we put the self checkout area of the map. You can also call it an "exit" for the current map.
     *
     * @return {@link java.util.List} with only one element, which is the {@link java.util.Map} containing the
     * namespace information.
     */
    private List<Set<InvioGeoPoint>> getSelfCheckoutArea() {
        final List<Set<InvioGeoPoint>> selfCheckoutAreaList = new ArrayList<Set<InvioGeoPoint>>(1);
        selfCheckoutAreaList.add(selfCheckoutArea);
        return selfCheckoutAreaList;
    }

    /**
     * Here we put the entry point of the map.
     *
     * @return {@link java.util.List} with only one element, which is the InvioGeoPoint representing the entry point
     * for the particular map.
     */
    private List<InvioGeoPoint> getEntryPoint() {
        final List<InvioGeoPoint> entryPointList = new ArrayList<InvioGeoPoint>();
        entryPointList.add(entryPoint);
        return entryPointList;
    }

    private List<Edge> getEdges() {
        return edges;
    }

    /**
     * Returns the identifier declaring which floor the particular map is.
     *
     * @return the map's floor identifier
     */
    private List<Integer> getMultilevelOrderIdentifier() {
        final List<Integer> multilevelList = new ArrayList<Integer>();
        multilevelList.add(multilevelOrder);
        return multilevelList;
    }


    @Override
    public Map<String, Collection> getResults() {
        final Map<String, Collection> result = new HashMap<String, Collection>();

        result.put(NORTH_ANGLE, getNorthAngle());
        result.put(INDOOR_SCALE, getIndoorScale());
        result.put(NAMESPACE, getNamespaceMap());
        result.put(ENTRY_POINT, getEntryPoint());
        result.put(SELF_CHECKOUT_AREA, getSelfCheckoutArea());
        result.put(MULTILEVEL_ORDER, getMultilevelOrderIdentifier());
        result.put(EDGES, getEdges());

        return result;
    }

    @Override
    public void parse(final InputStream input) throws IOException {
        try {
            final Document osmFile = new SAXBuilder().build(input);
            final Element rootElement = osmFile.getRootElement();

            //This is very IMPORTANT: we need to read the nodes first because other elements like shelves are holding
            //references to them!
            readNodes(rootElement);
            readWays(rootElement);

        } catch (final JDOMException e) {
            Log.e(TAG, "Exception during parsing of the .osm file. Reason: " + e.getMessage());
        }
        // Might want to use the IOUtilWrapper here for testing, but it's in the mapserver...
        closeQuietly(input);
    }

    private void readWays(final Element rootElement) {
        edges = new ArrayList<Edge>();
        final List<Element> ways = rootElement.getChildren("way");
        for (final Element wayElement : ways) {
            final List<Element> wayTags = wayElement.getChildren("tag");
            if (wayTags != null && wayTags.size() == 1) {
                if (wayTags.get(0).getAttributeValue("v").equals("self_checkout_area")) {
                    selfCheckoutArea = extractSetOfPointsFromWay(wayElement);
                } else if (wayTags.get(0).getAttributeValue("v").equals("Gang")) {
                    extractEdges(wayElement);
                }
            }
        }
    }

    /**
     * Parse nodes, which can be just single geo points, map metadata or products.
     *
     * @param rootElement the root element containing all nodes (<osm> ... </osm>)
     */
    private void readNodes(final Element rootElement) {
        points = new HashMap<String, InvioGeoPoint>();

        //Get all nodes
        final List<Element> nodes = rootElement.getChildren("node");

        for (final Element node : nodes) {
            final List<Element> children = node.getChildren();
            // If a node contains children, then it represents meta data or product, otherwise it's just
            // a simple node with lat lon which is part of the way or shelf closed area.
            if (children.isEmpty()) {
                extractSinglePoint(node);
            } else if (children.get(0).getAttributeValue("v").equals("metadata")) {
                extractMapMetaData(children);
            } else if (children.get(0).getAttributeValue("v").equals("entry")) {
                extractEntryPoint(node);
            }
        }
    }

    private Set<InvioGeoPoint> extractSetOfPointsFromWay(final Element wayElement) {
        final Set<InvioGeoPoint> nodes = new HashSet<InvioGeoPoint>();
        //Iterate over the node elements and put them into a set with previously parsed nodes (points).
        final List<Element> nodeElements = wayElement.getChildren("nd");
        for (final Element node : nodeElements) {
            final String nodeId = node.getAttributeValue("ref");
            nodes.add(new InvioGeoPoint(points.get(nodeId)));
        }
        return nodes;
    }

    private void extractEdges(final Element wayElement) {
        final List<InvioGeoPoint> nodes = new ArrayList<InvioGeoPoint>();
        //Iterate over the node elements and put them into a set with previously parsed nodes (points).
        final List<Element> nodeElements = wayElement.getChildren("nd");
        for (final Element node : nodeElements) {
            final String nodeId = node.getAttributeValue("ref");
            nodes.add(new InvioGeoPoint(points.get(nodeId)));
        }
        final int numberOfEdges = nodes.size() - 1;
        for (int i = 0; i < numberOfEdges; i++) {
            edges.add(new Edge(nodes.get(i), nodes.get(i + 1)));
        }
    }

    /**
     * Read single geo points which are belong to a way or shelf.
     *
     * @param node a single node with no children elements
     */
    private void extractSinglePoint(final Element node) {
        final String id = node.getAttributeValue("id");
        final double lat = Double.parseDouble(node.getAttributeValue("lat"));
        final double lon = Double.parseDouble(node.getAttributeValue("lon"));

        points.put(id, new InvioGeoPoint(lat, lon));
    }


    private void extractEntryPoint(final Element node) {
        final double lat = Double.parseDouble(node.getAttributeValue("lat"));
        final double lon = Double.parseDouble(node.getAttributeValue("lon"));
        entryPoint = new InvioGeoPoint(lat, lon);
    }

    /**
     * Extract the map metadata such as north angle, scale, map name and so on.
     *
     * @param children the child elements of the metadata element.
     */
    private void extractMapMetaData(final List<Element> children) {
        for (final Element element : children) {
            final String keyValue = element.getAttributeValue("k");

            if (keyValue.equals(METADATA_NORTH_ATTRIBUTE)) {
                northAngle = Integer.valueOf(element.getAttributeValue("v"));
            } else if (keyValue.equals(METADATA_SCALE_ATTRIBUTE)) {
                indoorScale = Float.valueOf(element.getAttributeValue("v"));
            } else if (keyValue.equals(METADATA_MULTILEVEL_ORDER)) {
                multilevelOrder = Integer.valueOf(element.getAttributeValue("v"));
            } else if (keyValue.startsWith("namespace_")) {
                namespace.put(keyValue, element.getAttributeValue("v"));
            }
        }
    }
}

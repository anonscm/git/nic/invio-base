package de.tarent.invio.android.base.task;


/**
 * The interface OsmParserKeys holds all the key-strings that can be found in the results that an OsmParser provides.
 */
public interface OsmParserKeys {

    String EDGES = "edges";

    String NORTH_ANGLE = "northAngle";

    String INDOOR_SCALE = "indoorScale";

    String NAMESPACE = "namespace";

    String MULTILEVEL_ORDER = "multilevelOrder";

    String ENTRY_POINT = "entryPoint";

    String SELF_CHECKOUT_AREA = "selfCheckoutArea";

}

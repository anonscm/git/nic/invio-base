package de.tarent.invio.android.base.task;

import android.util.Log;
import de.tarent.invio.android.base.fingerprint.FingerprintManager;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;


/**
 * This is the AsyncTask which downloads the fingerprints from the server, in the background.
 */
public class DownloadFingerprintsTask extends DownloadTask<Void, Void, Void> {

    public static final String TAG = "DownloadFingerprintsTask";

    private static final int FINGERPRINT_WIFI = 0;
    private static final int FINGERPRINT_BLUETOOTH = 1;
    private static final String FINGERPRINT_DOWNLOAD_FAILED_TAG = "Failed to download fingerprints: ";
    private static final String ENCODING = "UTF-8";

    protected final String mapName;

    protected final MapServerClient mapServerClient;

    private FingerprintManager fingerprintManager;

    /**
     * Construct a new DownloadFingerprintsTask, which belongs to a specific FingerprintManager
     *
     * @param fingerprintManager the owner of this task
     * @param mapServerClient    the client which should be used for talking to the map-server
     * @param mapName            the name of the map for which we shall download the fingerprints
     */
    public DownloadFingerprintsTask(final FingerprintManager fingerprintManager,
                                    final MapServerClient mapServerClient,
                                    final String mapName) {
        this.fingerprintManager = fingerprintManager;
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
    }

    @Override
    protected Void doInBackground(final Void... params) {
        try {
            fingerprintManager.setWifiFingerprintsJson(
                    inputStreamToString(getWifiFingerprintInputStream()));
            fingerprintManager.setBluetoothLEFingerprintsJson(
                    inputStreamToString(getBluetoothLEFingerprintInputStream()));

            fingerprintManager.setWifiFingerprintsProbabilityJson(
                    inputStreamToString(getWifiFingerprintProbabilityInputStream()));
            fingerprintManager.setBluetoothLEFingerprintsProbabilityJson(
                    inputStreamToString(getBluetoothLEFingerprintProbabilityInputStream()));

            fingerprintManager.setTransmitterNames(
                    inputStreamToString(getTransmitterNamesInputStream()));

            success = true;
        } catch (final UnsupportedEncodingException e) {
            success = false;
            Log.e(TAG, "Unsupported ENCODING " + e);
        } catch (final FileNotFoundException e) {
            success = false;
            Log.e(TAG, "File not found " + e);
        }

        return null;
    }

    private String inputStreamToString(final InputStream inputStream) {
        final StringWriter stringWriter = new StringWriter();
        try {
            IOUtils.copy(inputStream, stringWriter, ENCODING);
        } catch (final IOException e) {
            success = false;
            Log.e(TAG, "Failed to read fingerprints: " + e);
        }
        return stringWriter.toString();
    }


    // TODO: give notification in case of failure. Don't just log the errors. Do something :-)
    private String downloadTransmitterNames() {
        String json = "[]";
        try {
            json = mapServerClient.downloadTransmitterNames(mapName);
            success = true;
        } catch (final InvioException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        } catch (final IOException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        }

        return json;
    }

    // TODO: give notification in case of failure. Don't just log the errors. Do something :-)
    private String downloadFingerprints(final int type) {
        String json = "[]";
        try {
            if (type == FINGERPRINT_BLUETOOTH) {
                json = mapServerClient.downloadBluetoothLEFingerprintsDataWithCount(mapName);
            } else {
                json = mapServerClient.downloadWifiFingerprintsDataWithCount(mapName);
            }
            success = true;
        } catch (final InvioException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        } catch (final IOException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        }

        return json;
    }


    private String downloadFingerprintsProbability(final int type) {
        String json = "[]";
        try {
            if (type == FINGERPRINT_BLUETOOTH) {
                json = mapServerClient.downloadBluetoothLEFingerprintsData(mapName);
            } else {
                json = mapServerClient.downloadWifiFingerprintsData(mapName);
            }
            success = true;
        } catch (final InvioException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        } catch (final IOException e) {
            success = false;
            Log.e(TAG, FINGERPRINT_DOWNLOAD_FAILED_TAG + e);
        }

        return json;
    }

    /**
     * Get the {@link InputStream} with the wifi fingerprints from somewhere. In this case: from the mapserver.
     * But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given ENCODING is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getWifiFingerprintInputStream() throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprints(FINGERPRINT_WIFI);
        return new ByteArrayInputStream(json.getBytes(ENCODING));
    }


    /**
     * Get the {@link InputStream} with the wifi fingerprints with probability from somewhere. In this case: from the
     * mapserver. But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given ENCODING is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getWifiFingerprintProbabilityInputStream()
            throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprintsProbability(FINGERPRINT_WIFI);
        return new ByteArrayInputStream(json.getBytes(ENCODING));
    }

    /**
     * Get the {@link InputStream} with the bluetooth le fingerprints from somewhere. In this case: from the mapserver.
     * But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given ENCODING is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getBluetoothLEFingerprintInputStream()
            throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprints(FINGERPRINT_BLUETOOTH);
        return new ByteArrayInputStream(json.getBytes(ENCODING));
    }

    /**
     * Get the {@link InputStream} with the bluetooth fingerprints with probability from somewhere. In this case: from
     * the mapserver. But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the fingerprints can be read.
     * @throws UnsupportedEncodingException when the given ENCODING is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing fingerprints data file
     */
    protected InputStream getBluetoothLEFingerprintProbabilityInputStream()
            throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadFingerprintsProbability(FINGERPRINT_BLUETOOTH);
        return new ByteArrayInputStream(json.getBytes(ENCODING));
    }

    /**
     * Get the {@link InputStream} with the names of transmitter from somewhere. In this case: from
     * the mapserver. But you can override this method to get it from somewhere else.
     *
     * @return the InputStream from which the names of transmitter can be read.
     * @throws UnsupportedEncodingException when the given ENCODING is invalid
     * @throws FileNotFoundException        when overwriting methods cannot find the existing transmitter names data file
     */
    protected InputStream getTransmitterNamesInputStream()
            throws UnsupportedEncodingException, FileNotFoundException {
        final String json = downloadTransmitterNames();
        return new ByteArrayInputStream(json.getBytes(ENCODING));
    }

}

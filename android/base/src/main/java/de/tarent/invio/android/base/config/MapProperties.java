package de.tarent.invio.android.base.config;

import java.util.Properties;

/**
 * Properties class for the map.
 *
 * The properties are not optimal and need to be reworked. Some of the changes that should be made in the future can be
 * found here: https://evolvis.org/plugins/mediawiki/wiki/nic/index.php/Properties
 */
public class MapProperties extends Properties {

    /**
     * Corresponds to the tag (key) of the same name in the osm files. Is only interesting when using multi level maps,
     * which we do not (anymore). With simple maps, as we have them now, it just serves as the key for an unnecessary
     * level of indirection when the server finds the data for the map.
     */
    private static final String GROUP_NAME_KEY = "namespace_group_name";

    /**
     * Corresponds to the tag (key) of the same name in the osm files. Would have been used to differentiate between the
     * individual maps in one group and select a default map among them, when the level auto detection would not produce
     * a result. Doesn't apply at the moment (we have no multi maps).
     */
    private static final String SHORT_NAME_KEY = "namespace_short_name";

    /**
     * This is the name of the directory where the map is located. This was used for giving a location to upload to when
     * multiple maps were involved, but now it's sometimes used to find map information and such.
     */
    private static final String UPLOAD_LOCATION_KEY = "upload_location";

    /**
     * Gets the group name from the properties.
     *
     * @return the group name
     */
    public String getGroupName() {
        return getProperty(GROUP_NAME_KEY);
    }

    /**
     * Sets the property: group name.
     *
     * @param value the group name
     */
    public void setGroupName(final String value) {
        put(GROUP_NAME_KEY, value);
    }

    /**
     * Gets the short name from the properties.
     *
     * @return the short name
     */
    public String getShortName() {
        return getProperty(SHORT_NAME_KEY);
    }

    /**
     * Sets the property: short name.
     *
     * @param value the value to set
     */
    public void setShortName(final String value) {
        put(SHORT_NAME_KEY, value);
    }

    /**
     * Gets the upload location from the properties.
     *
     * @return the upload location
     */
    public String getUploadLocation() {
        return getProperty(UPLOAD_LOCATION_KEY);
    }

    /**
     * Sets the property: upload location.
     *
     * @param value the value
     */
    public void setUploadLocation(final String value) {
        put(UPLOAD_LOCATION_KEY, value);
    }

    /**
     * Checks if every property value was set. If one or more return null, the method returns false.
     *
     * @return returns false if one or more properties returns null
     */
    public boolean allPropertiesSet() {
        if (getGroupName() == null || getShortName() == null || getUploadLocation() == null) {
            return false;
        }

        return true;
    }
}

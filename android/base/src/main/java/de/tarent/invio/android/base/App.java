package de.tarent.invio.android.base;

import android.app.Application;
import android.content.Context;

/**
 * The application context for the entire application. Primarily used for accessing android resources without having to
 * pass the context every time. <b>This should NOT be used when inflating layouts!</b>
 */
public class App extends Application {

    private static App instance;

    /**
     * Constructor to set the instance.
     */
    public App() {
        instance = this; //NOSONAR - Writing to this static field from the constructor is required for android.
    }

    /**
     * Gets the {@link Context} in a static way.
     *
     * @return the {@link Context}
     */
    public static Context getContext() {
        return instance;
    }

}
package de.tarent.invio.android.base.fingerprint;

import de.tarent.invio.entities.Fingerprint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.OverlayItem;

/**
 * The FingerprintItem is an OverlayItem that represents a wifi/btle Fingerprint.
 */
public class FingerprintItem extends OverlayItem {
    private Fingerprint wifiFingerprint;
    private Fingerprint bluetoothLEFingerprint;

    /**
     * Create a new FingerprintItem.
     */
    public FingerprintItem() {
        super("", "", new GeoPoint(0, 0));
    }

    /**
     * Sets the correct geo point location.
     *
     * @param latE6  the latitude in E6 format
     * @param longE6 the longitude in E6 format
     */
    public void setGeoLocation(final int latE6, final int longE6) {
        getPoint().setCoordsE6(latE6, longE6);
    }

    /**
     * Sets the wifi fingerprint and then sets the correct icon if the fingerprint is bluetooth only.
     *
     * @param wifiFingerprint the wifi fingerprint
     */
    public void setWifiFingerprint(final Fingerprint wifiFingerprint) {
        this.wifiFingerprint = wifiFingerprint;
        if (bluetoothLEFingerprint == null) {
            setMarker(new FingerprintDrawable(wifiFingerprint, null));
        }
    }

    /**
     * Sets the bluetooth fingerprint and then sets the correct icon.
     *
     * @param bluetoothLEFingerprint the bluetooth fingerprint
     */
    public void setBluetoothLEFingerprint(final Fingerprint bluetoothLEFingerprint) {
        this.bluetoothLEFingerprint = bluetoothLEFingerprint;
        setMarker(new FingerprintDrawable(wifiFingerprint, bluetoothLEFingerprint));
    }

    public Fingerprint getWifiFingerprint() {
        return wifiFingerprint;
    }

    public Fingerprint getBluetoothLEFingerprint() {
        return bluetoothLEFingerprint;
    }
}

package de.tarent.invio.android.base.collector;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Class that holds the information for an iBeacon.
 */
public final class IBeacon {
    private static final byte[] APPLE_IDENTIFIER = {0x4C, 0x00};
    private static final byte[] IBEACON_SERVICE_IDENTIFIER = {0x02, 0x15};

    private UUID proximityUUID;
    private short major;
    private short minor;
    private byte txPower;

    /**
     * Default constructor.
     */
    private IBeacon() {

    }

    public UUID getProximityUUID() {
        return proximityUUID;
    }

    public void setProximityUUID(final UUID proximityUUID) {
        this.proximityUUID = proximityUUID;
    }

    public short getMajor() {
        return major;
    }

    public void setMajor(final short major) {
        this.major = major;
    }

    public short getMinor() {
        return minor;
    }

    public void setMinor(final short minor) {
        this.minor = minor;
    }

    public byte getTxPower() {
        return txPower;
    }

    public void setTxPower(final byte txPower) {
        this.txPower = txPower;
    }

    /**
     * Reads advertisementData (also known as scanRecord) from an Bluetooth LE Scan and returns an iBeacon on success.
     * <p/>
     * Example advertisementData from an iBeacon:
     * <p/>
     * 02 01 06 // flags
     * 1a ff 4c 00 02 15 // manufacturer data (2 bytes), apple identifier (2 bytes), iBeacon identifier (2 bytes)
     * aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa aa // 16 bytes, iBeacon UUID
     * 00 01 00 01 // iBeacon major (2 bytes, short, little endian), minor (2 bytes, short, little endian)
     * c6 // tx power (1 byte, little endian)
     *
     * @param advertisementData advertismentData as byte array.
     * @return iBeacon containing uuid, major, minor and txPower if advertisementData contains iBeacon data, null if not
     */
    public static IBeacon fromAdvertisementData(final byte[] advertisementData) {
        final ByteBuffer bb = ByteBuffer.wrap(advertisementData);

        while (bb.hasRemaining()) {
            if (bb.get() == APPLE_IDENTIFIER[0]
                    && bb.get() == APPLE_IDENTIFIER[1]
                    && bb.get() == IBEACON_SERVICE_IDENTIFIER[0]
                    && bb.get() == IBEACON_SERVICE_IDENTIFIER[1]) {

                //check if the remaining data can possibly hold a proximityUUID, 2 shorts and 1 byte
                if (bb.remaining() < 21) {
                    return null;
                }

                final IBeacon beacon = new IBeacon();
                beacon.proximityUUID = new UUID(bb.getLong(), bb.getLong());
                beacon.major = bb.getShort();
                beacon.minor = bb.getShort();
                beacon.txPower = bb.get();

                return beacon;
            }
        }

        return null;
    }
}

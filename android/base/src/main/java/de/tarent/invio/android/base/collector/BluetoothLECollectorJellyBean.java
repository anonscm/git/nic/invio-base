package de.tarent.invio.android.base.collector;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import de.tarent.invio.android.base.histogram.HistogramBuilder;
import de.tarent.invio.android.base.utils.InvioScanResult;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

/**
 * a BluetootLECollector implementation based on the Android 4.x (JellyBean) API
 */
@TargetApi(18)
public class BluetoothLECollectorJellyBean extends BluetoothLECollector {

    private static final String TAG = "BTLE_JB";

    /**
     * The interval in which the progress bar will be updated.
     */
    private static final int UPDATE_INTERVAL = 1000;

    private BluetoothAdapter.LeScanCallback callback;

    // true: start btle only once. false: start/stop scanning on every timer cycle
    private boolean startScanningOnlyOnce = false;

    /**
     * Construct a new BluetoothLECollectorJellyBean
     *
     * @param collectionMode the collection mode to use
     */
    public BluetoothLECollectorJellyBean(CollectionMode collectionMode) {
        super(collectionMode);

        createCallback();
    }

    @Override
    public void startSensors(final CollectorListener collectorListener, final int scanSeconds) {
        if (bluetoothEnabled && bluetoothAdapter != null) {
            Log.d(TAG, "StartSensors");

            histogramBuilder = new HistogramBuilder("Bluetooth LE Histogram " + new Date().toString(), maxAge);

            this.maxSeconds = scanSeconds;
            this.secondsIncrement = UPDATE_INTERVAL / 1000;
            this.secondsCounter = 0;
            this.lastHistogram = null;
            this.collectorListener = collectorListener;

            if (collectorListener != null) {
                collectorListener.onCollectionStart();
            }

            isScanning = true;
            if (this.startScanningOnlyOnce) {
                final boolean ok = bluetoothAdapter.startLeScan(callback);
                Log.i(TAG, "start scan=" + ok);
            }

            scheduleNextScan();
        }
    }

    @Override
    public void stopSensors() {

        if (bluetoothEnabled && bluetoothAdapter != null) {
            Log.d(TAG, "StopSensorsBTLE");

            if (collectorListener != null) {
                collectorListener.onCollectionStop();
            }

            try {
                stopScan();
                lastHistogram = null;
            } catch (final IllegalArgumentException e) {
                Log.e(TAG, "The receiver was not registered or already unregistered: " + e.getMessage());
            }
        }

        isScanning = false;
        if (this.startScanningOnlyOnce) {
            bluetoothAdapter.stopLeScan(callback);
        }

        for (final String bssid : counts.keySet()) {
            final int seen = counts.get(bssid);
            final int interval = (this.maxSeconds * 1000) / seen;
            Log.i(TAG, bssid + " seen " + counts.get(bssid) + ", interval=" + interval);
        }

    }

    private void scheduleNextScan() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isScanning) {
                    doScan();
                }
            }
        }, UPDATE_INTERVAL);
    }

    private void doScan() {

        //stop the scan, because of wifi/bluetooth bug in Nexus 4/7 see wiki Bluetooth Evaluation
        if (!this.startScanningOnlyOnce) {
            bluetoothAdapter.stopLeScan(callback);
            final boolean ok = bluetoothAdapter.startLeScan(callback);
            Log.i(TAG, "start scan=" + ok);
        }

        lastHistogram = histogramBuilder.build();
        SensorDataLogHelper.logHistogram(lastHistogram, SensorDataLogHelper.HistogramLogType.BluetoothLE);

        if (collectorListener != null) {
            collectorListener.onCollectionIteration((int)secondsCounter, maxSeconds);
        }

        secondsCounter += secondsIncrement;
        if (maxSeconds != -1 && secondsCounter >= maxSeconds) {
            stopSensors();
        } else {
            scheduleNextScan();
        }
    }

    private void stopScan() {
        bluetoothAdapter.stopLeScan(callback);
        timer.cancel();
        timer.purge();
    }

    private void createCallback() {
        callback = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice bluetoothDevice, final int level, final byte[] advertisingData) {
                final List<InvioScanResult> btleInvioScanResults = new ArrayList<InvioScanResult>();
                final IBeacon beacon = IBeacon.fromAdvertisementData(advertisingData);

                Log.i(TAG, "scan callback on iteration " + secondsCounter + " " + bluetoothDevice.toString());

                if (beacon != null && deviceType == DeviceType.iBeacon) {

                    // We are only interested in our own beacons:
                    if (proximityUUID != null && !beacon.getProximityUUID().equals(proximityUUID)) {
                        return;
                    }

                    final String bssid = beacon.getProximityUUID() + "_" + beacon.getMajor() + "_" + beacon.getMinor();

                    Log.i(TAG, "saw bssid=" + bssid);

                    final Integer i = counts.get(bssid);
                    counts.put(bssid, i==null ? 1 : i+1);

                    btleInvioScanResults.add(new InvioScanResult(bssid, level));
                    histogramBuilder.addScanResults(btleInvioScanResults);

                } else if (deviceType == DeviceType.BluetoothLE) {
                    btleInvioScanResults.add(new InvioScanResult(bluetoothDevice.getAddress(), level));
                    histogramBuilder.addScanResults(btleInvioScanResults);
                }
            }
        };
    }
}

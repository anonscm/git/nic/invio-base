package de.tarent.invio.android.base.collector;

/**
 * Callback for the collection of fingerprints.
 */
public interface CollectorListener {

    /**
     * Called when the collection is started.
     */
    void onCollectionStart();

    /**
     * Called when each new capture iteration starts.
     *
     * @param iteration     the current iteration
     * @param maxIterations the maximum number of iterations, -1 for infinite iterations
     */
    void onCollectionIteration(final int iteration, final int maxIterations);

    /**
     * Called when the capturing is stopped.
     */
    void onCollectionStop();

}

package de.tarent.invio.android.base.fingerprint;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.json.InvioGeoPointDeserializer;
import de.tarent.invio.android.base.task.CachedDownloadFingerprintsTask;
import de.tarent.invio.entities.Fingerprint;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.mapserver.MapServerClient;
import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The FingerprintManager holds the wifi-fingerprints and the overlay to display them.
 * It is also responsible for uploading and downloading fingerprints to and from the mapserver.
 */
public class FingerprintManager implements ItemizedIconOverlay.OnItemGestureListener<FingerprintItem> {
    //static boolean to check if bluetooth was denied by the user already
    private static boolean bluetoothAccessWasDenied = false;

    protected Activity activity;

    protected ItemizedIconOverlay<FingerprintItem> overlay;


    // wifiMapping is used to map a wifi-fingerprints id to the overlay index in the map
    // so that we can retreive it later when we get the same id for btle
    protected Map<String, Integer> wifiMapping = new HashMap<String,Integer>();

    protected MapView mapView;

    protected String mapName; // NOSONAR: this field is read by the AdminFingerprintManager

    /**
     * The MapServerClient is used by all the tasks that need to talk to the map-server:
     */
    protected MapServerClient mapServerClient;

    private List<Fingerprint> fingerprintsWifiProbability;
    private List<Fingerprint> fingerprintsBluetoothProbability;
    private Map<String, String> transmitterNames;

    /**
     * Construct a new FingerprintManager.
     *
     * @param activity        the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient the {@link MapServerClient}
     * @param mapView         the {@link MapView}
     * @param mapName         the name of the map, as it is used in the mapserver-URLs
     */
    public FingerprintManager(final Activity activity,
                              final MapServerClient mapServerClient,
                              final MapView mapView,
                              final String mapName) {
        this(activity, mapServerClient, mapView, mapName, true, true);
    }

    /**
     * Construct a new FingerprintManager.
     *
     * @param activity             the parent activity, from which we need the Context and FragmentManager
     * @param mapServerClient      the {@link MapServerClient}
     * @param mapView              the {@link MapView}
     * @param mapName              the name of the map, as it is used in the mapserver-URLs
     * @param downloadFingerprints whether to download fingerprints-data or not. Can be skipped for multi-maps, because
     *                             for those all the download-tasks are triggered by the MultiMap.
     * @param bluetoothRequired    whether or not bluetooth is actually required by the app.
     */
    public FingerprintManager(final Activity activity,
                              final MapServerClient mapServerClient,
                              final MapView mapView,
                              final String mapName,
                              final boolean downloadFingerprints,
                              final boolean bluetoothRequired) {
        this.activity = activity;
        this.mapView = mapView;
        this.mapName = mapName; //NOSONAR - this field is read by the AdminFingerprintManager
        this.mapServerClient = mapServerClient; //NOSONAR - this field is read by the AdminFingerprintManager

        if (bluetoothRequired) {
            enableBluetoothAdapter();
        }

        overlay = createFingerprintOverlay(activity); //NOSONAR - yes, we want subclasses to override this method.
        mapView.getOverlays().add(overlay);

        if (downloadFingerprints) {
            new CachedDownloadFingerprintsTask(this, mapServerClient, mapName).execute();
        }
    }

    /**
     * Remove a FingerprintItem from the overlay.
     *
     * @param item the FingerprintItem we want to delete
     */
    public void removeFingerprintFromOverlay(final FingerprintItem item) {
        overlay.removeItem(item);
        mapView.postInvalidate();
    }

    /**
     * Remove all FingerprintItem from the overlay.
     */
    public void removeAllFingerprintFromOverlay() {
        overlay.removeAllItems();
        mapView.postInvalidate();
    }

    /**
     * Get the list of the names for wholes transmitters.
     *
     * @return The list of the names for transmitters
     */
    public Map<String, String> getTransmitterNames() {
        return this.transmitterNames;
    }

    /**
     * Sets the list of transmitter names.
     *
     * @param json the transmitter names as json
     */
    public void setTransmitterNames(final String json) {
        final Type type = new TypeToken<Map<String, String>>(){}.getType();
        final Gson gson = new Gson();
        this.transmitterNames = gson.fromJson(json, type);
    }

    /**
     * Get the list of Wifi Fingerprints that this manager holds. Note that this is not a simple getter. It needs to do
     * some work to extract this list from the mapview-overlay.
     *
     * @return the list of Fingerprint-objects.
     */
    public List<Fingerprint> getWifiFingerprints() {
        final List<Fingerprint> fingerprints = new ArrayList<Fingerprint>();
        // Maybe we don't have an active overlay yet:
        if (overlay != null) {
            final int size = overlay.size();
            for (int i = 0; i < size; i++) {
                final Fingerprint fp = overlay.getItem(i).getWifiFingerprint();
                if(fp != null) {
                    fingerprints.add(fp);
                }
            }
        }

        return fingerprints;
    }

    public List<Fingerprint> getWifiFingerprintsWithProbability() {
        return this.fingerprintsWifiProbability;
    }

    /**
     * Get the list of Bluetooth LE Fingerprints that this manager holds. Note that this is not a simple getter. It
     * needs to do some work to extract this list from the mapview-overlay.
     *
     * @return the list of Fingerprint-objects.
     */
    public List<Fingerprint> getBluetoothLEFingerprints() {
        final List<Fingerprint> fingerprints = new ArrayList<Fingerprint>();
        // Maybe we don't have an active overlay yet:
        if (overlay != null) {
            final int size = overlay.size();
            for (int i = 0; i < size; i++) {
                final Fingerprint fp = overlay.getItem(i).getBluetoothLEFingerprint();
                if(fp != null) {
                    fingerprints.add(fp);
                }
            }
        }

        return fingerprints;
    }

    public List<Fingerprint> getBluetoothLEFingerprintsWithProbability() {
        return this.fingerprintsBluetoothProbability;
    }


    /**
     * Get a json-string that contains a serialization of the current wifi fingerprints.
     *
     * @return the fingerprints as json
     */
    public String getWifiFingerprintsJson() {
        final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();

        return gson.toJson(getWifiFingerprints());
    }

    /**
     * Get a json-string that contains a serialization of the current btle fingerprints.
     *
     * @return the fingerprints as json
     */
    public String getBluetoothLEFingerprintsJson() {
        final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();

        return gson.toJson(getBluetoothLEFingerprints());
    }

    /**
     * Deserialize a json-string into wifi fingerprints.
     *
     * @param json the wifi fingerprints as json
     */
    public void setWifiFingerprintsJson(final String json) {
        setFingerprintJson(json, false);
    }

    public void setWifiFingerprintsProbabilityJson(final String json) {
        this.fingerprintsWifiProbability = getFingerprintsFromJson(json, false, false);
    }

    /**
     * Deserialize a json-string into bluetooth fingerprints.
     *
     * @param json the bluetooth fingerprints as json
     */
    public void setBluetoothLEFingerprintsJson(final String json) {
        setFingerprintJson(json, true);
    }

    public void setBluetoothLEFingerprintsProbabilityJson(final String json) {
        this.fingerprintsBluetoothProbability = getFingerprintsFromJson(json, true, false);
    }

    private void setFingerprintJson(final String json, final boolean isBluetooth) {
        final List<Fingerprint> fingerprints = getFingerprintsFromJson(json, isBluetooth, true);

        if (fingerprints != null) {
            setFingerprints(fingerprints, isBluetooth);
        }
    }

    private void setFingerprints(final List<Fingerprint> fingerprints, final boolean isBluetooth) {
        for (int i = 0; i < fingerprints.size(); i++) {
            final Fingerprint fingerprint = fingerprints.get(i);
            if (fingerprint != null) {

                final FingerprintItem item = getOverlayItem(fingerprint, i, isBluetooth);

                if (isBluetooth) {
                    item.setBluetoothLEFingerprint(fingerprint);
                } else {
                    item.setWifiFingerprint(fingerprint);
                }

                item.setGeoLocation(
                        fingerprint.getPoint().getLatitudeE6(),
                        fingerprint.getPoint().getLongitudeE6());

                mapView.postInvalidate();
            }
        }
    }

    // NB: this depends on the order of setXxxFingerprintJson calls - wifi first, the btle
    // TODO: enforce this order!
    private FingerprintItem getOverlayItem(final Fingerprint fingerprint, final int index, boolean isBluetooth) {
        final FingerprintItem item;

        Integer wifiIndex = null;

        if (isBluetooth) {
            // check if we already have an index for this fp
            wifiIndex = wifiMapping.get(fingerprint.getId());
        } else {
            // add this fp to our wifiMapping
            wifiMapping.put(fingerprint.getId(), index);
        }

        if (wifiIndex == null) {
            // create a new FingerprintItem
            item = new FingerprintItem();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    overlay.addItem(item);
                }
            });
        } else {
            // Use the wifiMapping to get the existing WifiFingerprint to update it
            // with btle information
            item = overlay.getItem(wifiIndex);
        }

        return item;
    }


    private List<Fingerprint> getFingerprintsFromJson(final String json, final boolean isBluetooth,
                                                      final boolean isRawData) {
        final List<Fingerprint> fingerprints = parseJson(json);

        final TrackerManager.SensorMode sensorMode = TrackerManager.getInstance().getSensorMode();
        if (!isRawData && (!isBluetooth && sensorMode == TrackerManager.SensorMode.SENSOR_MODE_WIFI) ||
                (isBluetooth && sensorMode == TrackerManager.SensorMode.SENSOR_MODE_BLUETOOTH_LE_IBEACON)) {
            TrackerManager.getInstance().setFingerprints(fingerprints);
        }

        return fingerprints;
    }


    /**
     * The current number of fingerprints. TODO: really? How does overlay.size() work?
     *
     * @return the number of fingerprints
     */
    public int size() {
        return overlay.size();
    }

    /**
     * Detach all registered providers TODO: So why is it a method here if it isn't needed?
     */
    public void detach() {
        Log.i(this.getClass().getName(), "Fingerprint manager does not need to implement detach, but" +
                "Multilevel must because it needs to detach at least one tile provider.");
    }


    /**
     * Create the overlay that displays the fingerprints, or rather, NOT displays any fingerprints.
     * Overwrite this method if you want your FingerprintManager to actually draw fingerprint-icons!
     *
     * @param activity the activity, used for the context that the DefaultResourceProxyImpl of the overlay needs.
     * @return the new, invisible ItemizedIconOverlay
     */
    protected ItemizedIconOverlay<FingerprintItem> createFingerprintOverlay(final Activity activity) {
        return new ItemizedIconOverlay<FingerprintItem>(new ArrayList<FingerprintItem>(),
                activity.getResources().getDrawable(R.drawable.fingerprint),
                this,
                new DefaultResourceProxyImpl(activity)) {
            @Override
            protected void onDrawItem(final Canvas canvas, final FingerprintItem item,
                                      final Point curScreenCoords, final float aMapOrientation) {
                // Don't draw... this is the base-package, and only the admin-app needs these icons.
            }
        };
    }


    /**
     * Test if we have network (otherwise we don't need to try downloading anything).
     *
     * @return true = online, false = offline
     */
    protected boolean isNetworkAvailable() {
        final ConnectivityManager manager =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = manager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }


    /**
     * Create Fingerprints from json-string.
     *
     * @param json the json that contains the serialized fingerprints
     * @return the List of Fingerprints
     */
    public static List<Fingerprint> parseJson(final String json) {
        // We need this TypeToken-thingy because it is not possible to have a Class-object for a generic type.
        // But we must tell gson what kind of object it is supposed to create. That's how we can do is:
        final Type genericFingerprintArrayListType = new TypeToken<ArrayList<Fingerprint>>() {
        }.getType();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(InvioGeoPoint.class, new InvioGeoPointDeserializer());
        List<Fingerprint> fingerprints;
        try {
            fingerprints = gsonBuilder.create().fromJson(json, genericFingerprintArrayListType);
        } catch (final JsonSyntaxException e) {
            // If the syntax in the json string is wrong, we should catch the exception and return an empty list.
            fingerprints = new ArrayList<Fingerprint>();
        }
        if (fingerprints == null) {
            fingerprints = new ArrayList<Fingerprint>();
        }
        return fingerprints;
    }

    /**
     * The normal (kunden-) FingerprintManager doesn't show any items which could be tapped.
     * But the AdminFingerprintManager does.
     * {@inheritDoc}
     */
    @Override
    public boolean onItemSingleTapUp(final int index, final FingerprintItem item) {
        return false;
    }

    /**
     * The normal (kunden-) FingerprintManager doesn't show any items which could be tapped.
     * {@inheritDoc}
     */
    @Override
    public boolean onItemLongPress(final int index, final FingerprintItem item) {
        return false;
    }

    public ItemizedIconOverlay<FingerprintItem> getOverlay() {
        return overlay;
    }

    /**
     * Checks if bluetooth is enabled, if supported.
     *
     * @return true if bluetooth is enabled
     */
    @TargetApi(18)
    public boolean isBluetoothEnabled() {
        if (isBluetoothLESupported()) {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
            final BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

            return bluetoothAdapter.isEnabled();
        }
        return false;
    }

    private boolean isBluetoothLESupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        }
        return false;
    }

    @TargetApi(18)
    private void enableBluetoothAdapter() {
        if (isBluetoothLESupported()) {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
            final BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

            if ((bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) && !bluetoothAccessWasDenied) {
                final Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, 1);
                bluetoothAccessWasDenied = true;
            }
        }
    }

}

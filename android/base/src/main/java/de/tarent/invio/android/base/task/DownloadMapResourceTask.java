package de.tarent.invio.android.base.task;

import android.app.Activity;
import android.util.Log;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.map.IndoorMap;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.exception.InvioException;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.views.MapView;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This is the AsyncTask which downloads the tileMapResource.xml from the server, in the background.
 * That xml-file is created by gdal and it contains the zoom levels and the bounding box for our map.
 */
public class DownloadMapResourceTask extends DownloadTask<Void, Void, String> {

    public static final int MIN_ZOOM_LEVEL_PLUS = 3;

    private static final String TAG = "DownloadMapResourceTask";

    // This pattern matches numbers like 50.45621323455456 and takes the 50 and 6 digits after the decimal point.
    // We then parse this as an integer because some numbers might not be valid floats. But we want E6-integers anyway.
    private static final Pattern BOUNDING_BOX_PATTERN = Pattern.compile(
            "<BoundingBox minx=\"(-?\\d+).(\\d{6})\\d+\" miny=\"(-?\\d+).(\\d{6})\\d+\"" +
                    " maxx=\"(-?\\d+).(\\d{6})\\d+\" maxy=\"(-?\\d+).(\\d{6})\\d+\"/>");

    private static final Pattern ZOOM_LEVEL_PATTERN = Pattern.compile(
            "<TileSet .*? order=\"([\\d]+)\"/>");


    private Activity activity;

    private MapView mapView;

    private String mapName;

    private int minZoomLevel = 0;

    // This default seems to be the maximum that osmdroid supports:
    private int maxZoomLevel = 22;

    private BoundingBoxE6 boundingBox;

    private IndoorMap map;

    private MapServerClient mapServerClient;

    /**
     * Construct a new DownloadMapResourceTask for an IndoorMap (i.e. a part of a multi-level-map).
     *
     * @param activity        the activity, to get the R and for UI-access
     * @param mapServerClient the client which should be used for talking to the map-server
     * @param mapName         the name of the map for which we shall download the resource-xml
     * @param map             the IndoorMap for which the resources need to be downloaded.
     *                        If this is not null then this IndoorMap will receive the data from the resources.
     * @param listener        the DownloadListener that wants to be notified when we are done here
     */
    public DownloadMapResourceTask(final Activity activity,
                                   final MapServerClient mapServerClient,
                                   final String mapName,
                                   final IndoorMap map,
                                   final DownloadListener<String> listener) {
        this.activity = activity;
        this.mapServerClient = mapServerClient;
        this.mapName = mapName;
        this.map = map;
        if (listener != null) {
            downloadListeners.add(listener);
        }
    }

    /**
     * Construct a new DownloadMapResourceTask for the map of a MapView (i.e. a single-level-map).
     *
     * @param activity        the activity, to get the R and for UI-access
     * @param mapServerClient the client which should be used for talking to the map-server
     * @param mapView         the MapView which is to be configured according to the resources that we will have
     *                        downloaded. It will be zoomed/bounded according to the resources.
     * @param mapName         the name of the map for which we shall download the resource-xml
     */
    public DownloadMapResourceTask(final Activity activity,
                                   final MapServerClient mapServerClient,
                                   final MapView mapView,
                                   final String mapName) {
        this.activity = activity;
        this.mapServerClient = mapServerClient;
        this.mapView = mapView;
        this.mapName = mapName;
    }

    @Override
    protected String doInBackground(final Void... params) {
        String resourceXml = null;
        try {
            resourceXml = getXml(mapName);
            parseBoundingBox(resourceXml);
            parseZoomLevels(resourceXml);
            publishResourceData();
        } catch (final IOException e) {
            Log.e(TAG, "Failed to download tileMapResource.xml: " + e);
            success = false;
        } catch (final InvioException e) {
            Log.e(TAG, "Failed to download tileMapResource.xml: " + e);
            success = false;
        }

        success = true;

        return resourceXml;
    }

    /**
     * Get the XML from somewhere. Here, we get it directly from the server. Overwrite this method to get it from
     * somewhere else or to have it cached (see CachedDownloadMapResourceTask).
     *
     * @param mapName the name of the map
     * @return the xml as a string
     * @throws IOException    when the download failed (e.g. server not reachable)
     * @throws InvioException when the server answered something other than OK
     */
    protected String getXml(final String mapName) throws IOException, InvioException { //NOSONAR - Can't find InvioException
        return mapServerClient.getTilemapresourceXml(mapName);
    }

    /**
     * Depending on how we were constructed we need to either update the MapView or store our data into an IndoorMap.
     */
    private void publishResourceData() {
        if (mapView != null) {
            updateMapView();
        }
        if (map != null) {
            map.setZoomLevels(minZoomLevel, maxZoomLevel);
            map.setBoundingBox(boundingBox);
        }

        TrackerManager.getInstance().setBoundingBox(boundingBox);
    }

    private void updateMapView() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mapView.setMinZoomLevel(minZoomLevel);
                mapView.setMaxZoomLevel(maxZoomLevel);
                mapView.zoomToBoundingBox(boundingBox);

                // Because the bounding box does some calculations that require positive values, we can only add a
                // bounding box when in the positive lat/lon value range. This should be done better when no longer
                // using OSMdroid.
                if (    boundingBox.getLonWestE6() > 0 &&
                        boundingBox.getLonEastE6() > 0 &&
                        boundingBox.getLatSouthE6() > 0 &&
                        boundingBox.getLatNorthE6() > 0) {
                    createScrollableAreaLimit();
                }
                mapView.invalidate();
            }
        });
    }

    private void createScrollableAreaLimit() {
        final int height = boundingBox.getLatNorthE6() - boundingBox.getLatSouthE6();
        final int width = boundingBox.getLonEastE6() - boundingBox.getLonWestE6();
        final int divider = maxZoomLevel - minZoomLevel + 1;
        final BoundingBoxE6 boundingBoxE6 = new BoundingBoxE6(
                boundingBox.getLatNorthE6() + (height/divider),
                boundingBox.getLonEastE6() - (width),
                boundingBox.getLatSouthE6() - (height/divider),
                boundingBox.getLonWestE6() + (width)
        );

        mapView.setScrollableAreaLimit(boundingBoxE6);
    }

    private void parseZoomLevels(final String resourceXml) throws InvioException {
        final Matcher m = ZOOM_LEVEL_PATTERN.matcher(resourceXml);
        // The pattern will be found once for each zoomlevel, in order. So the first one will be the minimum and the
        // last one will be the maximum.
        if (m.find()) {
            minZoomLevel = Integer.parseInt(m.group(1)) + MIN_ZOOM_LEVEL_PLUS;
            maxZoomLevel = minZoomLevel;
            while (m.find()) {
                maxZoomLevel = Integer.parseInt(m.group(1));
            }
        } else {
            throw new InvioException("Can't find zoom levels in tileMapResource.xml!");
        }
    }

    // TODO: BoundingBox is rotated by 90 degress wrong and the usage of regex is hard to follow
    // Should be changed when the tileresource.xml files are adjusted correctly
    // because those files are defined wrong to fix this bug here
    private void parseBoundingBox(final String resourceXml) throws InvioException {
        final int south;
        final int west;
        final int north;
        final int east;

        // TODO: find out why the order in the xml doesn't match the names/values as we expect them.
        final Matcher matcher = BOUNDING_BOX_PATTERN.matcher(resourceXml);
        if (matcher.find()) {
            // We take the parts from before and after the decimal-point and parse the combined strings:
            south = Integer.parseInt(matcher.group(1) + matcher.group(2));
            west = Integer.parseInt(matcher.group(3) + matcher.group(4));
            north = Integer.parseInt(matcher.group(5) + matcher.group(6));
            east = Integer.parseInt(matcher.group(7) + matcher.group(8));
        } else {
            throw new InvioException("Can't find boundingbox in tileMapResource.xml!");
        }

        boundingBox = new BoundingBoxE6(north, east, south, west);
    }

}

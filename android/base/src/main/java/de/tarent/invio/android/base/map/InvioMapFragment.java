package de.tarent.invio.android.base.map;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.UrlTileProvider;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.entities.InvioGeoPoint;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implementation for the map fragments. Holds all of the basic information that can be used in any map fragment
 * anywhere.
 */
public class InvioMapFragment extends SupportMapFragment implements GoogleMap.OnCameraChangeListener {

    /**
     * How often the user's position gets updated per second.
     */
    public static final int UPDATES_PER_SECOND = 10;

    private String tilesURL = null;
    private int tileSize = 256;
    private int minZoomLevel = 0;
    private int maxZoomLevel = Integer.MAX_VALUE;
    private LatLngBounds boundingBox = null;
    private GoogleMap map;
    private boolean cameraInitialized = false;
    private Marker userPositionMarker;
    private LatLng userPosition = new LatLng(0, 0);
    private Timer userPositionUpdateTimer;
    private CameraPosition lastValidCameraPosition;
    private boolean focusOnUserPosition = false;
    private String mapName = "";
    private List<Polygon> polygons = new ArrayList<Polygon>();
    private List<Polyline> polylines = new ArrayList<Polyline>();
    private List<TextMarker> textMarkers = new ArrayList<TextMarker>();

    /**
     * Default constructor.
     */
    public InvioMapFragment() {

    }

    /**
     * Converts a given {@link InvioGeoPoint} into a google maps usable {@link LatLng}.
     *
     * @param geoPoint the {@link InvioGeoPoint}
     * @return the converted {@link LatLng}
     */
    public static LatLng invioGeoPointToLatLng(final InvioGeoPoint geoPoint) {
        return new LatLng(geoPoint.getLatitudeE6() / 1E6, geoPoint.getLongitudeE6() / 1E6);
    }

    /**
     * Converts a given {@link LatLng} into an {@link InvioGeoPoint}.
     *
     * @param latLng the {@link LatLng}
     * @return the convertedt {@link InvioGeoPoint}
     */
    public static InvioGeoPoint latLngToInvioGeoPoint(final LatLng latLng) {
        return new InvioGeoPoint(latLng.latitude, latLng.longitude);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        final View mapView = super.onCreateView(inflater, container, savedInstanceState);
        cameraInitialized = false;

        map = getMap();

        configureMap(savedInstanceState);
        createUserMarker();

        final TouchableWrapperFrameLayout frameLayout = new TouchableWrapperFrameLayout(getActivity());
        frameLayout.addView(mapView, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));

        final FrameLayout uiElements = (FrameLayout) inflater.inflate(R.layout.map_ui_elements, frameLayout);
        final ImageButton focusButton = (ImageButton) uiElements.findViewById(R.id.focusButton);
        focusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setFocusOnUserPosition(!isFocusOnUserPosition());
            }
        });

        return frameLayout;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        outState.putParcelable("boundingBox", boundingBox);
        outState.putString("tilesURL", tilesURL);
        outState.putInt("minZoomLevel", minZoomLevel);
        outState.putInt("maxZoomLevel", maxZoomLevel);
        outState.putString("mapName", mapName);

        Log.d("InvioMapFragment", "onSaveInstanceState");

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        map.clear();
        clearPolylines();
        clearPolygons();
        clearTextMarkers();
    }

    @Override
    public void onResume() {
        super.onResume();
        startUpdatingUserPosition();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopUpdatingUserPosition();
    }

    private void createUserMarker() {
        userPositionMarker = map.addMarker(new MarkerOptions().position(new LatLng(0, 0))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.user_position_icon)));
    }

    /**
     * Triggers when the map is touched.
     */
    public void onMapTouched() {
        setFocusOnUserPosition(false);
    }

    /**
     * Triggers when the map is clicked at the given {@link LatLng} coordinate.
     *
     * @param coordinate the {@link LatLng} coordinate
     */
    public void onMapClicked(final LatLng coordinate) {
        for (final Polygon polygon : polygons) {
            if (isPointInPolygon(coordinate, polygon)) {
                onPolygonClick(polygon);
            }
        }
    }

    /**
     * Triggers when a {@link Polygon} on the map is clicked.
     *
     * @param polygon the {@link Polygon}
     */
    public void onPolygonClick(final Polygon polygon) {

    }

    /**
     * Start updating the user position based on the set {@link #UPDATES_PER_SECOND}.
     */
    public void startUpdatingUserPosition() {
        userPositionUpdateTimer = new Timer(true);
        userPositionUpdateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                update();
            }
        }, 0, 1000 / UPDATES_PER_SECOND); // 1000ms/updates per second
    }

    /**
     * Stop updating the user position by cancelling, purging and setting the {@link #userPositionUpdateTimer}.
     */
    public void stopUpdatingUserPosition() {
        userPositionUpdateTimer.cancel();
        userPositionUpdateTimer.purge();
        userPositionUpdateTimer = null;
    }

    private void update() {
        if (TrackerManager.getInstance().isTracking()) {
            final InvioGeoPoint geoPoint = TrackerManager.getInstance().getCurrentPosition();
            final LatLng newUserPosition = new LatLng(
                    geoPoint.getLatitudeE6() / 1E6,
                    geoPoint.getLongitudeE6() / 1E6);

            if (userPosition.latitude != newUserPosition.latitude
                    || userPosition.longitude != newUserPosition.longitude) {
                userPosition = newUserPosition;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(boundingBox.contains(userPosition)) {
                            userPositionMarker.setPosition(userPosition);

                            if (focusOnUserPosition) {
                                map.animateCamera(CameraUpdateFactory.newLatLng(userPosition));
                            }
                        }
                    }
                });
            }
        }
    }

    private void configureMap(final Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            boundingBox = savedInstanceState.getParcelable("boundingBox");
            tilesURL = savedInstanceState.getString("tilesURL");
            minZoomLevel = savedInstanceState.getInt("minZoomLevel");
            maxZoomLevel = savedInstanceState.getInt("maxZoomLevel");
            mapName = savedInstanceState.getString("mapName");
        }

        map.getUiSettings().setCompassEnabled(false);
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setIndoorLevelPickerEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setMapType(GoogleMap.MAP_TYPE_NONE);
        map.addTileOverlay(new TileOverlayOptions().fadeIn(true).tileProvider(new UrlTileProvider(tileSize, tileSize) {
            @Override
            public URL getTileUrl(final int x, final int y, final int zoom) {
                try {
                    if (tilesURL != null) {
                        final String url = String.format(tilesURL, zoom, x, (1 << zoom) - y - 1);
                        return new URL(url);
                    } else {
                        return null;
                    }
                } catch (final MalformedURLException e) {
                    return null;
                }
            }
        }).zIndex(1));

        map.setOnCameraChangeListener(this);
    }

    /**
     * Navigate to the given {@link LatLng} position.
     *
     * @param position the {@link LatLng}
     */
    public void navigateToPosition(final LatLng position) {
        final InvioGeoPoint currentPosition = latLngToInvioGeoPoint(userPosition);
        if (boundingBox.contains(userPosition)) {
            final InvioGeoPoint[] points = TrackerManager.getInstance().getNavigation(
                    currentPosition, latLngToInvioGeoPoint(position));

            final LatLng[] latLngs = new LatLng[points.length];

            for (int i = 0; i < points.length; i++) {
                latLngs[i] = InvioMapFragment.invioGeoPointToLatLng(points[i]);
            }

            clearPolylines();
            addPolyline(latLngs);
        } else {
            Log.e("InvioMapFragment", "No navigation from outside the map possible");
        }
    }

    /**
     * Adds a {@link TextMarker} with the given text to the map.
     *
     * @param text the marker text
     * @return the new {@link TextMarker}
     */
    public TextMarker addTextMarker(final String text) {
        if (text != null) {
            final TextMarker textMarker = new TextMarker(getMap(), text);
            textMarkers.add(textMarker);
            return textMarker;
        }
        return null;
    }

    /**
     * Removes the given {@link TextMarker} from the map.
     *
     * @param textMarker the {@link TextMarker}
     */
    public void removeTextMarker(final TextMarker textMarker) {
        if (textMarker != null) {
            textMarkers.remove(textMarker);
            textMarker.getMarker().remove();
        }
    }

    /**
     * Removes all of the {@link TextMarker}s from the map.
     */
    public void clearTextMarkers() {
        for (final TextMarker marker : textMarkers) {
            marker.getMarker().remove();
        }

        textMarkers.clear();
    }

    /**
     * Adds a {@link Polyline} to the map with the given {@link LatLng} points.
     *
     * @param points the {@link LatLng} points
     * @return the {@link Polyline}
     */
    public Polyline addPolyline(final LatLng... points) {
        if (points != null && map != null) {
            final int color = getActivity().getResources().getColor(R.color.tarent_rot);
            final Polyline polyline =
                    map.addPolyline(new PolylineOptions().add(points).zIndex(3).color(color).width(10));
            polylines.add(polyline);
            return polyline;
        }

        return null;
    }

    /**
     * Adds a {@link Polygon} to the map with the given {@link LatLng} points.
     *
     * @param polygon the {@link LatLng} points
     * @return the {@link Polygon}
     */
    public Polygon addPolygon(final LatLng... polygon) {
        if (polygon != null && map != null) {
            final int color = getActivity().getResources().getColor(R.color.tarent_hellrot);
            final Polygon p = map.addPolygon(new PolygonOptions().add(polygon)
                    .zIndex(2).fillColor(
                            Color.argb(128,
                                    Color.red(color),
                                    Color.green(color),
                                    Color.blue(color))).strokeWidth(2).strokeColor(color));

            polygons.add(p);
            return p;
        }

        return null;
    }

    /**
     * Removes all of the {@link Polyline}s from the map.
     */
    public void clearPolylines() {
        for (final Polyline polyline : polylines) {
            polyline.remove();
        }
    }

    /**
     * Removes all of the {@link Polygon}s from the map.
     */
    public void clearPolygons() {
        for (final Polygon polygon : polygons) {
            polygon.remove();
        }
    }

    private void zoomToFitBoundingBox(final boolean animate) {
        if (boundingBox != null) {
            final CameraUpdate update = CameraUpdateFactory.newLatLngBounds(boundingBox, 0);
            if (animate) {
                map.animateCamera(update);
            } else {
                map.moveCamera(update);
            }
        }
    }

    public String getTilesURL() {
        return tilesURL;
    }

    public void setTilesURL(final String tilesURL) {
        this.tilesURL = tilesURL;
    }

    public int getTileSize() {
        return tileSize;
    }

    public void setTileSize(final int tileSize) {
        this.tileSize = tileSize;
    }

    public LatLngBounds getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(final LatLngBounds boundingBox) {
        this.boundingBox = boundingBox;
    }

    @Override
    public void onCameraChange(final CameraPosition cameraPosition) {
        if (!cameraInitialized) {
            zoomToFitBoundingBox(false);
            cameraInitialized = true;
        } else {
            if (cameraPosition.zoom > getMaxZoomLevel() - 1) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(cameraPosition.target, 17));
            } else if (cameraPosition.zoom < getMinZoomLevel() + 1) {
                zoomToFitBoundingBox(true);
            }

            if (!boundingBox.contains(cameraPosition.target)) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        lastValidCameraPosition.target, lastValidCameraPosition.zoom));
            } else {
                lastValidCameraPosition = map.getCameraPosition();
            }
        }
    }

    public int getMinZoomLevel() {
        return minZoomLevel;
    }

    public void setMinZoomLevel(final int minZoomLevel) {
        this.minZoomLevel = minZoomLevel;
    }

    public int getMaxZoomLevel() {
        return maxZoomLevel;
    }

    public void setMaxZoomLevel(final int maxZoomLevel) {
        this.maxZoomLevel = maxZoomLevel;
    }

    public boolean isFocusOnUserPosition() {
        return focusOnUserPosition;
    }

    /**
     * Centers the map around the current user position, also re-initializes the user position
     *
     * @param focusOnUserPosition true if you want to center to the current user position, otherwise false
     */
    public void setFocusOnUserPosition(final boolean focusOnUserPosition) {
        if(focusOnUserPosition != this.focusOnUserPosition){
            this.focusOnUserPosition = focusOnUserPosition;

            TrackerManager.getInstance().stopTracking();
            TrackerManager.getInstance().startTracking();
        }
    }

    private boolean isPointInPolygon(final LatLng point, final Polygon polygon) {
        final List<LatLng> vertices = polygon.getPoints();
        int intersectCount = 0;
        for (int j = 0; j < vertices.size() - 1; j++) {
            if (rayCastIntersect(point, vertices.get(j), vertices.get(j + 1))) {
                intersectCount++;
            }
        }

        return (intersectCount % 2) != 0;
    }

    private boolean rayCastIntersect(final LatLng point, final LatLng vertA, final LatLng vertB) {

        final double aY = vertA.latitude;
        final double bY = vertB.latitude;
        final double aX = vertA.longitude;
        final double bX = vertB.longitude;
        final double pY = point.latitude;
        final double pX = point.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY) || (aX < pX && bX < pX)) {
            return false;
        }

        final double m = (aY - bY) / (aX - bX);
        final double bee = (-aX) * m + aY;
        final double x = (pY - bee) / m;

        return x > pX;
    }

    public LatLng getUserPosition() {
        return userPosition;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(final String mapName) {
        this.mapName = mapName;
    }

    /**
     * Adds a {@link FrameLayout} wrapper that handles touch events on the map.
     */
    public class TouchableWrapperFrameLayout extends FrameLayout {

        /**
         * Constructor.
         *
         * @param context the {@link Context}
         */
        public TouchableWrapperFrameLayout(final Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                onMapTouched();
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                final LatLng coordinate = map.getProjection()
                        .fromScreenLocation(new Point((int) motionEvent.getX(), (int) motionEvent.getY()));
                onMapClicked(coordinate);
            }

            return super.dispatchTouchEvent(motionEvent);
        }
    }
}
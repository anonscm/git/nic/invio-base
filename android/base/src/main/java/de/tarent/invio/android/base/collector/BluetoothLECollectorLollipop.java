package de.tarent.invio.android.base.collector;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import de.tarent.invio.android.base.histogram.HistogramBuilder;
import de.tarent.invio.android.base.utils.InvioScanResult;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * a BluetoothLECollector based on the Android 5.x (Lollipop) API
 */
@TargetApi(21)
public class BluetoothLECollectorLollipop extends BluetoothLECollector {

    private static final String TAG = "BTLE_L";

    private static final int SCAN_INTERVAL_MS = 250;

    private ScanSettings scanSettings;
    private BluetoothLeScanner scanner;
    private Handler scanHandler = new Handler(Looper.getMainLooper());

    private Runnable scanRunnable = new Runnable() {
        @Override
        public void run() {

            Log.i(TAG, "scanRunnable tick " + secondsCounter);
            scanner.flushPendingScanResults(scanCallback);
            scanner.stopScan(scanCallback);
            scanner.startScan(scanFilters, scanSettings, scanCallback);

            if (collectorListener != null) {
                collectorListener.onCollectionIteration((int)secondsCounter, maxSeconds);
            }

            secondsCounter += secondsIncrement;
            if (maxSeconds != -1 && secondsCounter >= maxSeconds) {
                stopSensors();
            } else {
                scanHandler.postDelayed(this, SCAN_INTERVAL_MS);
            }

        }
    };

    private ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            final byte[] bytes = result.getScanRecord().getBytes();
            final IBeacon beacon = IBeacon.fromAdvertisementData(bytes);
            if (beacon != null) {

                // We are only interested in our own beacons:
                if (proximityUUID != null && !beacon.getProximityUUID().equals(proximityUUID)) {
                    return;
                }

                final int rssi = result.getRssi();

                final String bssid = beacon.getProximityUUID() + "_" + beacon.getMajor() + "_" + beacon.getMinor();
                Log.i(TAG, "saw bssid=" + bssid);

                final Integer i = counts.get(bssid);
                counts.put(bssid, i == null ? 1 : i + 1);

                final List<InvioScanResult> btleInvioScanResults = new ArrayList<InvioScanResult>();
                btleInvioScanResults.add(new InvioScanResult(bssid, rssi));
                histogramBuilder.addScanResults(btleInvioScanResults);

                lastHistogram = histogramBuilder.build();
                SensorDataLogHelper.logHistogram(lastHistogram, SensorDataLogHelper.HistogramLogType.BluetoothLE);
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);

            Log.e(TAG, "scan error callback iteration=" + secondsCounter + " error=" + errorCode);
        }
    };

    /**
     * Construct a new BluetoothLECollectorLollipop
     *
     * @param collectionMode the collection mode to use
     */
    public BluetoothLECollectorLollipop(CollectionMode collectionMode) {
        super(collectionMode);

        final ScanSettings.Builder scanSettingsBuilder = new ScanSettings.Builder();
        scanSettingsBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY);
        scanSettings = scanSettingsBuilder.build();

        scanner = BluetoothAdapter.getDefaultAdapter().getBluetoothLeScanner();
    }

    @Override
    public void startSensors(final CollectorListener collectorListener, final int scanSeconds) {

        histogramBuilder = new HistogramBuilder("Bluetooth LE Histogram " + new Date().toString(), maxAge);

        this.secondsIncrement = SCAN_INTERVAL_MS / 1000.0;
        this.maxSeconds = scanSeconds;
        this.secondsCounter = 0;
        this.lastHistogram = null;
        this.collectorListener = collectorListener;
        if (collectorListener != null) {
            collectorListener.onCollectionStart();
        }

        scanner.startScan(scanFilters, scanSettings, scanCallback);
        scanHandler.post(scanRunnable);
    }

    @Override
    public void stopSensors() {

        scanHandler.removeCallbacks(scanRunnable);
        scanner.stopScan(scanCallback);

        if (collectorListener != null) {
            collectorListener.onCollectionStop();
        }

        for (final String bssid : counts.keySet()) {
            final int seen = counts.get(bssid);
            final int interval = (this.maxSeconds * 1000) / seen;
            Log.i(TAG, bssid + " seen " + counts.get(bssid) + ", interval=" + interval);
        }
    }
}

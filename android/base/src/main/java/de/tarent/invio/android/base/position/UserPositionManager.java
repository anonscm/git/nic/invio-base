package de.tarent.invio.android.base.position;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import de.tarent.invio.android.base.App;
import de.tarent.invio.android.base.R;
import de.tarent.invio.android.base.TrackerManager;
import de.tarent.invio.android.base.map.InvioMapView;
import de.tarent.invio.android.base.utils.SensorDataLogHelper;
import de.tarent.invio.entities.InvioGeoPoint;
import de.tarent.invio.tracker.Particle;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Overlay;

import java.util.Timer;
import java.util.TimerTask;

import static de.tarent.invio.android.base.map.MapParams.isAdmin;
import static de.tarent.invio.android.base.map.MapParams.showingUserPosition;

/**
 * The UserPositionManager displays the user position.
 */
public class UserPositionManager {
    public static final int UPDATES_PER_SECOND = 10;
    /**
     * The overlay displays the users position and some additional diagnostic/debug info.
     */
    protected Overlay overlay;

    private Activity activity;

    private MapView mapView;

    private InvioGeoPoint userPosition = new InvioGeoPoint();
    private GeoPoint osmUserPosition = new GeoPoint(0, 0);
    private GeoPoint reuseOsmGeoPoint = new GeoPoint(0, 0);

    private Timer userPositionUpdateTimer;

    private Point reusePoint = new Point();
    private Paint reusePaint = new Paint();
    private Paint particlePaint = new Paint();

    private boolean drawParticles = false;

    /**
     * Constructs a new UserPositionManager.
     *
     * @param activity the parent {@link android.app.Activity}
     * @param mapView  the {@link org.osmdroid.views.MapView}
     */
    public UserPositionManager(final Activity activity, final MapView mapView) {
        this.activity = activity;
        this.mapView = mapView;
        overlay = new UserPositionOverlay(activity);
        mapView.getOverlays().add(overlay);

        if (App.getContext().getResources().getBoolean(R.bool.tracking_mode)) {
            startUpdating(); //NOSONAR - Overridable method is allowed to be called here.
        }

        particlePaint.setAntiAlias(true);
        particlePaint.setColor(Color.RED);
    }

    /**
     * Starts polling the TrackerManager for the current position and updates
     * the screen with the user position icon.
     */
    public void startUpdating() {
        userPositionUpdateTimer = new Timer(true);
        userPositionUpdateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                update();
            }
        }, 0, 1000 / UPDATES_PER_SECOND); // 1000ms/updates per second
    }

    /**
     * Stop updating the position
     */
    public void stopUpdating() {
        userPositionUpdateTimer.cancel();
        userPositionUpdateTimer.purge();
        userPositionUpdateTimer = null;
    }

    private void update() {
        if (TrackerManager.getInstance().isTracking()) {
            final InvioGeoPoint geoPoint = TrackerManager.getInstance().getCurrentPosition();
            if (userPosition.getLatitudeE6() != geoPoint.getLatitudeE6()
                    || userPosition.getLongitudeE6() != geoPoint.getLongitudeE6()) {
                userPosition = geoPoint;
                osmUserPosition.setLatitudeE6(userPosition.getLatitudeE6());
                osmUserPosition.setLongitudeE6(userPosition.getLongitudeE6());
                SensorDataLogHelper.logPosition(userPosition.getLatitudeE6(), userPosition.getLongitudeE6());
                focusOnUserPosition();
                mapView.postInvalidate();
            }
        }
    }

    private void focusOnUserPosition() {
        if (mapView instanceof InvioMapView && ((InvioMapView) mapView).isFocusing() && (showingUserPosition || isAdmin)) { // NOSONAR
            mapView.getController().setCenter(osmUserPosition);
            if (((InvioMapView) mapView).isMapSwitch()) {
                final BoundingBoxE6 boundingBox = new BoundingBoxE6(
                        osmUserPosition.getLatitudeE6(), osmUserPosition.getLongitudeE6(),
                        osmUserPosition.getLatitudeE6(), osmUserPosition.getLongitudeE6());

                mapView.post(new Runnable() {
                    @Override
                    public void run() {
                        mapView.zoomToBoundingBox(boundingBox);
                    }
                });

                ((InvioMapView) mapView).setMapSwitch(false);
            }
        }
    }

    /**
     * Set to true if you want the particles of the ParticleFilter to be drawn.
     *
     * @param showParticles true for particles to be shown, otherwise false
     */
    public void setDrawParticles(final boolean showParticles) {
        this.drawParticles = showParticles;
    }

    /**
     * The {@link Overlay} for the user position. It will be used to draw the user position and particles.
     */
    private final class UserPositionOverlay extends Overlay {

        final Bitmap userPositionBitmap;

        public UserPositionOverlay(final Context ctx) {
            super(ctx);

            userPositionBitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.user_position_icon);
        }

        @Override
        protected void draw(final Canvas canvas, final MapView mapView, final boolean b) {
            if (TrackerManager.getInstance().isTracking()) {
                final Projection proj = mapView.getProjection();
                proj.toPixels(osmUserPosition, reusePoint);


                if ((showingUserPosition || isAdmin)) {
                    canvas.drawBitmap(userPositionBitmap,
                            reusePoint.x - userPositionBitmap.getWidth() / 2,
                            reusePoint.y
                                    - (int) ((float) userPositionBitmap.getHeight() * 0.75f), reusePaint);
                }

                if (drawParticles) {
                    drawParticles(canvas, proj);
                    mapView.postInvalidate();
                }
            }
        }

        private void drawParticles(final Canvas canvas, final Projection proj) {
            final Particle[] particles = TrackerManager.getInstance().particles();
            for (int i = 0; i < particles.length; i++) {
                final Particle p = particles[i];
                reuseOsmGeoPoint.setLatitudeE6((int)(p.latitude * 1E6));
                reuseOsmGeoPoint.setLongitudeE6((int)(p.longitude * 1E6));
                proj.toPixels(reuseOsmGeoPoint, reusePoint);

                canvas.drawCircle(reusePoint.x, reusePoint.y, 3.0f, particlePaint);
            }
        }
    }
}

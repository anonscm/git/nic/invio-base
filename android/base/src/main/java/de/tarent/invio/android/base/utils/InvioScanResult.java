package de.tarent.invio.android.base.utils;

/**
 * The ScanResult stores the access point bssid/signal strength signalStrength pair.
 */
public class InvioScanResult {

    private String bssid;
    private int signalStrength;

    /**
     * Construct a new ScanResult.
     *
     * @param bssid the BSSID of the access point
     * @param signalStrength the measured signal strength
     */
    public InvioScanResult(final String bssid, final int signalStrength) {
        this.bssid = bssid;
        this.signalStrength = signalStrength;
    }

    public String getBssid() {
        return bssid;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

}

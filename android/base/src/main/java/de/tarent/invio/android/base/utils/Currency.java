package de.tarent.invio.android.base.utils;

import de.tarent.invio.android.base.wrapper.LocaleWrapper;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Currency class that holds all currency related utilities (eg. choosing the correct currency form based on device
 * language).
 *
 * @author Désirée Amling <d.amling@tarent.de>
 */
public class Currency {

    private static final String EURO = "€";
    private static final String POUND = "£";

    /**
     * The {@link LocaleWrapper} for the {@link Locale} class.
     */
    protected LocaleWrapper localeWrapper = new LocaleWrapper();

    private float price;

    /**
     * Constructor.
     *
     * @param price the price
     */
    public Currency(final float price) {
        setPrice(price);
    }

    /**
     * Formats the {@link #price} into the correct format for the device's {@link Locale}.
     *
     * @return the formatted currency string
     */
    public String getFormattedCurrencyForDevice() {
        final Locale locale = localeWrapper.getDefaultLocale();

        return getFormattedCurrencyForDevice(locale);
    }

    private String getFormattedCurrencyForDevice(final Locale deviceLocale) {
        if (deviceLocale.equals(Locale.GERMANY)) {
            return formattedGermanCurrency();
        } else if (deviceLocale.equals(Locale.ITALY)) {
            return formattedItalianCurrency();
        } else if (deviceLocale.equals(Locale.UK)) {
            return formattedUKCurrency();
        } else {
            return formattedUKCurrency();
        }
    }

    private String formattedGermanCurrency() {
        final NumberFormat numberFormat = getNumberFormat(Locale.GERMANY);

        return numberFormat.format(price) + " " + EURO;
    }

    private String formattedItalianCurrency() {
        final NumberFormat numberFormat = getNumberFormat(Locale.ITALY);

        return numberFormat.format(price) + " " + EURO;
    }

    private String formattedUKCurrency() {
        final NumberFormat numberFormat = getNumberFormat(Locale.UK);

        return numberFormat.format(price) + " " + POUND;
    }

    private NumberFormat getNumberFormat(final Locale locale) {
        final NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(2);

        return numberFormat;
    }

    /**
     * Sets the price after dividing the given price by 100f.
     *
     * @param price the price
     */
    public final void setPrice(final float price) {
        this.price = (price / 100f);
    }
}

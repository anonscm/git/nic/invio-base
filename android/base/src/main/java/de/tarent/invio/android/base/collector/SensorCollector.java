package de.tarent.invio.android.base.collector;

/**
 * A SensorCollector collects data from a sensor. It needs to be started and stopped, so that it can [un-]register
 * any listeners that it needs.
 */
public interface SensorCollector {

    /**
     * The value for infinite scan counts.
     */
    int SCAN_COUNT_INFINITE = -1;

    /**
     * Start the sensors.
     *
     * @param captureListener gets called on new sensor data, can be null.
     * @param scanCount       specifies the maximum scan iterations, -1 for infinite.
     */
    void startSensors(final CollectorListener captureListener, final int scanCount);

    /**
     * Stop the sensors and clean up any listeners, timers, etc.
     */
    void stopSensors();
}
package de.tarent.invio.android.base.fingerprint;

import de.tarent.invio.android.base.collector.IBeacon;
import org.junit.Assert;
import org.junit.Test;

public class IBeaconTest {
    @Test
    public void testIBeacon() {
        final byte[] iBeaconData = {0x02, 0x01, 0x06, //flags
                0x1A, (byte) 0xff, 0x4c, 0x00, 0x02, 0x15, // manufacturer data (2 bytes), apple identifier (2 bytes), iBeacon identifier (2 bytes)
                (byte) 0x10, (byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14, (byte) 0x15, (byte) 0x16, (byte) 0x17, // uuid
                (byte) 0x20, (byte) 0x21, (byte) 0x22, (byte) 0x23, (byte) 0x24, (byte) 0x25, (byte) 0x26, (byte) 0x27, // uuid
                0x00, 0x10, 0x00, (byte) 0xff, //major, minor
                (byte) 0x16}; //txPower

        final IBeacon beacon = IBeacon.fromAdvertisementData(iBeaconData);

        Assert.assertEquals(beacon.getProximityUUID().toString(), "10111213-1415-1617-2021-222324252627");
        Assert.assertEquals(beacon.getMajor(), 16);
        Assert.assertEquals(beacon.getMinor(), 255);
        Assert.assertEquals(beacon.getTxPower(), 22);
    }

    @Test
    public void testNoIBeacon() {
        final byte[] iBeaconData = {0x02, 0x01, 0x06, //flags
                0x1A, (byte) 0xff, 0x44, 0x11, 0x22, 0x11, // manufacturer data (2 bytes), unknown identifier (2 bytes), some bytes that are not an ibeacon idenfier (2 bytes)
                (byte) 0x10, (byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14, (byte) 0x15, (byte) 0x16, (byte) 0x17, // uuid
                (byte) 0x20, (byte) 0x21, (byte) 0x22, (byte) 0x23, (byte) 0x24, (byte) 0x25, (byte) 0x26, (byte) 0x27, // uuid
                0x00, 0x10, 0x00, (byte) 0xff, //major, minor
                (byte) 0x16}; //txPower

        final IBeacon beacon = IBeacon.fromAdvertisementData(iBeaconData);
        Assert.assertNull(beacon);

        final byte[] iBeaconData2 = {0x00};

        final IBeacon beacon2 = IBeacon.fromAdvertisementData(iBeaconData2);
        Assert.assertNull(beacon2);

        final byte[] iBeaconData3 = {};

        final IBeacon beacon3 = IBeacon.fromAdvertisementData(iBeaconData3);
        Assert.assertNull(beacon3);
    }

    @Test
    public void testInvalidIBeacon() {
        final byte[] iBeaconData = {0x02, 0x01, 0x06, //flags
                0x1A, (byte) 0xff, 0x4c, 0x00, 0x02, 0x15, // manufacturer data (2 bytes), apple identifier (2 bytes), iBeacon identifier (2 bytes)
                (byte) 0x10, (byte) 0x11, (byte) 0x12, (byte) 0x13, (byte) 0x14, (byte) 0x15, (byte) 0x16, (byte) 0x17}; // broken uuid

        final IBeacon beacon = IBeacon.fromAdvertisementData(iBeaconData);
        Assert.assertNull(beacon);

    }
}

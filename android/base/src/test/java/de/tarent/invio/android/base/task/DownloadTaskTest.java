package de.tarent.invio.android.base.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 18)
public class DownloadTaskTest {

    @Test
    public void testAddRemoveListeners() throws Throwable {
        final TestableDownloadTask task = new TestableDownloadTask();

        assertEquals(0, task.getListeners().size());

        final DownloadListener listener1 = mock(DownloadListener.class);
        final DownloadListener listener2 = mock(DownloadListener.class);

        task.addDownloadListener(listener1);
        assertEquals(listener1, task.getListeners().get(0));

        task.addDownloadListener(listener2);
        assertEquals(2, task.getListeners().size());

        task.removeDownloadListener(listener1);
        assertEquals(listener2, task.getListeners().get(0));
        assertEquals(1, task.getListeners().size());

        task.removeDownloadListener(listener1); // Was already removed!
        assertEquals(1, task.getListeners().size());

        task.removeDownloadListener(listener2);
        assertEquals(0, task.getListeners().size());
    }

    @Test
    public void testCallingOfListeners() {
        final TestableDownloadTask task = new TestableDownloadTask();
        final DownloadListener listener1 = mock(DownloadListener.class);
        final DownloadListener listener2 = mock(DownloadListener.class);
        task.addDownloadListener(listener1);
        task.addDownloadListener(listener2);

        final String result = "Win!";
        task.setSuccess(true);
        task.onPostExecute(result);
        verify(listener1).onDownloadFinished(task, true, result);
        verify(listener2).onDownloadFinished(task, true, result);

        task.setSuccess(false);
        task.onPostExecute(null);
        verify(listener1).onDownloadFinished(task, false, null);
        verify(listener2).onDownloadFinished(task, false, null);


    }

    public class TestableDownloadTask extends DownloadTask {
        @Override
        protected Object doInBackground(final Object... params) {
            return null;
        }

        public List getListeners() {
            return downloadListeners;
        }

        public void setSuccess(final boolean success) {
            this.success = success;
        }
    }
}

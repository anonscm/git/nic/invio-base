package de.tarent.invio.android.base.task;


import android.app.ProgressDialog;
import android.widget.ArrayAdapter;
import de.tarent.invio.mapserver.MapServerClient;
import de.tarent.invio.mapserver.response.MapListResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 18)
public class GetMapListTaskTest {

    @Before
    public void setup() throws Exception {
        Robolectric.getBackgroundScheduler().pause();
    }

    @Test
    public void testThatTwoMapsListResponse() throws Exception {
        final MapServerClient client = mock(MapServerClient.class);

        final List<String> mapList = new ArrayList<String>();
        mapList.add("bla_tarent 1OG");
        mapList.add("bla_tarent 2OG");
        mapList.add("blub_kunde 1OG");
        mapList.add("blub_kunde 2OG");

        final MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        final ProgressDialog progressDialog = mock(ProgressDialog.class);

        final ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        final GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter, "bla_");
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(2, task.filteredList.size());
    }

    @Test
    public void testFilterEmptyList() throws Exception {
        final MapServerClient client = mock(MapServerClient.class);

        final List<String> mapList = new ArrayList<String>();
        mapList.add("bla_tarent 1OG");
        mapList.add("bla_tarent 2OG");

        final String prefix = "blub_";

        final MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        final ProgressDialog progressDialog = mock(ProgressDialog.class);

        final ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        final GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter, prefix);
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(0, task.filteredList.size());
    }

    @Test
    public void testShowAllMapsPrefix() throws Exception {
        final MapServerClient client = mock(MapServerClient.class);

        final List<String> mapList = new ArrayList<String>();
        mapList.add("bla_tarent 1OG");
        mapList.add("bla_tarent 2OG");
        mapList.add("blub_kunde 1OG");
        mapList.add("blub_kunde 2OG");

        final String prefix = "tarentdebug";

        final MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        final ProgressDialog progressDialog = mock(ProgressDialog.class);

        final ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        final GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter, prefix);
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(4, task.filteredList.size());
    }

    @Test
    public void testEmptyMapResponse() throws Exception {
        final MapServerClient client = mock(MapServerClient.class);

        final List<String> mapList = new ArrayList<String>();

        final MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);
        when(client.getMapList(5000)).thenReturn(response);

        final ProgressDialog progressDialog = mock(ProgressDialog.class);

        final ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        final GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter, "bla");
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(0, task.mapList.size());
    }

    @Test
    public void testExceptionHandling() throws Exception {
        final MapServerClient client = mock(MapServerClient.class);

        final List<String> mapList = new ArrayList<String>();
        mapList.add("tarent 1OG");
        mapList.add("tarent 2OG");

        final MapListResponse response = mock(MapListResponse.class);
        when(response.getMapList()).thenReturn(mapList);

        when(client.getMapList(5000)).thenThrow(IOException.class);

        final ProgressDialog progressDialog = mock(ProgressDialog.class);

        final ArrayAdapter arrayAdapter = mock(ArrayAdapter.class);

        final GetMapListTask task = new GetMapListTask(client, progressDialog, arrayAdapter, "bla");
        task.execute();

        Robolectric.runBackgroundTasks();

        assertEquals(0, task.mapList.size());
    }
}

package de.tarent.invio.android.base.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 18)
public class DeviceInformationTest {

    private DeviceInformation deviceInformation;

    private String testString = "test";

    private WifiInfo wifiInfo;

    @Before
    public void setup() {
        final Context context = mock(Context.class);
        final WifiManager wifiManager = mock(WifiManager.class);
        wifiInfo = mock(WifiInfo.class);

        when(context.getSystemService(Context.WIFI_SERVICE)).thenReturn(wifiManager);
        when(wifiManager.getConnectionInfo()).thenReturn(wifiInfo);

        deviceInformation = new DeviceInformation(context);
    }

    @Test
    public void testGetCombinedDeviceIdAsSHA1StringWithNullValues() {
        testString = null;

        final DeviceInformation deviceInformationSpy = spy(deviceInformation);

        when(wifiInfo.getMacAddress()).thenReturn(testString);
        when(deviceInformationSpy.getAndroidId()).thenReturn(null);
        when(deviceInformationSpy.getSerial()).thenReturn(null);

        final String deviceId = deviceInformationSpy.getCombinedDeviceIdAsSHA1String();

        assertEquals("DA39A3EE5E6B4B0D3255BFEF95601890AFD80709", deviceId);
    }

    @Test
    public void testGetCombinedDeviceIdAsSHA1StringWithNonNullValues() {
        testString = "zero";

        final DeviceInformation deviceInformationSpy = spy(deviceInformation);

        when(wifiInfo.getMacAddress()).thenReturn(testString);
        // The following line does not work for some unknown reason.
//        when(deviceInformationSpy.getAndroidId()).thenReturn(testString);
        when(deviceInformationSpy.getSerial()).thenReturn(testString);

        final String deviceId = deviceInformationSpy.getCombinedDeviceIdAsSHA1String();

        assertEquals("70F7E347000D9F8A614D4F826DA5715B6BDBC40C", deviceId);
    }

    @Test
    public void testGetCombinedDeviceIdAsSHA1StringWithBadAlgorithm() {
        deviceInformation.algorithm = "not-really-an-algorithm";
        final DeviceInformation deviceInformationSpy = spy(deviceInformation);


        when(wifiInfo.getMacAddress()).thenReturn("one");
        when(deviceInformationSpy.getAndroidId()).thenReturn(null);
        when(deviceInformationSpy.getSerial()).thenReturn("two");

        final String deviceId = deviceInformationSpy.getCombinedDeviceIdAsSHA1String();

        assertEquals("twoone", deviceId);
    }

    @Test
    public void testGetCombinedDeviceIdAsSHA1StringWithBadCharsetName() {
        deviceInformation.charsetName = "not-really-an-algorithm";
        final DeviceInformation deviceInformationSpy = spy(deviceInformation);


        when(wifiInfo.getMacAddress()).thenReturn("one");
        when(deviceInformationSpy.getAndroidId()).thenReturn(null);
        when(deviceInformationSpy.getSerial()).thenReturn("two");

        final String deviceId = deviceInformationSpy.getCombinedDeviceIdAsSHA1String();

        assertEquals("twoone", deviceId);
    }
}
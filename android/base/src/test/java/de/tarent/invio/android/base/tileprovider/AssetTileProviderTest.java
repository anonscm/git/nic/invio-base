package de.tarent.invio.android.base.tileprovider;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.osmdroid.tileprovider.IRegisterReceiver;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 18)
public class AssetTileProviderTest {

    @Test
    public void testZoomLevels() {
        final InvioTileSource tileSource = mock(InvioTileSource.class);
        when(tileSource.getMaximumZoomLevel()).thenReturn(10);
        when(tileSource.getMinimumZoomLevel()).thenReturn(0);

        // Only required by the super-constructor:
        final IRegisterReceiver registerReceiver = mock(IRegisterReceiver.class);

        final AssetTileProvider atp = new AssetTileProvider(registerReceiver, tileSource, null);

        assertEquals(atp.getMaximumZoomLevel(), 10);
        assertEquals(atp.getMinimumZoomLevel(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetNullTileSourceInConstructor() {
        // Only required by the super-constructor:
        final IRegisterReceiver registerReceiver = mock(IRegisterReceiver.class);

        final AssetTileProvider atp = new AssetTileProvider(registerReceiver, null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetWrongTileSource() {
        // Only required by the super-constructor:
        final IRegisterReceiver registerReceiver = mock(IRegisterReceiver.class);

        final InvioTileSource goodTileSource = mock(InvioTileSource.class);
        final ITileSource badTileSource = mock(ITileSource.class);

        final AssetTileProvider atp = new AssetTileProvider(registerReceiver, goodTileSource, null);
        atp.setTileSource(badTileSource);
    }

    @Test
    public void testSetNullTileSource() {
        final InvioTileSource tileSource = mock(InvioTileSource.class);

        // Only required by the super-constructor:
        final IRegisterReceiver registerReceiver = mock(IRegisterReceiver.class);

        final AssetTileProvider atp = new AssetTileProvider(registerReceiver, tileSource, null);

        try {
            atp.setTileSource(null);
            fail("tileSource == null should throw an exception. We don't want NPE later on.");
        } catch (final IllegalArgumentException ex) {
            // Fine!
        }
    }

    @Test
    public void testSetTileSource() {
        final InvioTileSource oldTileSource = mock(InvioTileSource.class);
        when(oldTileSource.getMaximumZoomLevel()).thenReturn(7);

        final InvioTileSource newTileSource = mock(InvioTileSource.class);
        when(newTileSource.getMaximumZoomLevel()).thenReturn(42);

        // Only required by the super-constructor:
        final IRegisterReceiver registerReceiver = mock(IRegisterReceiver.class);

        final AssetTileProvider atp = new AssetTileProvider(registerReceiver, oldTileSource, null);
        assertEquals(atp.getMaximumZoomLevel(), 7);

        atp.setTileSource(newTileSource);
        assertEquals(atp.getMaximumZoomLevel(), 42);
    }

}

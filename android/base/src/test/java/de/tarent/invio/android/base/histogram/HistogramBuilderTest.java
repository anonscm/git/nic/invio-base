package de.tarent.invio.android.base.histogram;

import de.tarent.invio.android.base.utils.InvioScanResult;
import de.tarent.invio.entities.Histogram;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(emulateSdk = 18)
public class HistogramBuilderTest {

    private HistogramBuilder histogramBuilder;

    @Before
    public void setup() {
        histogramBuilder = new HistogramBuilder("1234567890");
    }

    @Test
    public void testBuildWithoutAddingHistogram() {
        final Histogram histogram = histogramBuilder.build();

        assertEquals(0, histogram.size());
    }

    @Test
    public void testAddScanResultsWithDifferentLevels() {
        final InvioScanResult scanResult1 = new InvioScanResult("test", 1);
        final InvioScanResult scanResult2 = new InvioScanResult("test2", 5);
        final List<InvioScanResult> scan = new ArrayList<InvioScanResult>();
        scan.add(scanResult1);
        scan.add(scanResult2);

        histogramBuilder.addScanResults(scan);
        final Histogram histogram = histogramBuilder.build();

        assertTrue(histogram.get("test").containsKey(1));
        assertTrue(histogram.get("test2").containsKey(5));
    }

    @Test
    public void testAddScanResultsWithSameLevels() {
        final InvioScanResult scanResult1 = new InvioScanResult("test", 1);
        final InvioScanResult scanResult2 = new InvioScanResult("test2", 1);
        final List<InvioScanResult> scan = new ArrayList<InvioScanResult>();
        scan.add(scanResult1);
        scan.add(scanResult2);

        histogramBuilder.addScanResults(scan);
        final Histogram histogram = histogramBuilder.build();

        assertTrue(histogram.get("test").containsKey(1));
        assertTrue(histogram.get("test2").containsKey(1));
    }
}
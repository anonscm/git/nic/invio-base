package de.tarent.invio.mapserver.response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * This {@link Response} will be used by any {@link de.tarent.invio.mapserver.exception.InvioException}s.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class ExceptionResponse extends Response {

    public static final int INTERNAL_SERVER_ERROR_STATUS = 500;

    /**
     * Default constructor
     */
    public ExceptionResponse() {
        super(INTERNAL_SERVER_ERROR_STATUS);
    }

    /**
     * Custom constructor
     *
     * @param status Http-Status code from the server response.
     * @param json   json body from the server response
     * @throws IOException if the json body could not read.
     */
    public ExceptionResponse(int status, InputStream json) throws IOException {
        super(status, json);
    }

    /**
     * Another custom constructor
     *
     * @param status Http-Status code from the server response.
     * @param body   body data from the server response
     */
    public ExceptionResponse(int status, Map<String, Object> body) {
        super(status, body);
    }

    @Override
    public ExceptionResponse setJsonValue(String name, Object value) {
        super.setJsonValue(name, value);
        return this;
    }

}
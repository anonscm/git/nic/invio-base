package de.tarent.invio.mapserver.response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * A {@link Response} for shopping cart data uploads.
 *
 * @author Timo Kanera, tarent solutions GmbH
 */
public class CartDataResponse extends Response {

    /**
     * Default constructor.
     */
    public CartDataResponse() {
        super();
    }

    /**
     * Constructor.
     *
     * @param statusCode the status code
     */
    public CartDataResponse(final int statusCode) {
        super(statusCode);
    }

    /**
     * Constructor.
     *
     * @param status the status code
     * @param json   the json content
     * @throws IOException if an error occurred
     */
    public CartDataResponse(final int status, final InputStream json) throws IOException {
        super(status, json);
    }

    /**
     * Constructor.
     *
     * @param status the status code
     * @param body   the json contentn
     */
    public CartDataResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartDataResponse setJsonValue(final String name, final Object value) {
        super.setJsonValue(name, value);
        return this;
    }
}

package de.tarent.invio.mapserver.response;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * This {@link Response} will be used by map properties requests.
 */
public class MapPropertiesResponse extends Response {

    private static final String MAP_PROPERTIES = "mapProperties";

    /**
     * Default constructor.
     */
    public MapPropertiesResponse() {
        super();
    }

    /**
     * Constructor.
     *
     * @param status the status
     * @param body   the json data body
     */
    public MapPropertiesResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * Sets the map {@link Properties} and returns this.
     *
     * @param mapProperties the map {@link Properties} to set.
     * @return this
     */
    public final MapPropertiesResponse setMapProperties(final Properties mapProperties) {
        final Map<String, String> propertiesMap = new HashMap<String, String>();
        final Enumeration enumeration = mapProperties.propertyNames();
        while (enumeration.hasMoreElements()) {
            final String key = (String) enumeration.nextElement();
            propertiesMap.put(key, mapProperties.getProperty(key));
        }

        setJsonValue(MAP_PROPERTIES, mapProperties);
        return this;
    }

    /**
     * Gets the map {@link Properties}.
     *
     * @return the map {@link Properties}
     */
    public final Properties getMapProperties() {
        final Properties mapProperties = new Properties();
        final Map<String, String> propertiesMap = (Map<String, String>) getJsonBodyValue(MAP_PROPERTIES);
        for (final Map.Entry<String, String> entry : propertiesMap.entrySet()) {
            mapProperties.setProperty(entry.getKey(), entry.getValue());
        }

        return mapProperties;
    }

}

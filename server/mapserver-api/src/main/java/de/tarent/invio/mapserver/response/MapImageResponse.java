package de.tarent.invio.mapserver.response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * This {@link Response} will be used by map image requests.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class MapImageResponse extends Response {
    private static final String URL = "URL";

    /**
     * Default constructor.
     */
    public MapImageResponse() {
        super();
    }

    /**
     * Constructor.
     *
     * @param statusCode the status code
     */
    public MapImageResponse(final int statusCode) {
        super(statusCode);
    }

    /**
     * Constructor.
     *
     * @param status the status
     * @param json   the {@link InputStream} from json
     * @throws IOException if something is wrong with the {@link InputStream}
     */
    public MapImageResponse(final int status, final InputStream json) throws IOException {
        super(status, json);
    }

    /**
     * Constructor.
     *
     * @param status the status
     * @param body   the json data body
     */
    public MapImageResponse(final int status, final Map<String, Object> body) {
        super(status, body);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MapImageResponse setJsonValue(final String name, final Object value) {
        super.setJsonValue(name, value);
        return this;
    }

    /**
     * Get the mapimage url (under this url we can find all map relevant data)
     *
     * @return mapimage url
     */
    public final String getImageURL() {
        final Object result = getJsonBodyValue(URL);

        if (result == null) {
            return null;
        }
        return result.toString();
    }

    /**
     * Set the map image url (under this url we can find all map relevant information)
     *
     * @param url the Url
     * @return this
     */
    public final MapImageResponse setImageURL(final String url) {
        return setJsonValue(URL, url);
    }
}

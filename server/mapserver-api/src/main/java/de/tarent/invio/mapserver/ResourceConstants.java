package de.tarent.invio.mapserver;

/**
 * This interface includes all resource-constants.
 */
public interface ResourceConstants {

    /* ==================
     * Global
     * ==================
     */

    String PARAM_MAP_NAME = "map_name";

    String MAPSERVER_ROOT = "/mapserver";
    String MONITOR_ROOT = "/monitor";



    /* ==================
     * Logfile specific
     * ==================
     */

    String LOG_ZIP_UPLOAD = "/logs/{mapName}";


        /* ==================
     * MapList specific
     * ==================
     */

    String MAP_LIST_ROOT = "/map_list";
    String MULTILEVEL_MAP_LIST = "/multilevel_map_list";

    /* ==================
     * Position specific
     * ==================
     */

    String POSITION_ROOT = "/location?";
    String POSITION_PARAM_MAPNAME = "map";
    String POSITION_PARAM_LONGITUDE = "lng";
    String POSITION_PARAM_LATITUDE = "lat";
    String POSITION_PARAM_DEVICE_ID = "deviceId";
    String POSITION_PARAM_STATUS = "status";

    /* ==================
     * GroupData specific
     * ==================
     */

    String GROUP_DATA_ROOT = "/group_data";


    /* ==================
     * Mapdata specific
     * ==================
     */

    String MAP_DATA_ROOT = "/map_data";

    String MAP_DATA_NAME = "map_data_name";
    String MAP_DATA = "map_data";
    String MAPDATA_TEMPLATE = "/maps/{mapName}/data/";

    /* ==================
     * Mapimage specific
     * ==================
     */
    String MAP_IMAGE_ROOT = "/map_images";

    String MAP_IMAGE_PARAM_MAP_IMAGE = "map_image";

    String MAP_IMAGE_PARAM_LOWER = "lower";
    String MAP_IMAGE_PARAM_UPPER = "upper";
    String MAP_IMAGE_PARAM_LEFT = "left";
    String MAP_IMAGE_PARAM_RIGHT = "right";

    String MAP_TILEMAPRESOURCE_TEMPLATE = "/maps/{mapName}/tiles/tilemapresource.xml";


    /* ====================
     * Fingerprint specific
     * ====================
     */
    String FINGERPRINTS_DATA_ROOT = "/fingerprints";
    String FINGERPRINTS_DATA_COUNT = "/count";
    String FINGERPRINTS_DATA_MODE_DOWNLOAD = "download";
    String FINGERPRINTS_DATA_MODE_UPLOAD = "upload";
    String FINGERPRINTS_DATA_TYPE = "fingerprintsType";
    String FINGERPRINTS_DATA_JSON = "fingerprintsJson";
    String FINGERPRINTS_DATA_MAPNAME = "mapName";
    String FINGERPRINTS_TRANSMITTER = "transmitter";
    String FINGERPRINTS_TRANSMITTER_NAMES = "names";
    String FINGERPRINTS_TRANSMITTER_TYPE = "transmittersType";
    String FINGERPRINTS_TRANSMITTER_UPDATE = "update";
    String FINGERPRINTS_TRANSMITTER_ACTIVE = "active";
    String FINGERPRINTS_TRANSMITTER_ONLYACTIVE = "onlyactive";
    String FINGERPRINTS_DATA_TYPE_WIFI = "wifi";
    String FINGERPRINTS_DATA_TYPE_BTLE = "btle";

    /* ====================
     * Shopping Cart specific
     * ====================
     */
    String CART_DATA_ROOT = "/carts";
    String CART_DATA_PARAM_CLIENT_ID = "client_id";
    String CART_DATA_PARAM_CART = "cart";
    String CART_DATA_FILE_SUFFIX = ".json";

    /* ====================
     * Map properties specific
     * ====================
     */
    String MAP_PROPERTIES_ROOT = "/properties";
    String MAP_PROPERTIES_SUFFIX = ".properties";


    /* ====================
     * Version specific
     * ====================
     */
    String VERSION_ROOT = "/getVersion";
    String SERVER_VERSION = "JENKINS_PLACEHOLDER";
}

package de.tarent.invio.mapserver.response;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is responsible for uniform a response-schema.
 * This should be used for each interface that doesn't
 * return an html as response body.
 * <p/>
 * Diese Klasse ist dafür zuständig, ein einheitliches
 * Antwort-Schema zu liefern. Diese sollte für alle
 * Schnittstellen verwendet werden, die keine HTML-Datei
 * als Body zurückliefert.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class Response {
    /**
     * The status message key.
     */
    protected static final String STATUS_MESSAGE = "status";

    /**
     * Standard status message for when no json content was found.
     */
    protected static final String NO_JSON_MESSAGE = "Internal Server Error: No json-content in jsonBody!";

    private int statusCode;
    private Map<String, Object> jsonBody = new HashMap<String, Object>();

    /**
     * Default constructor.
     */
    public Response() {
        this(HttpStatus.SC_OK);
        setStatusMessage("successful"); //NOSONAR - Constructor calls overridable method
    }

    /**
     * Constructor.
     *
     * @param status The used HttpStatus code
     */
    public Response(final int status) {
        this.statusCode = status;
    }

    /**
     * Constructor.
     *
     * @param status         Http-Status code from the server response.
     * @param serverResponse json body from the server response
     * @throws IOException if the json body could not read.
     */
    @SuppressWarnings("unchecked")
    public Response(final int status, final InputStream serverResponse) throws IOException {
        this.statusCode = status;

        if (serverResponse != null) {
            final String json = IOUtils.toString(serverResponse, "UTF-8");
            this.jsonBody = new Gson().fromJson(json, Map.class);
        } else {
            this.setStatusMessage(NO_JSON_MESSAGE); //NOSONAR - Constructor calls overridable method
        }
    }

    /**
     * Constructor.
     * <p/>
     * <b>IMPORTANT</b> this constructor must be visible in <b>each</b> subclasses!
     *
     * @param status   Http-Status code from the server response.
     * @param jsonBody body data from the server response
     */
    public Response(final int status, final Map<String, Object> jsonBody) {
        this.statusCode = status;

        if (jsonBody != null) {
            this.jsonBody = jsonBody;
        } else {
            this.jsonBody = new HashMap<String, Object>();
            this.setStatusMessage(NO_JSON_MESSAGE); //NOSONAR - Constructor calls overridable method
        }
    }

    /**
     * Gets the status message from the jsonBody value {@link #STATUS_MESSAGE}.
     *
     * @return the status message
     */
    public final String getStatusMessage() {
        final Object msg = getJsonBodyValue(STATUS_MESSAGE);

        if (msg == null) {
            return null;
        }
        return msg.toString();
    }

    /**
     * Set the status message which will be included into
     * the json that will be included into the response.
     *
     * @param message a status message
     * @return the {@link Response}
     */
    public final Response setStatusMessage(final Object message) {
        return setJsonValue(STATUS_MESSAGE, message);
    }

    /**
     * Set a value to the json that will be included into
     * the response.
     *
     * @param name  The name of the json field.
     * @param value The value of the json field.
     * @return the {@link Response}
     */
    public Response setJsonValue(final String name, final Object value) {
        jsonBody.put(name, value);

        return this;
    }

    /**
     * @return Http-Status Code
     */
    public int getStatus() {
        return statusCode;
    }

    /**
     * Gets the json body value based on the given key.
     *
     * @param key the key
     * @return the value matching the key
     */
    public Object getJsonBodyValue(final String key) {
        return jsonBody.get(key);
    }

    Map<String, Object> getJsonBody() {
        return this.jsonBody;
    }

    /**
     * Builds the json object.
     *
     * @return json string
     */
    public String buildJson() {
        final Gson mapper = new Gson();
        return mapper.toJson(jsonBody);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + " [statusCode=" + statusCode + ", json=" + jsonBody + "]";
    }
}

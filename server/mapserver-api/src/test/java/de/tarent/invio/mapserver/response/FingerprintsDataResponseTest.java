package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class FingerprintsDataResponseTest {

    private FingerprintsDataResponse fingerprintsDataResponse;

    @Before
    public void setup() {
        fingerprintsDataResponse = new FingerprintsDataResponse();
    }

    @Test
    public void testThatSetJsonValueSetsTheValueInTheJsonBody() {
        final String key = "key";
        final String value = "value";
        fingerprintsDataResponse.setJsonValue(key, value);

        assertEquals(value, fingerprintsDataResponse.getJsonBodyValue(key));
    }

    @Test
    public void testConstructors() throws IOException {
        final int statusCode = 1;
        final Map<String, Object> jsonBody = new HashMap<String, Object>();
        jsonBody.put("test", "test");

        final FingerprintsDataResponse responseStatusMap = new FingerprintsDataResponse(statusCode, jsonBody);
        assertEquals(statusCode, responseStatusMap.getStatus());
        assertEquals("test", responseStatusMap.getJsonBodyValue("test"));
        assertNull(responseStatusMap.getStatusMessage());

        final Map<String, Object> nullJsonBody = null;
        final FingerprintsDataResponse responseStatusMapNull = new FingerprintsDataResponse(statusCode, nullJsonBody);
        assertEquals(statusCode, responseStatusMapNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusMapNull.getStatusMessage());
        assertNotNull(responseStatusMapNull.getJsonBody());
    }
}
package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class MapDataResponseTest {
    private MapDataResponse mapDataResponse;

    @Before
    public void setup() {
        mapDataResponse = new MapDataResponse();
    }

    @Test
    public void testThatSetJsonValueSetsTheValueInTheJsonBody() {
        final String key = "key";
        final String value = "value";
        mapDataResponse.setJsonValue(key, value);

        assertEquals(value, mapDataResponse.getJsonBodyValue(key));
    }

    @Test
    public void testConstructors() throws IOException {
        final int statusCode = 1;
        final MapDataResponse responseStatusCode = new MapDataResponse(statusCode);
        assertEquals(statusCode, responseStatusCode.getStatus());

        final InputStream inputStream = this.getClass().getResource("/response.json").openStream();
        final MapDataResponse responseStatusStream = new MapDataResponse(statusCode, inputStream);
        assertEquals(statusCode, responseStatusStream.getStatus());
        assertEquals("test", responseStatusStream.getJsonBodyValue("test"));
        assertNull(responseStatusStream.getStatusMessage());

        final InputStream nullInputStream = null;
        final MapDataResponse responseStatusStreamNull = new MapDataResponse(statusCode, nullInputStream);
        assertEquals(statusCode, responseStatusStreamNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusStreamNull.getStatusMessage());

        final MapDataResponse responseStatusMap = new MapDataResponse(statusCode, responseStatusStream.getJsonBody());
        assertEquals(statusCode, responseStatusMap.getStatus());
        assertEquals("test", responseStatusMap.getJsonBodyValue("test"));
        assertNull(responseStatusMap.getStatusMessage());

        final Map<String, Object> nullJsonBody = null;
        final MapDataResponse responseStatusMapNull = new MapDataResponse(statusCode, nullJsonBody);
        assertEquals(statusCode, responseStatusMapNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusMapNull.getStatusMessage());
        assertNotNull(responseStatusMapNull.getJsonBody());
    }
}
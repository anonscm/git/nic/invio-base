package de.tarent.invio.mapserver.response;

import org.apache.http.HttpStatus;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CartDataResponseTest {

    int statusCode = HttpStatus.SC_OK;

    @Test
    public void testCartDataResponseInt() {
        CartDataResponse response = new CartDataResponse(statusCode);
        assertEquals(statusCode, response.getStatus());
    }

    @Test
    public void testCartDataResponseIntMap() {
        Map<String, Object> jsonBody = new HashMap<String, Object>();
        CartDataResponse response = new CartDataResponse(statusCode, jsonBody);
        assertEquals(statusCode, response.getStatus());
        assertEquals(jsonBody, response.getJsonBody());
    }

    @Test
    public void testSetJsonValue() {
        String key = "key";
        CartDataResponse response = new CartDataResponse();
        response.setJsonValue(key, statusCode);
        assertEquals(statusCode, response.getJsonBodyValue(key));
    }

}

package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class MapImageResponseTest {
    private MapImageResponse mapImageResponse;

    private String urlKey = "URL";
    private String urlValue = "urlValue";

    @Before
    public void setup() {
        mapImageResponse = new MapImageResponse();
        mapImageResponse.setJsonValue(urlKey, urlValue);
    }

    @Test
    public void testThatGetImageUrlReturnsNullWhenJsonValueIsNull() {
        final MapImageResponse mapImageResponseSpy = spy(mapImageResponse);
        when(mapImageResponseSpy.getJsonBodyValue(any(String.class))).thenReturn(null);

        final String imageUrl = mapImageResponseSpy.getImageURL();

        assertNull(imageUrl);
    }

    @Test
    public void testThatGetImageUrlReturnsStringFromJsonBodyValue() {
        final String imageUrl = mapImageResponse.getImageURL();

        assertEquals(urlValue, imageUrl);
    }

    @Test
    public void testThatSetImageUrlSetsTheValueInTheJsonBody() {
        final String newUrl = "newUrl";
        mapImageResponse.setImageURL(newUrl);

        assertEquals(newUrl, mapImageResponse.getJsonBodyValue(urlKey));
    }

    @Test
    public void testConstructors() throws IOException {
        final int statusCode = 1;
        final MapImageResponse responseStatusCode = new MapImageResponse(statusCode);
        assertEquals(statusCode, responseStatusCode.getStatus());

        final InputStream inputStream = this.getClass().getResource("/response.json").openStream();
        final MapImageResponse responseStatusStream = new MapImageResponse(statusCode, inputStream);
        assertEquals(statusCode, responseStatusStream.getStatus());
        assertEquals("test", responseStatusStream.getJsonBodyValue("test"));
        assertNull(responseStatusStream.getStatusMessage());

        final InputStream nullInputStream = null;
        final MapImageResponse responseStatusStreamNull = new MapImageResponse(statusCode, nullInputStream);
        assertEquals(statusCode, responseStatusStreamNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusStreamNull.getStatusMessage());

        final MapImageResponse responseStatusMap = new MapImageResponse(statusCode, responseStatusStream.getJsonBody());
        assertEquals(statusCode, responseStatusMap.getStatus());
        assertEquals("test", responseStatusMap.getJsonBodyValue("test"));
        assertNull(responseStatusMap.getStatusMessage());

        final Map<String, Object> nullJsonBody = null;
        final MapImageResponse responseStatusMapNull = new MapImageResponse(statusCode, nullJsonBody);
        assertEquals(statusCode, responseStatusMapNull.getStatus());
        assertEquals(Response.NO_JSON_MESSAGE, responseStatusMapNull.getStatusMessage());
        assertNotNull(responseStatusMapNull.getJsonBody());
    }
}
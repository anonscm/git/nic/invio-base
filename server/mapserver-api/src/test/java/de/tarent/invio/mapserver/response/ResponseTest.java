package de.tarent.invio.mapserver.response;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ResponseTest {

    private Response response;

    @Before
    public void setup() {
        final Map<String, Object> jsonBody = new HashMap<String, Object>();
        jsonBody.put("test", "test");
        response = new Response(1, jsonBody);
    }

    @Test
    public void testThatBuildJsonReturnsProperJsonString() {
        final String jsonString = response.buildJson();

        assertEquals("{\"test\":\"test\"}", jsonString);
    }

    @Test
    public void testThatToStringPassesCorrectToString() {
        final String toString = response.toString();

        assertEquals("Response [statusCode=1, json={test=test}]", toString);
    }
}
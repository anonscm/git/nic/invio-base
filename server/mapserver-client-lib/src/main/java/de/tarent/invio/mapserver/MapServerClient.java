package de.tarent.invio.mapserver;


import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.response.CartDataResponse;
import de.tarent.invio.mapserver.response.MapDataResponse;
import de.tarent.invio.mapserver.response.MapImageResponse;
import de.tarent.invio.mapserver.response.MapListResponse;
import de.tarent.invio.mapserver.response.MapPropertiesResponse;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * This is the interface for the map server client.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public interface MapServerClient {

    // These constants are used to differentiate between the two technologies, in methods that have selection-parameters
    // for that. It can refer to fingerprints or transmitters (i.e. beacons/access points).
    int TYPE_WIFI = 0;
    int TYPE_BLUETOOTH = 1;

    /**
     * Returns the configured endpoint for the mapserver.
     *
     * @return the configured endpoint for the mapserver.
     */
    String getEndPoint();

    /**
     * Returns the configured endpoint for the monitor server.
     *
     * @return the configured endpoint for the monitor server.
     */
    String getMonitorEndPoint();

    /**
     * Upload a new map image. These image will be tiled by the server.
     * After that the server will give us the location of these tiles.
     *
     * @param mapName      the name of the map
     * @param left         the longitude of left edge of the boundingbox for this map
     * @param upper        the latitude of upper edge of the boundingbox for this map
     * @param right        the longitude of right edge of the boundingbox for this map
     * @param lower        the latitude of lower edge of the boundingbox for this map
     * @param mapImageFile the map image file
     * @return the server response
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    MapImageResponse uploadMapImage(final String mapName, final File mapImageFile,
                                    final float left, final float upper, final float right, final float lower)
            throws IOException;

    /**
     * Upload a new map image. These image will be tiled by the server.
     * After that the server will give us the location of these tiles.
     *
     * @param mapName  the name of the map
     * @param left     the longitude of left edge of the boundingbox for this map
     * @param upper    the latitude of upper edge of the boundingbox for this map
     * @param right    the longitude of right edge of the boundingbox for this map
     * @param lower    the latitude of lower edge of the boundingbox for this map
     * @param mapImage the map image
     * @return the server response
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    MapImageResponse uploadMapImage(final String mapName, final InputStream mapImage,
                                    final float left, final float upper, final float right, final float lower)
            throws IOException;

    /**
     * Upload a map data file.
     *
     * @param mapName     the name of the map
     * @param mapDataName the name of the map data
     * @param mapData     the map data file
     * @return the server response
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    MapDataResponse uploadMapData(final String mapName, final String mapDataName, final File mapData)
            throws IOException;

    /**
     * Uploads a map data file.
     *
     * @param mapName     the name of the map
     * @param mapDataName the name of the map data.
     * @param mapData     the map data {@link InputStream}
     * @return the server response.
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    MapDataResponse uploadMapData(final String mapName, final String mapDataName, final InputStream mapData)
            throws IOException;

    /**
     * Uploads a shopping cart.
     *
     * @param clientID the unique ID of the client
     * @param map      the name of the map
     * @param cart     the shopping cart {@link InputStream}
     * @return the server response
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    CartDataResponse uploadCartData(final String clientID, final String map, final InputStream cart)
            throws IOException;

    /**
     * Uploads a shopping cart.
     *
     * @param clientID the unique ID of the client
     * @param map      the name of the map
     * @param cart     the shopping cart
     * @return the server response
     * @throws IOException if there was a problem reading the file or transmitting
     *                     the request to the server (e.g the host is not reachable)
     */
    CartDataResponse uploadCartData(final String clientID, final String map, final File cart)
            throws IOException;

    /**
     * Download the mapdata from server
     *
     * @param mapName the map name
     * @return the xml string containing the mapdata
     * @throws IOException    TODO
     * @throws InvioException TODO
     */
    String downloadMapData(final String mapName) throws IOException, InvioException;

    /**
     * Download the transmitter info from the server.
     *
     * @param mapName the map name
     * @param transmitterType either TYPE_WIFI or TYPE_BLUETOOTH.
     * @return the json file with the description of the selected transmitters
     * @throws IOException    TODO
     * @throws InvioException TODO
     */
    String downloadTransmitter(final String mapName, int transmitterType) throws IOException, InvioException;

   /**
     * Download the transmitter info from the server.
     *
     * @param mapName the map name
     * @param timeout the connection timeout in milliseconds
     * @param transmitterType either TYPE_WIFI or TYPE_BLUETOOTH.
     * @return the json file with the description of the selected transmitters
     * @throws IOException    TODO
     * @throws InvioException TODO
     */
     String downloadTransmitter(final String mapName, final int timeout, int transmitterType)
             throws IOException, InvioException;
    
    /**
     * Download occupancy grid map image from server
     *
     * @param mapName the name of the map
     * @return String of grid map image response
     * @throws IOException TODO
     * @throws InvioException TODO
     */
    String downloadOccupancyGridMap(final String mapName) throws IOException, InvioException;

    /**
     * Uploads the fingerprint data file.
     *
     * @param mapName          the map name
     * @param fingerprintsJson the fingerprints in json-format
     * @throws IOException    when the client cannot execute the POST request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    void uploadWifiFingerprintsData(final String mapName, String fingerprintsJson) throws IOException, InvioException;

    /**
     * Uploads the fingerprint data file.
     *
     * @param mapName          the map name
     * @param fingerprintsJson the fingerprints in json-format
     * @throws IOException    when the client cannot execute the POST request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    void uploadBluetoothLEFingerprintsData(final String mapName, String fingerprintsJson)
            throws IOException, InvioException;

    /**
     * Download the fingerprints from server
     *
     * @param mapName the map name
     * @return the JSON-string containing the fingerprint-data
     * @throws IOException    when the client cannot execute the GET request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    String downloadWifiFingerprintsData(final String mapName) throws IOException, InvioException;

    /**
     * Download the fingerprints from server
     *
     * @param mapName the map name
     * @return the JSON-string containing the fingerprint-data
     * @throws IOException    when the client cannot execute the GET request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    String downloadBluetoothLEFingerprintsData(final String mapName) throws IOException, InvioException;


    /**
     * Download the fingerprints from server with counts instead of with probabilities
     *
     * @param mapName the map name
     * @return the JSON-string containing the fingerprint-data
     * @throws IOException    when the client cannot execute the GET request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    String downloadWifiFingerprintsDataWithCount(final String mapName) throws IOException, InvioException;

    /**
     * Download the fingerprints from server with counts instead of with probabilities
     *
     * @param mapName the map name
     * @return the JSON-string containing the fingerprint-data
     * @throws IOException    when the client cannot execute the GET request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    String downloadBluetoothLEFingerprintsDataWithCount(final String mapName) throws IOException, InvioException;

    /**
     * Uploads the position data
     *
     * @param mapName     the map name
     * @param deviceId    the unique device name
     * @param latitudeE6  position
     * @param longitudeE6 position
     * @param level       the current level/floor
     * @param status      the customer's status
     * @throws IOException    when the client cannot execute the POST request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    void uploadPositionData(final String mapName, final String deviceId, final int latitudeE6, final int longitudeE6,
                            final int level, final String status) throws IOException, InvioException;

    /**
     * Query a list of available maps from the map server.
     *
     * @param timeout the http-timeouts (in ms) that should be used
     * @return the server response containing a list of existing maps. Will contain an empty list in case of timeout
     * @throws IOException if the {@link MapListResponse} could not be built
     */
    MapListResponse getMapList(final int timeout) throws IOException;

    /**
     * Query a list of available maps from the map server. No special timeout, try for whatever is the default...
     *
     * @return the server response containing a list of existing maps
     * @throws IOException if the {@link MapListResponse} could not be built
     */
    MapListResponse getMapList() throws IOException;

    /**
     * Get the tilemapresource.xml from the server. It contains the available zoomlevels as well as the coordinates
     * of the boundingbox.
     *
     * @param mapName the map name
     * @return a String containing the raw xml
     * @throws InvioException if the server doesn't return OK
     * @throws IOException    if something else with the connection doesn't work out
     */
    String getTilemapresourceXml(final String mapName) throws IOException, InvioException;


    /**
     * Get the group data zip file as a stream from the server.
     *
     * @param groupName name of the group / building, for example "tarent solutions GmbH"
     * @param timeout   how long should we wait for the answer
     * @return the group data file as a zip file
     * @throws IOException if something went wrong during the connection.
     */
    InputStream getGroupData(final String groupName, final int timeout) throws IOException;


    /**
     * Downloads the map properties from the server.
     *
     * @param mapProperties the file name
     * @return the map configuration as a {@link MapPropertiesResponse}
     * @throws IOException    if something went wrong while connecting
     * @throws InvioException if the file does not exist
     */
    MapPropertiesResponse downloadMapProperties(final String mapProperties) throws IOException, InvioException;


    /**
     * Upload the zipfile which contains all the sensor-logs.
     * It either succeeds or throws an exception. The caller should not concern itself with other details.
     *
     * @param mapName the name of the map
     * @param zipFile the complete zip file
     * @throws InvioException if we don't get the http status that we expect (202 ACCEPTED).
     * @throws IOException if something is wrong with the zipfile.
     */
    void uploadLogZip(String mapName, File zipFile) throws InvioException, IOException;

    /**
     * Downloads the transmitter names for a map
     * @param mapName the map name
     * @return the JSON-string containing the transmitter-names
     * @throws IOException    when the client cannot execute the GET request
     * @throws InvioException when something bad happens (TODO: what might that be?)
     */
    String downloadTransmitterNames(final String mapName) throws IOException, InvioException;

}

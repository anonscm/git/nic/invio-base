package de.tarent.invio.mapserver.response;

import de.tarent.invio.mapserver.exception.InvioRuntimeException;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Map;

/**
 * This class is responsible for building our {@link Response}.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 * @author Atanas Alexandrov, <a.alexandrov@tarent.de>
 */
public final class ResponseBuilder {

    private static final String EXPECTED_CONTENT_TYPE = "application/json";


    private ResponseBuilder() {
        //this is a utility class and there is no need for a instance
    }

    /**
     * Build a {@link Response} from a {@link HttpResponse}.
     *
     * @param httpResponse the response from the server
     * @param responseType target specific type of response
     * @param <T>          target specific type of response
     * @return our response-object
     * @throws IOException if there was a problem with reading the body.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Response> T build(HttpResponse httpResponse, Class<T> responseType) throws IOException {
        final String contentType = httpResponse.getEntity().getContentType().getValue();
        final Response result = getResponse(httpResponse, contentType);
        if (responseType == Response.class) {
            return (T) result;
        }

        try {
            final Constructor<T> constructor = responseType.getConstructor(int.class, Map.class);
            final Map<String, Object> bodyArg = getBodyFromJsonResponse(contentType, result);
            return constructor.newInstance(result.getStatus(), bodyArg);
        } catch (Exception e) { //NOSONAR - i dont want to catch > 7 exceptions separately!
            throw new InvioRuntimeException("No response constructor for " + responseType, e);
        }
    }

    /**
     * Gets the json body from the given {@link Response}.
     *
     * @param contentType the content type
     * @param response    the json body {@link Response}
     * @return the json body
     */
    private static Map<String, Object> getBodyFromJsonResponse(final String contentType, final Response response) {
        Map<String, Object> body = null;
        if (contentType.contains(EXPECTED_CONTENT_TYPE)) {
            body = response.getJsonBody();
        }
        return body;
    }

    /**
     * Gets the {@link Response} based on the {@link HttpResponse} and content type.
     *
     * @param httpResponse the {@link HttpResponse}
     * @param contentType  the content type
     * @return the {@link Response}
     * @throws IOException if the http response encounters issues
     */
    private static Response getResponse(final HttpResponse httpResponse, final String contentType) throws IOException {
        final InputStream body = httpResponse.getEntity().getContent();
        final int statusCode = httpResponse.getStatusLine().getStatusCode();
        final Response response;
        if (contentType.contains(EXPECTED_CONTENT_TYPE)) {
            response = new Response(statusCode, body);
        } else {
            response = new Response(statusCode);
        }
        return response;
    }
}

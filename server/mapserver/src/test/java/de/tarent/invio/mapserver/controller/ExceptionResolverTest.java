package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

public class ExceptionResolverTest {

    ExceptionResolver toTest;

    @Before
    public void setup() {
        toTest = spy(new ExceptionResolver());
    }

    @Test
    public void handleExceptions() {
        final InvioException aException = new InvioException("aMessage");

        final ResponseEntity<String> result = toTest.handleExceptions(aException);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        assertTrue(result.getBody().contains(aException.getMessage()));
    }

}

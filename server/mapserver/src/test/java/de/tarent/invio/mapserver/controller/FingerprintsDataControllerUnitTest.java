package de.tarent.invio.mapserver.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.Transmitter;
import de.tarent.sellfio.web.model.WifiAp;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by mley on 03.11.15.
 */
public class FingerprintsDataControllerUnitTest {


    @Test
    public void testTransmitterNameMapping() {
        HashMap<String, String> result;

        FingerprintsDataController fdc = new FingerprintsDataController();

        TransmitterDAO daoMock = mock(TransmitterDAO.class);

        // virtual aps with ap name, on vap has ssid
        List<WifiAp> aps = new ArrayList<>();
        WifiAp ap = new WifiAp();
        ap.setName("meinAP");
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "ab:cd:ef")));
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "ab:cd:eg")));
        ap.getNetworks().get(1).setName("ssid1");
        aps.add(ap);



        // virtual aps without ap name, one vap has ssid
        ap = new WifiAp();
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "bb:cd:ef")));
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "bb:cd:eg")));
        ap.getNetworks().get(1).setName("ssid2");
        aps.add(ap);



        // with ap name and ssid
        ap = new WifiAp();
        ap.setName("ap3");
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "cb:cd:ef")));
        ap.getNetworks().get(0).setName("ssid3");
        aps.add(ap);



        // with ap name
        ap = new WifiAp();
        ap.setName("ap2");
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "db:cd:ef")));
        aps.add(ap);



        // no ap name, with ssid
        ap = new WifiAp();
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "eb:cd:ef")));
        ap.getNetworks().get(0).setName("ssid3");
        aps.add(ap);


        // no ap name, no ssid
        ap = new WifiAp();
        ap.getNetworks().add(new NetworkNode(new Transmitter(1, true, "wifi", "fb:cd:ef")));
        aps.add(ap);



        List<BtleTransmitter> btles = new ArrayList<>();
        btles.add(new BtleTransmitter(new Transmitter(1, true, "btle", "uuid_1_2")));
        btles.add(new BtleTransmitter(new Transmitter(1, true, "btle", "uuid_1_3")));
        btles.get(1).setName("iBacon1");

        when(daoMock.getWifiAps(anyString())).thenReturn(aps);
        when(daoMock.getIBeacons(anyString())).thenReturn(btles);

        ReflectionTestUtils.setField(fdc, "transmitterDAO", daoMock);

        String json = fdc.getTransmitterNamesByMap("map");

        Gson gson = new Gson();
        Type t = new TypeToken<HashMap<String, String>>(){}.getType();
        result = gson.fromJson(json, t);


        assertEquals("meinAP_ab:cd:ef", result.get("ab:cd:ef"));
        assertEquals("meinAP_ssid1", result.get("ab:cd:eg"));

        assertEquals("bb:cd:ef", result.get("bb:cd:ef"));
        assertEquals("ssid2", result.get("bb:cd:eg"));

        assertEquals("ap3", result.get("cb:cd:ef"));

        assertEquals("ap2", result.get( "db:cd:ef"));

        assertEquals("ssid3", result.get("eb:cd:ef"));

        assertEquals("fb:cd:ef", result.get("fb:cd:ef"));

        assertNull(result.get("uuid_1_2"));
        assertEquals("iBacon1", result.get("uuid_1_3"));
    }


}

package de.tarent.invio.mapserver.exception;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class InvioExceptionTest {

    @Test
    public void testIfMessageIsSet() {
        String testMessage = "testmessage";
        InvioException tempInvioException = new InvioException(testMessage);
        assertTrue(tempInvioException.getMessage().equals(testMessage));
    }

    @Test
    public void testIfCauseIsSet() {
        Throwable testCause = new Throwable();
        InvioException tempInvioException = new InvioException(testCause);
        assertTrue(tempInvioException.getCause() == testCause);
    }


    @Test
    public void testIfMessageAndCauseAreSet() {
        String testMessage = "testmessage";
        Throwable testCause = new Throwable();
        InvioException tempInvioException = new InvioException(testMessage, testCause);
        assertTrue(tempInvioException.getMessage().equals(testMessage));
        assertTrue(tempInvioException.getCause() == testCause);
    }

}

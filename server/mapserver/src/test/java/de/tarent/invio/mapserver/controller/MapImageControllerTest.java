package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.gdal.Gdal;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletRequest;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyFloat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class MapImageControllerTest {

    // The object we want to test:
    private MapImageController mapImageController;

    @Mock
    private Gdal gdal;

    @Mock
    private DirectoryManager directoryManager;

    @Mock
    private ServletRequest request;


    @Before // Call this method before each of the @Test-methods:
    public void setUp() throws IOException {
        initMocks(this);

        mapImageController = spy(new MapImageController(request, gdal, directoryManager));

        // This might cause problems as a {@link System} property is being changed. It might be better to have this
        // line cleaned up after all tests have been run. (Maybe saving the original value and then setting it after).
        System.setProperty("catalina.base", System.getProperty("java.io.tmpdir"));

        when(directoryManager.getTempDirectory()).thenCallRealMethod();
        when(directoryManager.getTempFile()).thenCallRealMethod();
    }
    
    @Test
    public void generateTilesUrl() {
        final File localPath = new File("/var/lib/tomcat/webapps/maps/kitty/");
        doReturn("localhost").when(request).getServerName();
        doReturn(8080).when(request).getServerPort();

        final String resultURL = mapImageController.generateTilesUrl(localPath);

        assertEquals("http://localhost:8080/maps/kitty/", resultURL);
    }


    // This test is boring because the controller doesn't do much. Integration-test will be more interesting.
    // TODO: check what can be tested regarding the boundingbox-coordinates!
    @Test
    public void testUpload() throws IOException, InvioException {
        final File mapTilesDirectory = directoryManager.getTempDirectory();
        final String gdalResult = "aGdalPath";
        try {
            doReturn(mapTilesDirectory).when(directoryManager).getMapTilesDirectory(anyString());
            doNothing().when(mapImageController).updateTiles(anyString(), any(File.class));
            doReturn(gdalResult).when(gdal).makeRasterTiles(anyString(), anyFloat(), anyFloat(), anyFloat(), anyFloat());
            doReturn("http://localhost:8080/maps/kitty").when(mapImageController).generateTilesUrl(any(File.class));

            // MultipartFile is a big interface, so we use a mock instead:
            MultipartFile file = mock(MultipartFile.class);

            ResponseEntity<String> response = null;


            try {
                doReturn(0).when(mapImageController).runMegaScript(any(File.class), anyString());
                response = mapImageController.uploadImage("myFile", 1f, 2f, 2f, 1f, file);
            } catch (InvioException e) {
                //MEGA_SCRIPT is not present an is not relevant for this test.
                e.printStackTrace();
            }


            verify(gdal).makeRasterTiles(anyString(), anyFloat(), anyFloat(), anyFloat(), anyFloat());
            verify(mapImageController).updateTiles(eq(gdalResult), eq(mapTilesDirectory));
            assertTrue(response.getBody().contains("image-upload successful"));
            assertTrue(response.getBody().contains("localhost"));
        } finally {
            FileUtils.deleteDirectory(mapTilesDirectory);
        }
    }

    @Test(expected = InvioException.class)
    public void upload_IOException() throws InvioException, IOException {
        doThrow(new IOException()).when(directoryManager).getMapTilesDirectory(anyString());

        // MultipartFile is a big interface, so we use a mock instead:
        MultipartFile file = mock(MultipartFile.class);

        mapImageController.uploadImage("myFile", 1f, 2f, 2f, 1f, file);
    }

    @Test
    public void updateTiles_firstTime() throws IOException {
        final File tmpDir = directoryManager.getTempDirectory();
        final File aFile = new File(tmpDir, "test");
        aFile.createNewFile();
        aFile.deleteOnExit();

        final File targetDestination = directoryManager.getTempDirectory();

        try {
            mapImageController.updateTiles(tmpDir.getAbsolutePath(),
                    targetDestination);

            assertFalse(tmpDir.exists());
            assertTrue(targetDestination.exists());
            assertTrue(targetDestination.list().length == 1);
        } finally {
            FileUtils.deleteDirectory(tmpDir);
            FileUtils.deleteDirectory(targetDestination);
        }
    }

    @Test
    public void updateTiles_secondTime() throws IOException {
        final File tmpDir = directoryManager.getTempDirectory();
        File aFile = new File(tmpDir, "test");
        aFile.createNewFile();
        aFile.deleteOnExit();

        final File targetDestination = directoryManager.getTempDirectory();
        aFile = new File(targetDestination, "test1");
        aFile.createNewFile();
        aFile.deleteOnExit();
        aFile = new File(targetDestination, "test2");
        aFile.createNewFile();
        aFile.deleteOnExit();

        try {
            mapImageController.updateTiles(tmpDir.getAbsolutePath(),
                    targetDestination);

            assertFalse(tmpDir.exists());
            assertTrue(targetDestination.exists());
            assertTrue(targetDestination.list().length == 1);
        } finally {
            FileUtils.deleteDirectory(tmpDir);
            FileUtils.deleteDirectory(targetDestination);
        }
    }
}

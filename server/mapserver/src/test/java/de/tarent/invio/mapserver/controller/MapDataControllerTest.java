package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MapDataControllerTest {

    MapDataController toTest;

    @Mock
    DirectoryManager directoryManager;

    @Before
    public void setup() {
        initMocks(this);
        toTest = spy(new MapDataController(directoryManager));
    }

    @Test
    public void getTargetDestination() throws IOException {
        final String mapName = "aMapName";
        final String mapDataName = "aMapDataName";
        final File mapDataRoot = new File("/tmp/");

        doReturn(mapDataRoot).when(directoryManager).getMapDataDirectory(eq(mapName));

        final File result = toTest.getTargetDestination(mapName, mapDataName);

        assertEquals(new File(mapDataRoot, mapDataName).getAbsolutePath(),
                result.getAbsolutePath());
    }

    @Test
    public void upload_successful() throws InvioException, IOException {
        final String mapName = "mapName";
        final String mapDataName = "mapDataName";
        final MultipartFile mockMultipartFile = mock(MultipartFile.class);
        when(mockMultipartFile.getOriginalFilename()).thenReturn("filename.osm");

        final File targetDestination = new File("./test");
        doReturn(targetDestination).when(toTest).getTargetDestination(anyString(), anyString());

        doNothing().when(directoryManager).moveFile(any(File.class), any(File.class));

        final ResponseEntity<String> result = toTest.upload(mapName, mapDataName, mockMultipartFile);

        assertEquals(HttpStatus.OK, result.getStatusCode());

        ArgumentCaptor<File> fileCap = ArgumentCaptor.forClass(File.class);
        verify(mockMultipartFile).transferTo(fileCap.capture());
        verify(directoryManager).moveFile(same(fileCap.getValue()), same(targetDestination));
        verify(toTest).getTargetDestination(eq(mapName), eq(mapDataName));
    }

    @Test(expected = InvioException.class)
    public void upload_IOException() throws InvioException, IOException {
        final String mapName = "mapName";
        final String mapDataName = "mapDataName";
        final MultipartFile mockMultipartFile = mock(MultipartFile.class);
        when(mockMultipartFile.getOriginalFilename()).thenReturn("filename.osm");

        doThrow(new IOException()).when(toTest).getTargetDestination(anyString(), anyString());

        toTest.upload(mapName, mapDataName, mockMultipartFile);
    }

}

package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MapListControllerTest {

    // The object we want to test:
    private MapListController mapListController;

    private final String MAPNAME_1 = "froscon";
    private final String MAPNAME_2 = "tarent1og";
    private final String MAPNAME_3 = "testfloor-tarent3";
    private final String MAPNAME_4 = "testfloor-tarent4";

    @Mock
    private DirectoryManager directoryManager;

    @Before // Call this method before each of the @Test-methods:
    public void setUp() throws IOException, InvioException {
        mapListController = new MapListController(directoryManager);
    }

    @Test
    public void testGetMapList() throws InvioException {
        List<String> resultList = Arrays.asList(MAPNAME_1, MAPNAME_2);
        when(directoryManager.getMapList()).thenReturn(resultList);

        ResponseEntity<String> response = mapListController.getMapList();

        assertTrue(response.getBody().contains(MAPNAME_1));
        assertTrue(response.getBody().contains(MAPNAME_2));
    }

    @Test
    public void testGetMultilevelMapList() throws InvioException, IOException, SAXException, ParserConfigurationException {
        List<String> resultList = Arrays.asList(MAPNAME_1, MAPNAME_3, MAPNAME_4);
        when(directoryManager.getMapList()).thenReturn(resultList);

        File map1Directory = new File(this.getClass().getResource("/froscon.osm").getFile()).getParentFile();
        when(directoryManager.getMapDataDirectory(MAPNAME_1)).thenReturn(map1Directory);
        File map3Directory = new File(this.getClass().getResource("/testfloor-tarent3.osm").getFile()).getParentFile();
        when(directoryManager.getMapDataDirectory(MAPNAME_3)).thenReturn(map3Directory);
        File map4Directory = new File(this.getClass().getResource("/testfloor-tarent4.osm").getFile()).getParentFile();
        when(directoryManager.getMapDataDirectory(MAPNAME_4)).thenReturn(map4Directory);

        ResponseEntity<String> result = mapListController.getMultilevelMapList("tarent AG");
        assertFalse(result.getBody().contains(MAPNAME_1));
        assertTrue(result.getBody().contains(MAPNAME_3));
        assertTrue(result.getBody().contains(MAPNAME_4));

        // Again to test case sensitivity
        result = mapListController.getMultilevelMapList("TARENT ag");
        assertFalse(result.getBody().contains(MAPNAME_1));
        assertTrue(result.getBody().contains(MAPNAME_3));
        assertTrue(result.getBody().contains(MAPNAME_4));
    }

    @Test
    public void testGetMultilevelMapListWithFileNotFoundException() throws InvioException, IOException, SAXException, ParserConfigurationException {
        List<String> resultList = Arrays.asList(MAPNAME_1, MAPNAME_3, MAPNAME_4);
        when(directoryManager.getMapList()).thenReturn(resultList);

        ResponseEntity<String> result = mapListController.getMultilevelMapList("tarent AG");
        // empty mapList is correct
        assertEquals("{\"mapList\":[],\"status\":\"successful\"}", result.getBody());
    }
}

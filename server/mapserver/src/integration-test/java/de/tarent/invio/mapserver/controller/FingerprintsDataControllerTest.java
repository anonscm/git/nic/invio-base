package de.tarent.invio.mapserver.controller;


import de.tarent.sellfio.web.dao.AbstractDAO;
import de.tarent.sellfio.web.dao.FingerprintDAO;
import de.tarent.sellfio.web.dao.FingerprintDAOUtils;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.Histogram;
import org.apache.commons.io.IOUtils;
import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;


/**
 * Class to test the management of fingerprints in the database.
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/test-context.xml"})
@ContextConfiguration(classes = {TestConfig.class})
@WebAppConfiguration
public class FingerprintsDataControllerTest extends AbstractDAO {

    FingerprintsDataController fingerprintsDataController;

    @Autowired
    private FingerprintDAO fingerprintDAO;

    @Autowired
    private TransmitterDAO transmitterDAO;

    private static String WIFI_JSON = "fingerprints_data_tarent3og.json";
    private static String BTLE_JSON = "fingerprints_btle_data_tarent3og.json";
    private static String WIFI_JSON2 = "fingerprints_wifi.json";

    private static String WIFI_PROBABILITY_JSON = "fingerprints_probability_wifi.json";
    private static String BTLE_PROBABILITY_JSON = "fingerprints_probability_btle.json";
    private static String WIFI_COUNT_JSON = "fingerprints_count_wifi.json";
    private static String BTLE_COUNT_JSON = "fingerprints_count_btle.json";

    public static String getStringFromFile(String fileName) {
        String str = "";
        try {
            URL resource = FingerprintsDataControllerTest.class.getClassLoader().getResource(fileName);
            if (resource != null) {
                str = IOUtils.toString(resource);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Before
    public void startup() {
        fingerprintsDataController = new FingerprintsDataController();
        ReflectionTestUtils.setField(fingerprintsDataController, "fingerprintDAO", fingerprintDAO);
        ReflectionTestUtils.setField(fingerprintsDataController, "transmitterDAO", transmitterDAO);
    }



    /**
     * Execute a SQL sentence
     *
     * @param sql query
     */
    @Transactional(value = "txName", readOnly = false)
    public void executeSQL(String sql) {
        final Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
        query.executeUpdate();
    }

    /**
     * Checks for equality of fingerprint lists, ignoring the ordering
     *
     * @param fp1 list 1
     * @param fp2 list 2
     * @return true if equals, false otherwise
     */
    private boolean fingerprintsEqual(List<Fingerprint> fp1, List<Fingerprint> fp2) {
        return fp1.size() == fp2.size() && fp1.containsAll(fp2);
    }

    /**
     * Test getting a json from a non existing map in the data base
     *
     * @throws IOException
     */
    @Test
    public void testDataNotExisting() throws IOException {
        String json;
        json = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);

        assertEquals(json, "[]");
    }

    @Test
    public void testInsertionWifi() throws IOException {
        String sourceJson = getStringFromFile(WIFI_JSON);
        fingerprintsDataController.uploadFingerprints("testMap", "wifi", sourceJson);
        String resultJsonWifi = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);
        String resultJsonBtle = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "btle", false);

        List<Fingerprint> sourceFingerprints = FingerprintDAOUtils.fromJson(sourceJson);
        List<Fingerprint> resultFingerprints = FingerprintDAOUtils.fromJson(resultJsonWifi);

        assertTrue(fingerprintsEqual(sourceFingerprints, resultFingerprints));
        assertEquals(resultJsonBtle, "[]");
    }

    @Test
    public void testInsertionBluetoothLE() throws IOException {
        String sourceJson = getStringFromFile(BTLE_JSON);
        fingerprintsDataController.uploadFingerprints("testMap", "btle", sourceJson);
        String resultJsonWifi = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);
        String resultJsonBtle = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "btle", false);

        List<Fingerprint> sourceFingerprints = FingerprintDAOUtils.fromJson(sourceJson);
        List<Fingerprint> resultFingerprints = FingerprintDAOUtils.fromJson(resultJsonBtle);

        assertTrue(fingerprintsEqual(sourceFingerprints, resultFingerprints));
        assertEquals(resultJsonWifi, "[]");
    }

    @Test
    public void testMultipleInsertions() throws IOException {
        String json1 = getStringFromFile(WIFI_JSON);
        String json2 = getStringFromFile(WIFI_JSON2);

        List<Fingerprint> fingerprints2 = FingerprintDAOUtils.fromJson(json2);

        fingerprintsDataController.uploadFingerprints("testMap", "wifi", json1);
        fingerprintsDataController.uploadFingerprints("testMap", "wifi", json2);

        String jsonResult = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);

        assertTrue(fingerprintsEqual(fingerprints2, FingerprintDAOUtils.fromJson(jsonResult)));
    }

    @Test
    public void testMultipleInsertionsDontAlterOtherMaps() throws IOException {
        String json1 = getStringFromFile(WIFI_JSON);
        String json2 = getStringFromFile(WIFI_JSON2);

        List<Fingerprint> fingerprints1 = FingerprintDAOUtils.fromJson(json1);
        List<Fingerprint> fingerprints2 = FingerprintDAOUtils.fromJson(json2);

        fingerprintsDataController.uploadFingerprints("testMap", "wifi", json1);
        fingerprintsDataController.uploadFingerprints("testMap2", "wifi", json2);

        String jsonResultMap1 = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);
        String jsonResultMap2 = fingerprintsDataController.getFingerprintsByMapAndType("testMap2", "wifi", false);

        assertTrue(fingerprintsEqual(fingerprints1, FingerprintDAOUtils.fromJson(jsonResultMap1)));
        assertTrue(fingerprintsEqual(fingerprints2, FingerprintDAOUtils.fromJson(jsonResultMap2)));
    }


    @Test
    public void testUpdateAllTransmitterNoJsonResult() throws IOException {
        String json = getStringFromFile(WIFI_JSON);

        List<Fingerprint> fingerprints = FingerprintDAOUtils.fromJson(json);

        fingerprintsDataController.uploadFingerprints("testMap", "wifi", json);


        Set<String> identifiers = new HashSet<>();

        //Get all the different identifier to put active=false
        for (Fingerprint fingerprint : fingerprints) {
            Histogram histogram = fingerprint.getHistogram();

            for (String identifier : histogram.keySet()) {
                identifiers.add(identifier);
            }
        }

        for (String identifier : identifiers) {
            fingerprintsDataController.updateTransmitterActive("testMap", identifier, false);
        }

        String jsonResultMap1 = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", true);
        String jsonResultMap2 = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);

        assertEquals(jsonResultMap1, "[]");
        assertTrue(fingerprintsEqual(fingerprints, FingerprintDAOUtils.fromJson(jsonResultMap2)));
    }


    @Test
    public void testInsertionCounterGetCounter() throws IOException {
        String sourceJsonCountWifi = getStringFromFile(WIFI_COUNT_JSON);
        String sourceJsonCountBtle = getStringFromFile(BTLE_COUNT_JSON);

        List<Fingerprint> sourceFingerprintsCountWifi = FingerprintDAOUtils.fromJson(sourceJsonCountWifi);
        List<Fingerprint> sourceFingerprintsCountBtle = FingerprintDAOUtils.fromJson(sourceJsonCountBtle);

        fingerprintsDataController.uploadFingerprints("testMap", "wifi", sourceJsonCountWifi);
        fingerprintsDataController.uploadFingerprints("testMap", "btle", sourceJsonCountBtle);

        String resultJsonWifi = fingerprintsDataController.getFingerprintsByMapAndTypeWithCount("testMap", "wifi", false);
        String resultJsonBtle = fingerprintsDataController.getFingerprintsByMapAndTypeWithCount("testMap", "btle", false);

        List<Fingerprint> resultFingerprintsWifi = FingerprintDAOUtils.fromJson(resultJsonWifi);
        List<Fingerprint> resultFingerprintsBtle = FingerprintDAOUtils.fromJson(resultJsonBtle);

        assertTrue(fingerprintsEqual(sourceFingerprintsCountWifi, resultFingerprintsWifi));
        assertTrue(fingerprintsEqual(sourceFingerprintsCountBtle, resultFingerprintsBtle));
    }


    @Test
    public void testInsertionCounterGetProbability() throws IOException {
        String sourceJsonCountWifi = getStringFromFile(WIFI_COUNT_JSON);
        String sourceJsonCountBtle = getStringFromFile(BTLE_COUNT_JSON);

        String sourceJsonProbabilityWifi = getStringFromFile(WIFI_PROBABILITY_JSON);
        String sourceJsonProbabilityBtle = getStringFromFile(BTLE_PROBABILITY_JSON);

        List<Fingerprint> sourceFingerprintsProbabilityWifi = FingerprintDAOUtils.fromJson(sourceJsonProbabilityWifi);
        List<Fingerprint> sourceFingerprintsProbabilityBtle = FingerprintDAOUtils.fromJson(sourceJsonProbabilityBtle);

        fingerprintsDataController.uploadFingerprints("testMap", "wifi", sourceJsonCountWifi);
        fingerprintsDataController.uploadFingerprints("testMap", "btle", sourceJsonCountBtle);

        String resultJsonWifi = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "wifi", false);
        String resultJsonBtle = fingerprintsDataController.getFingerprintsByMapAndType("testMap", "btle", false);

        List<Fingerprint> resultFingerprintsWifi = FingerprintDAOUtils.fromJson(resultJsonWifi);
        List<Fingerprint> resultFingerprintsBtle = FingerprintDAOUtils.fromJson(resultJsonBtle);

        assertTrue(fingerprintsEqual(sourceFingerprintsProbabilityWifi, resultFingerprintsWifi));
        assertTrue(fingerprintsEqual(sourceFingerprintsProbabilityBtle, resultFingerprintsBtle));
    }

}

package de.tarent.invio.mapserver.controller;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.SQLException;

/**
 * Created by mley on 30.10.15.
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(value = {"de.tarent.sellfio.web"})
@PropertySources({
        @PropertySource(value = "classpath:config.properties")
})
public class TestConfig {

    /*
     * If you are wondering where the rest of the configuration is, look into the webcommon project. The
     * PersistenceConfig class is used to setup hibernate, flyway, transactions, etc
     *
     */


    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    @Bean(name = "dataSource")
    public DataSource dataSource() throws PropertyVetoException, SQLException {
        final ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass("org.postgresql.Driver");
        ds.setJdbcUrl("jdbc:postgresql://localhost:5432/test");
        ds.setUser("test");
        ds.setPassword("test");

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        // delete any existing tables before we start
        String sql = FingerprintsDataControllerTest.getStringFromFile("fingerprintsDataController_drop_tables.sql");

        ds.getConnection().createStatement().execute(sql);

        return ds;
    }


}

package de.tarent.invio.mapserver.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.invio.mapserver.ResourceConstants;
import de.tarent.sellfio.web.dao.FingerprintDAO;
import de.tarent.sellfio.web.dao.FingerprintDAOUtils;
import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * The GroupDataController is used to download multiple files for a whole group of maps in one request. A group is
 * supposed to be individual levels in the same building.
 *
 * It contains, for each map in the group:
 * - the osm-file
 * - the tilemapresource.xml
 * - the fingerprints_data.json
 *
 * It is a bit outdated, because the fingerprints need to be requested through the FingerprintsDataController (they
 * are stored in the DB now, and not in a file any more).
 * Also, at the moment we don't use multi level maps at all in our clients.
 */
@Controller
@RequestMapping(ResourceConstants.GROUP_DATA_ROOT)
public class GroupDataController {

    private static final Logger LOGGER = Logger.getLogger(GroupDataController.class);

    @Autowired
    private final DirectoryManager directoryManager;

    @Autowired
    private final MapListController mapListController;
    private String groupName;

    @Autowired
    private FingerprintDAO fingerprintDAO;

    /**
     * Creates a controller which returns all the data of a group (fingerprints, the .osm and the tilemapinfo).
     * Because no server side modifications are done, we use only get requests.
     *
     * @param directoryManager  contains directory/path-helper-methods, to get the map (directory) names
     * @param mapListController contains the method for getting a list of mapnames belonging to a group
     */

    @Autowired
    public GroupDataController(final DirectoryManager directoryManager, final MapListController mapListController) {
        this.directoryManager = directoryManager;
        this.mapListController = mapListController;
    }

    /**
     * This method returns all the data (except for the tiles) of maps belonging to a given group.
     *
     * @param groupName the group for which the data should be returned
     * @return a zipfile containing the .osm, the fingerprints and the tilemapresource.xml of all maps belonging
     * to the group
     * @throws ParserConfigurationException if the parser was configured the wrong way
     * @throws SAXException                 if something went wrong with the parsing of the groupdata json containing
     * the mapnames
     * @throws InvioException               exceptions produced by our code
     * @throws java.io.IOException          if something went wrong with the file operations
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public ByteArrayResource getGroupData(@RequestParam("group") String groupName)
            throws ParserConfigurationException, SAXException, InvioException, IOException {
        this.groupName = groupName;
        final String mapListJSON = mapListController.getMultilevelMapList(groupName).getBody();
        final Gson gson = new GsonBuilder().create();
        final JsonElement groupDataJsonElement = gson.fromJson(mapListJSON, JsonElement.class);
        final JsonObject groupDataJsonObject = groupDataJsonElement.getAsJsonObject();
        final int mapNameNumber = groupDataJsonObject.getAsJsonArray("mapList").size();
        final List<String> mapNames = new ArrayList<String>();
        for (int i = 0; i < mapNameNumber; i++) {
            mapNames.add(groupDataJsonObject.getAsJsonArray("mapList").get(i).toString().replace("\"", ""));
        }
        if (!mapNames.isEmpty()) {
            final byte[] zipStream;
            if (checkIfGroupDataZipFileIsUpToDate(mapNames)) {
                zipStream = getCachedZipByteArray();
            } else {
                zipStream = generateZipByteArray(mapNames);
            }
            return new ByteArrayResource(zipStream);
        }
        return null;
    }

    /**
     * Returns the cached group zip file as an array of bytes
     *
     * @return byte-array containing the group zip file
     * @throws java.io.IOException is thrown if something IO-related goes wrong
     */
    private byte[] getCachedZipByteArray() throws IOException {
        final Path groupDataPath = Paths.get(
                directoryManager.getGroupDataDirectory() + File.separator + groupName + ".zip");
        final byte[] groupDataByteArray = Files.readAllBytes(groupDataPath);
        return groupDataByteArray;
    }

    /**
     * Here we create the group data zip file in memory depending on the list of the map names (e.g. folder names).
     *
     * @param mapNames representing the folder names inside the maps directory
     * @return byte array containing zip file
     * @throws java.io.IOException if something went wrong with the file operations
     */
    protected byte[] generateZipByteArray(List<String> mapNames) throws IOException {
        final File mapDataParentFile = directoryManager.getMapDataDirectory(mapNames.get(0)).getParentFile();
        final String mapPath = mapDataParentFile.getParent();
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final ZipOutputStream groupDataZip = new ZipOutputStream(bos);

        //We need to collect 3 files per map: osf-file, fingerprints and tilemapresource and zip it.
        for (String mapName : mapNames) {
            final ZipEntry data = new ZipEntry(mapName + "/data/" + mapName + ".osm");
            final File dataFile = new File(
                    mapPath + File.separator + mapName + "/data/" + mapName + ".osm");
            writeToZipOutputStream(groupDataZip, dataFile, data);

            final List<Fingerprint> fingerprints = fingerprintDAO.getByMapAndType(mapName, "wifi", true);

            if (!fingerprints.isEmpty()) {
                final ZipEntry fingerprintsZipEntry = new ZipEntry(mapName + "/fingerprints/fingerprints_data.json");
                writeToZipOutputStream(groupDataZip, FingerprintDAOUtils.toJson(fingerprints), fingerprintsZipEntry);
            }

            final ZipEntry tilemapresource = new ZipEntry(mapName + "/tiles/tilemapresource.xml");
            final File tilemapresourceFile = new File(
                    mapPath + File.separator + mapName + "/tiles/tilemapresource.xml");
            writeToZipOutputStream(groupDataZip, tilemapresourceFile, tilemapresource);
        }
        groupDataZip.close();
        createOrReplaceGroupDataZipFile(bos);
        return bos.toByteArray();
    }

    /**
     * Creates a new zip file, replacing any old one with the same name
     *
     * @param bos the data to be persisted/cached
     * @throws java.io.IOException is thrown if something goes wrong with the IO-Operations
     */
    private void createOrReplaceGroupDataZipFile(ByteArrayOutputStream bos) throws IOException {
        final String groupFilePath = directoryManager.getGroupDataDirectory().getPath();
        File groupFile = new File(groupFilePath);
        if (groupFile.mkdirs()) {
            groupFile = new File(groupFilePath + File.separator + groupName + ".zip");
            try (FileOutputStream groupFileOutputStream = new FileOutputStream(groupFile)) {
                groupFileOutputStream.write(bos.toByteArray());
                groupFileOutputStream.close();
            } catch (IOException e) {
                LOGGER.error("IO-Error", e);
            }
        }
    }

    /**
     * Checks if any file belonging to the group was modified after the current group zip file was created.
     *
     * @param mapNames the names of all maps belonging to the current group
     * @return true if the zip file is up to date, otherwise false is returned
     * @throws java.io.IOException is thrown if some IO-Operation fails
     */
    private boolean checkIfGroupDataZipFileIsUpToDate(List<String> mapNames) throws IOException {
        final File groupFile = new File(
                directoryManager.getGroupDataDirectory().getPath() + File.separator + groupName + ".zip");
        final File mapDataParentFile = directoryManager.getMapDataDirectory(mapNames.get(0)).getParentFile();
        final String mapPath = mapDataParentFile.getParent();
        //Collect all files of the group. so that we can compare the last modification dates
        final File[] files = new File[mapNames.size() * 3];
        for (int i = 0; i < mapNames.size(); i++) {
            files[i * 3] = new File(
                    mapPath + File.separator + mapNames.get(i) + "/data/" + mapNames.get(i) + ".osm");
            files[i * 3 + 1] = new File(
                    mapPath + File.separator + mapNames.get(i) + "/fingerprints/fingerprints_data.json");
            files[i * 3 + 2] = new File(
                    mapPath + File.separator + mapNames.get(i) + "/tiles/tilemapresource.xml");
        }

        //Check if any of the files was modified after the zip-File was created
        for (File file : files) {
            if (!groupFile.exists() || groupFile.lastModified() < file.lastModified()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Write the file as the zip entry to the zip output stream.
     *
     * @param groupDataZip ZipOutputStream
     * @param file         which should be added to the archive
     * @param zipEntry     representing the file inside the archive
     * @throws java.io.IOException if file cannot be read
     */
    private void writeToZipOutputStream(final ZipOutputStream groupDataZip, final File file, final ZipEntry zipEntry)
            throws IOException {

        if (file.exists()) {
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                final byte[] b = new byte[(int) file.length()];
                fileInputStream.read(b);
                groupDataZip.putNextEntry(zipEntry);
                groupDataZip.write(b);
            } catch (IOException e) {
                LOGGER.error("IO-Error", e);
            }
        }
    }

    /**
     * Write the file as the zip entry to the zip output stream.
     *
     * @param groupDataZip ZipOutputStream
     * @param str         which string should be added to the archive
     * @param zipEntry    representing the file inside the archive
     * @throws java.io.IOException if file cannot be read
     */
    private void writeToZipOutputStream(final ZipOutputStream groupDataZip, final String str, final ZipEntry zipEntry)
            throws IOException {

        if (str != null && str.length() > 0) {
            groupDataZip.putNextEntry(zipEntry);
            groupDataZip.write(str.getBytes(StandardCharsets.UTF_8));
        }
    }

}

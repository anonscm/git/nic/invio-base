package de.tarent.invio.mapserver.exception;

/**
 * This exception should be used when any GDAL related exception is thrown.
 * <p/>
 * Diese Exception sollte verwendet werden, wenn es
 * sich um eine Ausnhame im Zusammenhang mit GDAL handelt.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class GdalException extends InvioRuntimeException {

    /**
     * Default constructor.
     */
    public GdalException() {
        super();
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     */
    public GdalException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     *
     * @param cause the cause of the exception
     */
    public GdalException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor.
     *
     * @param message the message to be shown
     * @param cause   the cause of the exception
     */
    public GdalException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

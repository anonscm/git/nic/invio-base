package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.management.DirectoryManager;
import de.tarent.invio.mapserver.response.MapPropertiesResponse;
import de.tarent.invio.mapserver.response.Response;
import de.tarent.invio.mapserver.response.ResponseBuilder;
import de.tarent.invio.mapserver.wrapper.IOUtilWrapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static de.tarent.invio.mapserver.ResourceConstants.MAP_PROPERTIES_ROOT;
import static de.tarent.invio.mapserver.ResourceConstants.MAP_PROPERTIES_SUFFIX;

/**
 * The controller for the downloading of the map properties.
 */
@Controller
@RequestMapping(MAP_PROPERTIES_ROOT)
public class MapPropertiesController {

    private static final Logger LOGGER = Logger.getLogger(MapPropertiesController.class);

    @Autowired
    private final DirectoryManager directoryManager;

    private final IOUtilWrapper ioUtilWrapper = new IOUtilWrapper();

    /**
     * Constructor.
     *
     * @param directoryManager the {@link DirectoryManager}
     */
    @Autowired
    public MapPropertiesController(final DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }

    /**
     * Gets the map properties.
     *
     * @param mapProperties the map properties file name, without the suffix, found in the config.xml
     * @return the {@link ResponseEntity} with the downloaded data
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity getMapProperties(@RequestParam("map_properties") String mapProperties) {
        return buildResponse(mapProperties);
    }

    private ResponseEntity buildResponse(final String fileName) {
        final Properties mapProperties = readMapProperties(fileName);
        if (mapProperties.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        final Response response = new MapPropertiesResponse().setMapProperties(mapProperties);
        return ResponseBuilder.buildResponseEntity(response);
    }

    private Properties readMapProperties(final String fileName) {
        final Properties mapProperties = new Properties();
        FileInputStream fileInputStream = null;
        try {
            final File propertiesDirectory = directoryManager.getPropertiesDirectory();
            final File propertiesFile = new File(propertiesDirectory + File.separator + fileName
                    + MAP_PROPERTIES_SUFFIX);
            fileInputStream = new FileInputStream(propertiesFile);
            mapProperties.load(fileInputStream);
        } catch (final IOException e) {
            LOGGER.warn(e);
        } finally {
            ioUtilWrapper.closeQuietly(fileInputStream);
        }
        return mapProperties;
    }
}

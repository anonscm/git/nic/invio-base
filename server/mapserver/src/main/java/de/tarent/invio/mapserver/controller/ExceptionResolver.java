package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.exception.InvioException;
import de.tarent.invio.mapserver.exception.InvioRuntimeException;
import de.tarent.invio.mapserver.response.ExceptionResponse;
import de.tarent.invio.mapserver.response.Response;
import de.tarent.invio.mapserver.response.ResponseBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * This class is responsible for resolving the exceptions which
 * can be thrown by the handler-methods in each controller.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
@ControllerAdvice
@EnableWebMvc
public class ExceptionResolver {

    private static final Logger LOGGER = Logger.getLogger(ExceptionResolver.class);

    /**
     * Handles thrown {@link InvioException}s.
     *
     * @param e the {@link InvioRuntimeException} that was thrown
     * @return A uniform exception response
     */
    @ExceptionHandler({InvioException.class, InvioRuntimeException.class})
    public ResponseEntity<String> handleExceptions(final InvioException e) {
        LOGGER.debug("Exception " + e.getClass().getName() + " will be resolved.", e);

        final Response response = new ExceptionResponse()
                .setStatusMessage(e.getMessage());

        final ResponseEntity<String> responseEntity = ResponseBuilder.buildResponseEntity(response);
        return responseEntity;
    }
}

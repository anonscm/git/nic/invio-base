package de.tarent.invio.mapserver.utils;


import com.google.gson.Gson;
import com.google.gson.JsonParseException;

/**
 * Checks if a given string is valid JSON.
 */
public final class JSONValidator {

    /**
     * This class should not be instantiated.
     */
    private JSONValidator() {
    }

    /**
     * Checks if a given string is valid JSON.
     *
     * @param toCheck the string to be validated
     * @return true if the string is valid json, false if it is not
     */
    public static boolean isJSONValid(String toCheck) {
        final Gson gson = new Gson();
        try {
            gson.toJson(toCheck);
        } catch (JsonParseException e) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            return false;
        }
        return true;
    }
}

package de.tarent.invio.mapserver.controller;

import de.tarent.invio.mapserver.ResourceConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * A controller used for server version information
 */
@Controller
@RequestMapping(ResourceConstants.VERSION_ROOT)
public class InvioVersionController {

    /**
     * Returns the current server version as a response to a GET-request.
     *
     * @return current version
     */
    @RequestMapping(method = RequestMethod.GET, produces = "text/plain")
    @ResponseBody
    public String getVersion() {
        return ResourceConstants.SERVER_VERSION;
    }
}

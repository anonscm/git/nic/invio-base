package de.tarent.invio.mapserver.response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * This class is responsible for building a {@link ResponseEntity}
 * from our {@link Response}.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public final class ResponseBuilder {

    private ResponseBuilder() {
        //this is a utility class and there is no need for a instance
    }

    /**
     * Builds the {@link ResponseEntity} from the {@link Response}.
     *
     * @param response the {@link Response}
     * @return the {@link ResponseEntity}
     */
    public static ResponseEntity<String> buildResponseEntity(final Response response) {
        final HttpHeaders headers = buildHeaders();

        final ResponseEntity<String> responseEntity = new ResponseEntity<String>(
                response.buildJson(), headers, HttpStatus.valueOf(response.getStatus()));

        return responseEntity;
    }

    private static HttpHeaders buildHeaders() {
        final HttpHeaders headers = new HttpHeaders();

        //set content type to json
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }
}

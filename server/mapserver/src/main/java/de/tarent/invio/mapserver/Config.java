package de.tarent.invio.mapserver;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import de.tarent.invio.mapserver.management.DirectoryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.IOException;

/**
 * Created by mley on 29.07.15.
 */
@Configuration
@PropertySources({
        // The second source, if it is found, will override values from the first source, i.e. the included
        // config.properties has the default values:
        @PropertySource("classpath:config.properties"),
        @PropertySource(value = "file:///etc/sellfio/mapserver.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file://${CATALINA_BASE}/conf/mapserver.properties", ignoreResourceNotFound = true)
})
public class Config {

    @Autowired
    private Environment env;

    /**
     * PropertySourcesPlaceholderConfigurer factory
     *
     * @return a PropertySourcesPlaceholderConfigurer object
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * Get the datasource.
     *
     * @return DataSource object
     * @throws java.beans.PropertyVetoException ¯\_(ツ)_/¯
     */
    @Bean(name = "dataSource")
    public DataSource dataSource() throws PropertyVetoException {
        final ComboPooledDataSource ds = new ComboPooledDataSource();
        ds.setDriverClass(env.getProperty("db.driver"));
        ds.setJdbcUrl(env.getProperty("db.url"));
        ds.setUser(env.getProperty("db.user"));
        ds.setPassword(env.getProperty("db.password"));

        ds.setAcquireIncrement(5);
        ds.setMinPoolSize(5);
        ds.setMaxPoolSize(50);
        ds.setMaxIdleTime(1800);
        ds.setIdleConnectionTestPeriod(1800);
        ds.setMaxStatements(50);

        return ds;
    }

    /**
     * Creates a new JDBC Template
     * @param dataSource DataSource object
     * @return the JDBC Template
     */
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /**
     * Get the multipart resolver bean with 10MB upload size
     *
     * @return CommonMultipartResolver object
     */
    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver getMultiPartResolver() {
        final CommonsMultipartResolver cmr = new CommonsMultipartResolver();
        cmr.setMaxUploadSize(env.getProperty("multipartResolver.maxUploadSize", Integer.class));
        return cmr;
    }

    /**
     * Get the DirectoryManager bean.
     *
     * @return DirectoryManager object.
     * @throws IOException if initialization fails
     */
    @Bean(name = "directoryManager")
    public DirectoryManager getDirectoryManager() throws IOException {
        final DirectoryManager dm = new DirectoryManager(env.getProperty("directoryManager.relativePath"));
        return dm;
    }


}

package de.tarent.invio.mapserver.management;

import de.tarent.invio.mapserver.exception.InvioException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is responsible for managing the map directory tree.
 *
 * @author Sven Schumann, <s.schumann@tarent.de>
 */
public class DirectoryManager {

    /* Directory-Tree:
     *
     * \ <catalina.home>
     * |- <relPath>                 - see constructor arg
     *   \- <mapname>               - the name of the map
     *   | \- tiles                 - subdir for map tiles
     *   | | \- <zoom>              - for each zoom level
     *   | |   \- <x-pos>           - x-position
     *   | |     \- <y-pos>.png     - y-position
     *   | |- mapdata               - subdir for map data (created by the map editor)
     *   | |  \- <data>.osm         - example map data
     *   | |- fingerprints          - subdir for fingerprints (created by admin-app)
     *   | |  \- fp.xml              - example fingerprint-file
     *   |- <mapname>               - and so on...
     *     \- ...
     */

    protected static final String SUB_DIR_FINGERPRINTS = File.separator + "fingerprints" + File.separator;
    protected static final String SUB_DIR_DATA = File.separator + "data" + File.separator;
    protected static final String SUB_DIR_TILES = File.separator + "tiles" + File.separator;
    private final File rootDir;

    /**
     * Creates a {@link DirectoryManager}. This constructor is used by spring.
     *
     * @param relPath the relative path for the base directory from catalina.home
     * @throws IOException when the maps directory cannot be created
     */
    public DirectoryManager(final String relPath) throws IOException {
        this.rootDir = getMapsDirectory(relPath);
    }

    /**
     * Creates a unique temporary file. This file will be automatically deleted when finished.
     *
     * @return a temporary file.
     * @throws IOException when file creation failed.
     */
    public File getTempFile() throws IOException {
        final File tmp = File.createTempFile("temp", ".tmp");
        tmp.deleteOnExit();

        return tmp;
    }

    /**
     * Creates a unique temporary directory. This can be used for
     * store files temporarily. <b>ATTENTION</b>: You must delete the directory
     * manually if you no longer require it.
     *
     * @return a temporary directory.
     * @throws IOException when directory creation failed.
     */
    public File getTempDirectory() throws IOException {
        final File temp = File.createTempFile("temp", Long.toString(System.nanoTime()));

        if (!temp.delete()) {
            throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
        }

        verifyDirectoryExistence(temp);
        return temp;
    }

    /**
     * Verifies whether or not the directory exists on the server. If it does not exist, then it creates it.
     *
     * @param directory the maps directory
     * @throws IOException when directory was not created
     */
    private void verifyDirectoryExistence(final File directory) throws IOException {
        if (!directory.exists() && !directory.mkdirs()) {  //inclusive sub-dirs
            throw new IOException("Could not create directory: " + directory.getAbsolutePath());
        }
    }

    private File getMapsDirectory(final String subPath) throws IOException {
        final File catalinaBase = new File(System.getProperty("catalina.base")).getAbsoluteFile();
        final File mapsDirectory = new File(catalinaBase, subPath);

        verifyDirectoryExistence(mapsDirectory);
        return mapsDirectory;
    }

    /**
     * Get the directory for the map tiles.
     *
     * @param mapName the name of the map
     * @return the map tiles directory
     * @throws IOException if the directory could not be created
     */
    public File getMapTilesDirectory(final String mapName) throws IOException {
        final File mapTilesDirectory = new File(rootDir, mapName + SUB_DIR_TILES);

        verifyDirectoryExistence(mapTilesDirectory);
        return mapTilesDirectory;
    }

    /**
     * Get the directory for the map data.
     *
     * @param mapName the name of the map
     * @return the map data directory
     * @throws IOException if the directory could not be created
     */
    public File getMapDataDirectory(final String mapName) throws IOException {
        final File mapDataDirectory = new File(rootDir, mapName + SUB_DIR_DATA);

        verifyDirectoryExistence(mapDataDirectory);
        return mapDataDirectory;
    }

    /**
     * Get the directory for the fingerprints.
     *
     * @param mapName the name of the map.
     * @return the fingerprints directory
     * @throws IOException if the directory could not be created
     */
    public File getMapFingerprintsDirectory(final String mapName) throws IOException {
        final File mapFingerprintsDirectory = new File(rootDir, mapName + SUB_DIR_FINGERPRINTS);

        verifyDirectoryExistence(mapFingerprintsDirectory);
        return mapFingerprintsDirectory;
    }




    /**
     * Returns the directory used to store group-Zips.
     *
     * @return the group data directory
     * @throws IOException if the directory could not be created
     */
    public File getGroupDataDirectory() throws IOException {
        final File groupDataDirectory = new File(this.rootDir + File.separator + "groupData" + File.separator);

        verifyDirectoryExistence(groupDataDirectory);

        return groupDataDirectory;
    }

    /**
     * Returns the directory used to store the map properties.
     *
     * @return the properties directory
     * @throws IOException if the directory could not be created
     */
    public File getPropertiesDirectory() throws IOException {
        final File propertiesDirectory = new File(this.rootDir + File.separator + "properties" + File.separator);

        verifyDirectoryExistence(propertiesDirectory);

        return propertiesDirectory;
    }

    /**
     * Returns an  unsorted string list of existing maps - eg folder names - under the root directory.
     *
     * @return list of the existing map names
     * @throws InvioException Exception in case of a missing read permission for the file system
     */
    public List<String> getMapList() throws InvioException {
        final List<String> result = new ArrayList<>();
        if (rootDir != null) {
            if (!rootDir.canRead()) {
                throw new InvioException("Missing permission to read the file system.");
            }

            final List<File> mapDirectories = getOnlyDirectoriesList();

            for (File file : mapDirectories) {
                result.add(file.getName());
            }
        }
        return result;
    }

    /**
     * Moves the given file to the given destination. A previously existing destination file is deleted first.
     *
     * @param source      the source file
     * @param destination the destination file
     * @throws IOException if the destination couldn't be deleted or moving the file failed
     */
    public void moveFile(File source, File destination) throws IOException {
        if (destination.exists() && !destination.delete()) {
            throw new IOException("Failed to delete source file.");
        }
        final File destinationFile = new File(destination.getAbsolutePath());
        FileUtils.moveFile(source, destinationFile);
    }

    /**
     * Returns a list with only directories in it.
     *
     * @return list of directories ignoring files
     */
    private List<File> getOnlyDirectoriesList() {
        return Arrays.asList(rootDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory();
            }
        }));
    }

}

package de.tarent.invio.mapserver.controller;

import com.google.gson.Gson;
import de.tarent.sellfio.web.dao.FingerprintDAO;
import de.tarent.sellfio.web.dao.FingerprintDAOUtils;
import de.tarent.sellfio.web.dao.FingerprintStoreException;
import de.tarent.sellfio.web.dao.TransmitterDAO;
import de.tarent.sellfio.web.model.BtleTransmitter;
import de.tarent.sellfio.web.model.Fingerprint;
import de.tarent.sellfio.web.model.NetworkNode;
import de.tarent.sellfio.web.model.WifiAp;
import de.tarent.sellfio.web.util.FingerprintsUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_COUNT;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_JSON;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_MAPNAME;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_MODE_DOWNLOAD;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_MODE_UPLOAD;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_ROOT;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_DATA_TYPE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER_ACTIVE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER_NAMES;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER_ONLYACTIVE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER_TYPE;
import static de.tarent.invio.mapserver.ResourceConstants.FINGERPRINTS_TRANSMITTER_UPDATE;


/**
 * This controller is used to handle the upload and download of the fingerprints.
 * <p/>
 * Also to handle:
 * - the update of a transmitter's status (active=true or active=false).
 * - the download of transmitter's identifiers by map
 * <p/>
 * The handling of Fingerprints and Transmitters is done together here.
 * TODO: maybe they should be separated, or the controller should be renamed.
 */
@RestController
public class FingerprintsDataController {

    private static final Logger LOGGER = Logger.getLogger(FingerprintsDataController.class);


    @Autowired
    private FingerprintDAO fingerprintDAO;

    @Autowired
    private TransmitterDAO transmitterDAO;

    /**
     * Get the JSON String for the Fingerprints filtered by map_id and fingerprint_type ('wifi' or 'btle').
     * This version contains probabilities for the signal strengths instead of counts. For new fingerprints, which
     * already are stored with absolute counts, the probabilities will be calculated on the fly (and will not be
     * persisted).
     *
     * @param mapName               the map name
     * @param fingerprintType       'wifi' or 'btle'
     * @param onlyActiveTransmitter true: only include measurements for active transmitters (default);
     *                              false: all measurements
     * @return a JSON String
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_DATA_MODE_DOWNLOAD, method = RequestMethod.GET)
    public String getFingerprintsByMapAndType(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName,
                                              @RequestParam(FINGERPRINTS_DATA_TYPE) String fingerprintType,
                                              @RequestParam(value = FINGERPRINTS_TRANSMITTER_ONLYACTIVE,
                                                      required = false,
                                                      defaultValue = "true") Boolean onlyActiveTransmitter) {

        try {
            final List<Fingerprint> fingerprints = fingerprintDAO.getByMapAndType(mapName,
                    fingerprintType,
                    onlyActiveTransmitter);
            final List fingerprintsWithCalculatedProbability =
                    FingerprintsUtil.getFingerprintsWithCalculatedProbability(fingerprints);
            return FingerprintDAOUtils.toJson(fingerprintsWithCalculatedProbability);
        } catch (final Exception e) { //NOSONAR
            // TODO: should this not be an internal server error? If not, then why the exception?
            LOGGER.error("Download of " + fingerprintType + " fingerprints for map " + mapName + " failed due to " +
                    e.getMessage());
        }

        return "[]";
    }


    /**
     * Get the JSON String for the Fingerprints filtered by map_id and fingerprint_type ('wifi' or 'btle').
     * This version contains absolute scan-counts instead of probabilities. (Old) fingerprints which are still
     * stored with probabilities will be converted on the fly (and the result will be stored in the DB).
     *
     * @param mapName               the map name
     * @param fingerprintType       'wifi' or 'btle'
     * @param onlyActiveTransmitter true: only include measurements for active transmitters (default);
     *                              false: all measurements
     * @return a JSON String
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_DATA_COUNT + "/" +
            FINGERPRINTS_DATA_MODE_DOWNLOAD, method = RequestMethod.GET)
    public String getFingerprintsByMapAndTypeWithCount(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName,
                                                       @RequestParam(FINGERPRINTS_DATA_TYPE) String fingerprintType,
                                                       @RequestParam(value = FINGERPRINTS_TRANSMITTER_ONLYACTIVE,
                                                               required = false,
                                                               defaultValue = "true") Boolean onlyActiveTransmitter) {

        try {
            final List<Fingerprint> fingerprints = fingerprintDAO.getByMapAndType(mapName,
                    fingerprintType,
                    onlyActiveTransmitter);

            updateFingerprintCountsIfNecessary(mapName, fingerprintType, fingerprints);

            return FingerprintDAOUtils.toJson(fingerprints);
        } catch (final Exception e) { //NOSONAR
            // TODO: should this not be an internal server error? If not, then why the exception?
            LOGGER.error("Download of " + fingerprintType + " fingerprints for map " + mapName + " failed due to " +
                    e.getMessage());
        }

        return "[]";
    }


    /**
     * If any of these fingerprints have a count < 1 then they are still old fingerprints with probabilities instead
     * of counts. We then restore the counts as closely as possible and save them again.
     * TODO: remove this method after all data on all servers has been converted.
     *
     * @param fingerprints the list of Fingerprints from our DB
     */
    private void updateFingerprintCountsIfNecessary(final String mapName,
                                                    final String fingerprintType,
                                                    final List<Fingerprint> fingerprints)
            throws FingerprintStoreException {

        // Check if any fingerprint has probabilities < 1 (i.e. not "counts"). (It should be all or none.)
        boolean convert = false;
        FingerprintsLoop:
        for (Fingerprint fingerprint : fingerprints) {
            for (Map<Integer, Float> transmitter : fingerprint.getHistogram().values()) {
                for (Float count : transmitter.values()) {
                    if (count < 1) {
                        convert = true;
                        break FingerprintsLoop;
                    }
                }
            }
        }

        if (convert) {
            FingerprintDAOUtils.convertProbabilitiesToCounts(fingerprints);
            fingerprintDAO.replaceFingerprints(mapName, fingerprintType, fingerprints);
        }
    }


    /**
     * Get a json from List<String> which is a list of identifiers of transmitters.
     *
     * @param mapName               Name of the map
     * @param transmitterType       'wifi' or 'btle'
     * @param onlyActiveTransmitter true: only include active transmitters (default); false: all transmitters
     * @return A json from a a List of the identifiers of the transmitters
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_TRANSMITTER + "/" +
            FINGERPRINTS_DATA_MODE_DOWNLOAD, method = RequestMethod.GET)
    public String getTransmittersByMap(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName,
                                       @RequestParam(FINGERPRINTS_TRANSMITTER_TYPE) String transmitterType,
                                       @RequestParam(value = FINGERPRINTS_TRANSMITTER_ONLYACTIVE,
                                               required = false,
                                               defaultValue = "true") Boolean onlyActiveTransmitter) {

        final List transmitters = transmitterDAO.getTransmitterIdsByMapAndType(
                mapName, transmitterType, onlyActiveTransmitter);
        final Gson gson = new Gson();
        return gson.toJson(transmitters);

    }

    /**
     * Get a json for the mapping of transmitter identifiers to more human readable names.
     *
     * @param mapName Name of the map
     * @return json
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_TRANSMITTER + "/" +
            FINGERPRINTS_TRANSMITTER_NAMES + "/" + FINGERPRINTS_DATA_MODE_DOWNLOAD, method = RequestMethod.GET)
    public String getTransmitterNamesByMap(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName) {

        final Map<String, String> mapping = new HashMap<>();
        for (WifiAp ap : transmitterDAO.getWifiAps(mapName)) {
            final boolean virtualAp = ap.getNetworks().size() > 1;
            for (NetworkNode n : ap.getNetworks()) {
                mapping.put(n.getTransmitter().getIdentifier(),
                        getName(ap.getName(), n.getName(), n.getMacAddress(), virtualAp));
            }
        }
        for (BtleTransmitter b : transmitterDAO.getIBeacons(mapName)) {
            if (b.getName() != null && !b.getName().isEmpty()) {
                mapping.put(b.getTransmitter().getIdentifier(), b.getName());
            }
        }

        return new Gson().toJson(mapping);
    }

    private String getName(String apName, String ssid, String macAddress, boolean virtualAP) {
        String name;
        if (apName == null || apName.isEmpty()) {
            if (ssid == null || ssid.isEmpty()) {
                name = macAddress;
            } else {
                name = ssid;
            }
        } else {
            if (virtualAP) {
                if (ssid == null || ssid.isEmpty()) {
                    name = apName + "_" + macAddress;
                } else {
                    name = apName + "_" + ssid;
                }
            } else {
                name = apName;
            }
        }

        return name;
    }


    /**
     * Upload a JSON and insert into the database
     *
     * @param mapName          Name of the map
     * @param fingerprintType  'wifi' or 'btle'
     * @param fingerprintsJson JSON String
     * @return a response 'CREATED' or 'INTERNAL_SERVER_ERROR'
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_DATA_MODE_UPLOAD, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity uploadFingerprints(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName,
                                             @RequestParam(FINGERPRINTS_DATA_TYPE) String fingerprintType,
                                             @RequestParam(FINGERPRINTS_DATA_JSON) String fingerprintsJson) {
        try {
            fingerprintDAO.replaceFingerprintsFromJson(mapName, fingerprintType, fingerprintsJson);
            return new ResponseEntity(HttpStatus.CREATED);
        } catch (Exception e) { //NOSONAR
            LOGGER.error("Upload of " + fingerprintType + " fingerprints for map " + mapName + " failed : " +
                    e.getMessage());
        }

        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Update "active" to true or false of an existing transmitter in the database
     *
     * @param mapName    Name of the map
     * @param identifier of the transmitter
     * @param active     flag to activate or deactivate (1/0, true/false, etc...)
     * @return a response 'CREATED', 'INTERNAL_SERVER_ERROR' or 'BAD_REQUEST'
     */
    @RequestMapping(path = FINGERPRINTS_DATA_ROOT + "/" + FINGERPRINTS_TRANSMITTER + "/" +
            FINGERPRINTS_TRANSMITTER_UPDATE, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity updateTransmitterActive(@RequestParam(FINGERPRINTS_DATA_MAPNAME) String mapName,
                                                  @RequestParam(FINGERPRINTS_TRANSMITTER) String identifier,
                                                  @RequestParam(FINGERPRINTS_TRANSMITTER_ACTIVE) Boolean active) {

        try {
            final boolean ok = transmitterDAO.updateTransmitter(mapName, identifier, active);

            if (ok) {
                return new ResponseEntity(HttpStatus.OK);
            }
        } catch (Exception e) { //NOSONAR
            LOGGER.error("Update of transmitter " + identifier + " for map " + mapName + " failed " +
                    e.getMessage());
        }

        return new ResponseEntity(HttpStatus.NOT_MODIFIED);
    }

}
